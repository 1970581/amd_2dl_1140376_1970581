package AMD_2DL_1140376_1970581;
//package graph;

import java.util.LinkedList;
import java.util.Iterator;

/**
 * Implementation of graph algorithms for a (undirected) graph structure 
 * Considering generic vertex V and edge E types
 * 
 * Works on AdjancyMatrixGraph objects
 * 
 * @author DEI-ESINF && HUGO && CELSO
 * 
 */
public class GraphAlgorithms {

    private static <T> LinkedList<T> reverse(LinkedList <T> list){
        LinkedList<T> reversed = new LinkedList<T>();
        Iterator<T> it = list.iterator();
        while(it.hasNext()) reversed.push(it.next());
        return reversed;
    }

    /**
     * Performs depth-first search of the graph starting at vertex.
     * Calls package recursive version of the method.
     *
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if vertex does not exist
     *
     */
    public static <V,E> LinkedList<V> DFS(AdjacencyMatrixGraph<V,E> graph, V vertex) {

        int index = graph.toIndex(vertex);
        if (index == -1)
            return null;

        LinkedList<V> resultQueue = new LinkedList<V>();
        resultQueue.add(vertex);
        boolean [] knownVertices = new boolean[graph.numVertices];
        DFS(graph, index, knownVertices, resultQueue);
        return resultQueue;
    }

    /**
     * Actual depth-first search of the graph starting at vertex.
     * The method adds discovered vertices (including vertex) to the queue of vertices
     *
     * @param graph Graph object
     * @param index Index of vertex of graph that will be the source of the search
     * @param known previously discovered vertices
     * @param verticesQueue queue of vertices found by search
     *
     */
    static <V,E> void DFS(AdjacencyMatrixGraph<V,E> graph, int index, boolean [] knownVertices, LinkedList<V> verticesQueue) {
        knownVertices[index] = true;                              
        for (int i = 0 ; i < graph.numVertices ; i++) {     
            if(graph.edgeMatrix[index][i] != null && knownVertices[i] == false){
                verticesQueue.add(graph.vertices.get(i));
                DFS(graph, i, knownVertices, verticesQueue);
            }
        }
    }

    /**
     * Performs breath-first search of the graph starting at vertex.
     * The method adds discovered vertices (including vertex) to the queue of vertices
     *	
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if vertex does not exist
     *
     */ 
    public static <V,E> LinkedList<V> BFS(AdjacencyMatrixGraph<V,E> graph, V vertex) {
		//throw new UnsupportedOperationException("Not supported yet.");	
        
        int index = graph.toIndex(vertex);
        if (index == -1)
            return null;
        
        
        
        LinkedList<V> qbfs = new LinkedList();
        LinkedList<V> qaux = new LinkedList();
        boolean visited[] = new boolean[graph.numVertices];//Inicializado a falsos
        System.out.println("BFS");
        
        qbfs.add(vertex);///***********************************************************************************************************
        qaux.add(vertex);
        visited[graph.toIndex(vertex)] = true;
        
        while(!qaux.isEmpty()){
            vertex = qaux.removeFirst();
            System.out.println(vertex);
            
            //VAMOS USAR SO VERTEX's pk o prog nao lida com edges repetidas que a prof usa nos testes.
            for (V v : graph.directConnections(vertex)){
                System.out.println("Processing "+ vertex +" - "+ v + " visited: " +v +" "+ visited[graph.toIndex(v)]);
                if( !visited[graph.toIndex(v)]){
                    qbfs.add(v);
                    qaux.add(v);
                    visited[graph.toIndex(v)] = true;
                }
            }
            
            /*************** NAO USAR PORQUE O GRAPH NAO LIDA COM EDGES REPETIDAS QUE O TESTE USA.
            for (E e : graph.outgoingEdges(vertex)){
                V[] vv = graph.endVertices(e);
                
                //System.out.println(qbfs);
                V vAdj = vertex.equals(vv[0]) ? vv[1] : vv[0];
                
                System.out.println("Processing "+ vv[0]+" "+vv[1] + " visited: " +vAdj +" "+ visited[graph.toIndex(vv[1])]);
                
                if( !visited[graph.toIndex(vAdj)]){
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                    visited[graph.toIndex(vAdj)] = true;
                }
            
            }
            */
        }
        
        
        
        return qbfs;
    }

    /**
     * All paths between two vertices 
     * Calls recursive version of the method.
     *
     * @param graph Graph object
     * @param source Source vertex of path 
     * @param dest Destination vertex of path
     * @param path LinkedList with paths (queues)
     * @return false if vertices not in the graph
     *
     */
    public static <V,E> boolean allPaths(AdjacencyMatrixGraph<V,E> graph, V source, V dest, LinkedList<LinkedList<V>> paths) {
	if ( !graph.checkVertex(dest) || !graph.checkVertex(source)) return false;
        
        paths.clear();  // Vamos limpar o paths, sempre que iniciarmos o metodo. O teste unitario das prof nao faz isto. Depois falha nos outros testes.
        
        
        boolean visited[] = new boolean[graph.numVertices];  //Array de bollean que indica se visitado o index de um vertice. True = visited.
        LinkedList<V> path = new LinkedList<>();
                
        allPaths(graph, graph.toIndex(source), graph.toIndex(dest), visited, path, paths);
        
        /*  DESNECESSAIOR, corrigido para nao fazer isto.
        //Vamos pagar os paths que tem size = 0.
        Iterator it = paths.iterator();
        while(it.hasNext()){
            LinkedList<V> l = (LinkedList) it.next();
            if (l.isEmpty()) it.remove();
        }
        */
        
        return true;  ///HUGO
    }
    
    
    
    /**
     * Actual paths search 
     * The method adds vertices to the current path (stack of vertices)
     * when destination is found, the current path is saved to the list of paths 
     *
     * @param graph Graph object
     * @param sourceIdx Index of source vertex 
     * @param destIdx Index of destination vertex
     * @param knownVertices previously discovered vertices
     * @param auxStack stack of vertices in the path
     * @param path LinkedList with paths (queues)
     * 
     */
    static <V,E> void allPaths(AdjacencyMatrixGraph<V,E> graph, int sourceIdx, int destIdx, boolean [] knownVertices, LinkedList<V> auxStack, LinkedList<LinkedList<V>> paths) {
        V vOrig = graph.vertices.get(sourceIdx);
        V vDest = graph.vertices.get(destIdx);
        knownVertices[sourceIdx] = true;
        
        //auxStack = path
        
        auxStack.add(vOrig);
        
        for (V v : graph.directConnections(graph.vertices.get(sourceIdx))){
            
            if ( v.equals(vDest)){
                auxStack.add(vDest);
                
                paths.add((LinkedList<V>)auxStack.clone());
                auxStack.removeLast();
            }
            else {
                if(!knownVertices[graph.toIndex(v)]){
                    //RECURSIVE CALL
                    allPaths( graph, graph.toIndex(v), destIdx, knownVertices, auxStack , paths );
                }
            }
        }
        
        /*   NAO USAR EDGES PK PROFESORA USA EDGES REPETIDAS E O PROGRAMA NAO FUNCIONA COM EDGES REPETIDAS.
        for (E e : graph.outgoingEdges(graph.vertices.get(sourceIdx))){
            V[] aux = graph.endVertices(e);
            if ( aux[1].equals(vDest)){
                auxStack.add(vDest);
                paths.add(auxStack);
                auxStack.removeLast();
            }
            else {
                if(!knownVertices[graph.toIndex(aux[1])]){
                    //RECURSIVE CALL
                    allPaths( graph, graph.toIndex(aux[1]), destIdx, knownVertices, auxStack , paths );
                }
            }
        }
        */
        knownVertices[sourceIdx] = false;
        auxStack.removeLast();   //HUGO
    }

    /**
     * Transforms a graph into its transitive closure 
     * uses the Floyd-Warshall algorithm
     * 
     * @param graph Graph object
     * @param dummyEdge object to insert in the newly created edges
     * @return the new graph 
     */
    public static <V, E> AdjacencyMatrixGraph<V, E> transitiveClosure(AdjacencyMatrixGraph<V, E> graph, E dummyEdge){
            //throw new UnsupportedOperationException("Not supported yet.");	//M**************************************** HUGO 
        // FALTA CORRIGIR NAO USAR!!!!!!!!
        System.out.println("transitiveClosure com DummyEdge IMCOMPLETO");
        AdjacencyMatrixGraph<V,E> matrizDevolver = (AdjacencyMatrixGraph) graph.clone();
        int n = matrizDevolver.numVertices;
        for (int k = 0; k < n; k++){
            for (int i = 0; i < n ; i++){
                if(i != k && matrizDevolver.privateGet(i, k) != null){
                    for (int j = 0; j < n ; j++){
                        if ( i != j && k != j && matrizDevolver.privateGet(k, j) != null ){
                            //matrizDevolver.insertEdge(vertexA, vertexB, dummyEdge)
                            matrizDevolver.edgeMatrix[i][j] = dummyEdge;
                            
                        }
                    }
                }
            }
        }
        
        return matrizDevolver;
    }

}
