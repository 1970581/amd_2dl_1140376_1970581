/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

/**
 *
 * @author i140376
 */
public class Utils {
    
    /**
     * Relacao para converter Km para metros.
     */
    static private double KM_A_METROS_X_1000 = 1000;
    
    /**
     * Verifica a distancia entre duas coordenadas, erro menor a 2-5% em Portugal.
     * @param latitudeA  latitude da coordernada A
     * @param longitudeA longitude da coordernada A
     * @param latitudeB latitude da coordernada B
     * @param longitudeB longitude da coordernada B
     * @return distancia entre as 2 coordenadas.
     */
    public static float calcularDistanciaEntreDuasCoordenadas(float latitudeA, float longitudeA, float latitudeB, float longitudeB){
        
        /*   METODO DE CALCULO DE DISTANCIA ENTRE 2 PONTOS GPS baseado no U.S. Census Bureau 
        http://andrew.hedges.name/experiments/haversine/
        
            dlon = lon2 - lon1
            dlat = lat2 - lat1
            a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2
            c = 2 * atan2( sqrt(a), sqrt(1-a) )
            d = R * c (where R is the radius of the Earth) 

        Note: this formula does not take into account the non-spheroidal (ellipsoidal) shape of the Earth. 
        It will tend to overestimate trans-polar distances and underestimate trans-equatorial distances.
        The values used for the radius of the Earth (3961 miles & 6373 km) are optimized for locations 
        around 39 degrees from the equator (roughly the Latitude of Washington, DC, USA). 
        */
        double correcaoPI = Math.PI / 180;
        double lon1 = longitudeA * correcaoPI ;
        double lon2 = longitudeB* correcaoPI;
        double lat1 = latitudeA* correcaoPI;
        double lat2 = latitudeB* correcaoPI;
        double distancia = 0;
        
        double dlon = lon2-lon1;
        double dlat = lat2-lat1;
        
        
        //double a = Math.sin(dlat/2)*Math.sin(dlat/2) +  Math.cos(lat1) *  Math.cos(lat2) * ( Math.sin(dlon/2) *Math.sin(dlon/2)  );
        double a = Math.sin(dlat/2)*Math.sin(dlat/2) +  Math.cos(lat1) *  Math.cos(lat2) *  Math.sin(dlon/2) * Math.sin(dlon/2) ;
        double c = 2 * Math.atan2( Math.sqrt(a), Math.sqrt(1-a) );
        distancia = 6373 * c * Utils.KM_A_METROS_X_1000;
        return (float )distancia;
    
    }
}


/* LIXO - Velho codigo para coordenadas x,y
float distancia = 0;
        distancia = ( 
                ( (latitudeA - latitudeB) * (latitudeA - latitudeB) ) + 
                ( (longitudeA - longitudeB) * (longitudeA - longitudeB) )
                );
        distancia = (float) Math.sqrt(distancia);
        return distancia;

*/