/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** Controlador para remover utilizador
 *
 * @author 
 */
public class RemoverUtilizadorController {
    
     /* Mundo */
    private Mundo mundo;
    /* Registo de cidades do Mundo */
    private RegistoCidade registoCidade;
    /* Registo de utilizadores do mundo */
    private RegistoUtilizador registoUtilizador;
    
    
    /**
     * Construtor com parametro
     * @param mundo 
     */
    public RemoverUtilizadorController(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }
    
     /**
     * Verifica se existe um utilizador com este nomeUtilizador.
     * @param nomeUtilizador
     * @return true se existir, senao false
     */
    public boolean existeUtilizadorComNome(String nomeUtilizador){
        Utilizador utilizadorComNome = this.registoUtilizador.getUtilizador(nomeUtilizador);
        if (utilizadorComNome == null) {return false;}
        else return true;
    }
    
    
    /**
     * Remove um utilizador de nomeUtilizador do registo de utilizadores. 
     * Remove as ligacoes de amizade, e corrige os Mayors de todas as cidade que alguma vez visitou.
     * @param nomeUtilizador nome do utilizador a remover.
     * @return sucessso da operacao
     */
    public boolean removerUtilizador(String nomeUtilizador){
                
        Map <String, Utilizador> mapRegistoUtilizador = this.registoUtilizador.getMapRegistoUtilizador();
        
        Utilizador utilizadorARemover = mapRegistoUtilizador.remove(nomeUtilizador);
        if (utilizadorARemover == null) {return false;}
        
        //Buscamos a Lista das cidades visitadas com repeticoes do utilizador.
        List <Cidade> listaCidadesVisitadasComRepetidas = utilizadorARemover.getListaCidadesVisitadas();
        
        //Criamos um saco para guardar as cidades recalcular o Mayor.
        Set <Cidade> cidadesAVerificarMayor = new LinkedHashSet<>();
        
        Iterator iterador = listaCidadesVisitadasComRepetidas.iterator();
        
        //Iteramos a Lista e passamos para o saco sem repetidas as onde ele é Mayor e necessitam recalcular o Mayor.
        while(iterador.hasNext()){
            Cidade cidadeVisitada = (Cidade )iterador.next();
            if (cidadeVisitada.getMayor() != null && cidadeVisitada.getMayor().equals(utilizadorARemover)){//Atencao a mayor a null.
                cidadesAVerificarMayor.add(cidadeVisitada);
                cidadeVisitada.resetMayorENumeroMaxVisitasANull();//Mayor a null
            }
        }
        
        //Iteramos o map de utilizadores, e para cada cidade do Saco reverificamos o Mayor da cidade.
        for(Utilizador utilizador : mapRegistoUtilizador.values()){
            Iterator iteradorSet = cidadesAVerificarMayor.iterator();
            
            while(iteradorSet.hasNext()){
                ((Cidade)iteradorSet.next()).utilizadorMoveuParaCidadeActualizarMayor(utilizador);
            }
        }
        
        //Buscamos o Mapa de amigos do utilizador.
        Map <String, Utilizador> mapaDeAmigos = utilizadorARemover.getRegistoAmizade().getRegistoAmizade();
        
        //E em cada amigo, apagamos a ligacao Amigo-UtilizadorARemover.
        for(Utilizador utilizadorAmigo : mapaDeAmigos.values()){
            utilizadorAmigo.getRegistoAmizade().removerAmizade(utilizadorARemover);
        }
        //Nao apagamos as ligacoes UtilizadorARemover-Amigo em UtilizadorARemover,
        // porque o objecto neste momento  encontra-se solto (desrefenciado/morto) a espera do Garbage collector.
        
        return true;
    }
    
    
}
