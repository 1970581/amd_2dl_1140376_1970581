/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;

/** controller para fazer check in numa cidade
 *
 * @author 
 */
public class FazerCheckInNovaCidadeController {
    
    
    /* Mundo */
    private Mundo mundo;
    /* Registo de cidades do Mundo */
    private RegistoCidade registoCidade;
    /* Registo de utilizadores do mundo */
    private RegistoUtilizador registoUtilizador;
    
    /**
     * Construtor com parametro, o Mundo
     * @param mundo 
     */
    public FazerCheckInNovaCidadeController(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }
    
    /**
     * Verifica se existe utilizador com este nome.
     * @param nomeUtilizador
     * @return 
     */
    public boolean existeUtilizadorComNome(String nomeUtilizador){
        return (this.registoUtilizador.getUtilizador(nomeUtilizador) != null);
    }
    
    /**
     * Verifica se existe cidade com nome
     * @param nomeCidade
     * @return 
     */
    public boolean existeCidadeComNome(String nomeCidade){
        return (this.registoCidade.getCidade(nomeCidade) != null);
    }
    
    /**
     * Devolve o nome da cidade onde se encontra o utilizador com o nome
     * @param utilizador
     * @return 
     */
    public String nomeDaCidadeOndeEstou(String utilizador){
        Cidade cidadeOndeEstou = this.registoUtilizador.getUtilizador(utilizador).getUltimaCidade();
        if (cidadeOndeEstou == null) return null;
        return cidadeOndeEstou.getNome();
    }
    
    /**
     * Move o utilizador pelo nome para a cidade pelo nome
     * @param nomeUtilizador
     * @param nomeCidade
     * @return falso se falhar, true se conseguir.
     */
    public boolean moverUtilizadorParaCidade(String nomeUtilizador, String nomeCidade){
        Cidade cidadeOndeQueroIr = this.registoCidade.getCidade(nomeCidade);
        if (cidadeOndeQueroIr == null ) return false;
        Utilizador utilizador = this.registoUtilizador.getUtilizador(nomeUtilizador);
        if (utilizador == null) return false;
        return utilizador.moverParaCidade(cidadeOndeQueroIr);
        
    }
    
    
}
