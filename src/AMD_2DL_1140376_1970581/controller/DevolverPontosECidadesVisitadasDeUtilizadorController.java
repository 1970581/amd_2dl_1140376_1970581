/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.Iterator;
import java.util.List;


/** Controller para devolver os pontos de um utilizador de as cidade visitadas.
 *
 * @author 
 */
public class DevolverPontosECidadesVisitadasDeUtilizadorController {
    
        /* Mundo */
    private Mundo mundo;
    /* Registo de cidades do Mundo */
    private RegistoCidade registoCidade;
    /* Registo de utilizadores do mundo */
    private RegistoUtilizador registoUtilizador;
    
    /**
     * Construtor com parametro, o Mundo
     * @param mundo 
     */
    public DevolverPontosECidadesVisitadasDeUtilizadorController(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }
    
    /**
     * Devolve o numero de pontos de um utilizador sob a form Integer, null se nao existir utilizador.
     * @param nomeUtilizador nome do utilizador
     * @return numero de pontos
     */
    public Integer obterPontosDeUtilizador(String nomeUtilizador){
        Utilizador utilizador = registoUtilizador.getUtilizador(nomeUtilizador);
        if (utilizador == null) return null;
        return Integer.valueOf( utilizador.getPontos() );
    }
    
    /**
     * Devolve para um utilizador indicado por nome um array de Strings com as cidades visitadas.
     * @param nomeUtilizador nome do utilizador
     * @return cidade visitadas String[]
     */
    public String[] obterStringArrayComNomeCidadesVisitadasPeloUtilizador(String nomeUtilizador){
        String [] stringArrayComNomeCidadesVisitadas;        
        
        Utilizador utilizador = registoUtilizador.getUtilizador(nomeUtilizador); //apanhamos o utilizador
        if (utilizador == null) return null;
        
        List <Cidade> listaCidadesVisitadas = utilizador.getListaCidadesVisitadas();  //apanhamos as cidades visitadas pelo utilizador
        
        if(listaCidadesVisitadas.isEmpty()) return null;
        
        stringArrayComNomeCidadesVisitadas = new String[listaCidadesVisitadas.size()]; //criamos array de Strings com o tamanho da lista
        
        Iterator iterador = listaCidadesVisitadas.listIterator(); // Criamos iterador da lista, visto ser mais rapido a iterar.
        
        for (int i = 0; i< listaCidadesVisitadas.size(); i++){
            
            stringArrayComNomeCidadesVisitadas[i] = ((Cidade)iterador.next()).getNome();
            
        }
        
        return stringArrayComNomeCidadesVisitadas;
    }
    
    
    
    
    
}
