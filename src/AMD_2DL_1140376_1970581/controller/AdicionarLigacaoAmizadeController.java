/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;

/**
 *
 * @author i140376
 */
public class AdicionarLigacaoAmizadeController {

    /* Mundo */
    private Mundo mundo;
    /* Registo de cidades do Mundo */
    private RegistoCidade registoCidade;
    /* Registo de utilizadores do mundo */
    private RegistoUtilizador registoUtilizador;

    /**
     * Construtor com parametro
     *
     * @param mundo
     */
    public AdicionarLigacaoAmizadeController(Mundo mundo) {
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }

    public boolean isUtilizador(String nome) {
        if (this.registoUtilizador.getUtilizador(nome) == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isAmigo(String nomeUtilizador, String nomeAmigo) {
        Utilizador utilizador = this.registoUtilizador.getUtilizador(nomeUtilizador);
        Utilizador amigo = this.registoUtilizador.getUtilizador(nomeAmigo);
        if (utilizador == null || amigo == null) {
            return false;
        }
        return utilizador.getRegistoAmizade().isAmigo(amigo);
    }

    public boolean criarAmizade(String nomeUtilizador, String nomeAmigo) {
        Utilizador utilizador = this.registoUtilizador.getUtilizador(nomeUtilizador);
        Utilizador amigo = this.registoUtilizador.getUtilizador(nomeAmigo);
        if (utilizador == null || amigo == null) {
            return false;
        }
        if (utilizador.getRegistoAmizade().isAmigo(amigo)) {
            return false;
        } else {
            return utilizador.adicionarAmizade(amigo);

        }

    }
}
