/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author KAMMIKAZI
 */
public class UtilizadoresMaisInfluentesDaRedeController {
    /* Mundo */

    private Mundo mundo;
    /* Registo de cidades do Mundo */
    private RegistoCidade registoCidade;
    /* Registo de utilizadores do mundo */
    private RegistoUtilizador registoUtilizador;

    /**
     * Construtor com parametro
     *
     * @param mundo
     */
    public UtilizadoresMaisInfluentesDaRedeController(Mundo mundo) {
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }
    /**
     * metodo para verficiar se o registo de utilizador nao se encontra vazio
     * @return true caso haja utilizadores no registo, falso caso contrario
     */
    public boolean isEmpty() {
        return registoUtilizador.getMapRegistoUtilizador().isEmpty();
    }

    /**
     * Listar nome de utilizadores com mais amigos na rede.
     * @return lista com nome de utilizadores com mais amigos na rede
     */
    public ArrayList obterUtilizadoresMaisInfulentesDaRede() {
        if (!isEmpty()) {
            ArrayList utilizadoresMaisInfluentes = new ArrayList<>();
            List<Utilizador> listUtilizadores = registoUtilizador.listarUtilizadoresComMaisAmigos();
            for (Utilizador utilizador : listUtilizadores) {
                utilizadoresMaisInfluentes.add(utilizador.getNome());
            }
            return utilizadoresMaisInfluentes;
        } else {
            return null;
        }
    }
}
