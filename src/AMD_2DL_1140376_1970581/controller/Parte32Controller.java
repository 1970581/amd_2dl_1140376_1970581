/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.BST;
import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author KAMMIKAZI
 */
public class Parte32Controller {

    private Mundo mundo;
    private RegistoCidade registoCidade;
    private RegistoUtilizador registoUtilizador;
    private BST<CidadeVisitas> bstCidadeVisitas;
    private int visitas = 0;
    
    public Parte32Controller(Mundo mundo) {
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
        bstCidadeVisitas = new BST();
        for (Cidade c : registoCidade.getMapRegistoCidade().values()) {
            visitas = 0;
            for (Utilizador u : registoUtilizador.getMapRegistoUtilizador().values()) {
                if(u.calcularNumeroDeVisitasACidade(c) > 0){
                visitas++;
                }
            }
            bstCidadeVisitas.insert(new CidadeVisitas(c,visitas));
        }
    }
        

    

    public ArrayList<String> ordenarCidadeVisitas() {
        ArrayList<String> lista = new ArrayList<>();
        for (CidadeVisitas cv : bstCidadeVisitas.inOrder()) {
            lista.add(cv.toString());
        }
        return lista;
    }





/**
 * Nested class Utilizador Pontos
 */
public class CidadeVisitas implements Comparable<CidadeVisitas> {

    Cidade c;
    int visitas;

    public CidadeVisitas(Cidade c,int visitas) {
        this.c = c;
        this.visitas = visitas;
    }
    
    public boolean equals(Object obj) {
        return c.equals(obj);
    }

    @Override
    public String toString() {
        return c.getNome() + ":" + visitas;
    }

    @Override
    public int compareTo(CidadeVisitas o) {
        int compPontos = this.visitas - o.visitas;
        if (compPontos == 0) {
            return this.c.getNome().compareTo(o.c.getNome());
        }
        return compPontos;
    }

}
}

