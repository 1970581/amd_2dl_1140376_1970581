/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.CidadeMayor;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/** Controller para devolver a relacao Cidade Mayor de forma Ordernada 
 *  por pontos de mayor decrescentes.
 *
 * @author 
 */
public class DevolverCidadeMayorController {
    
        /* Mundo */
    private Mundo mundo;
    /* Registo de cidades do Mundo */
    private RegistoCidade registoCidade;
    /* Registo de utilizadores do mundo */
    private RegistoUtilizador registoUtilizador;
    
    
    /**
     * Construtor com parametro
     * @param mundo 
     */
    public DevolverCidadeMayorController(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }
    
    
    /**Cria um set auto-ordenado Cidade-Mayor por pontos de mayor de forma decrescente.
     * 
     * @return um set auto-ordenado Cidade-Mayor por pontos de mayor de forma decrescente.
     */
    public Set<CidadeMayor> criarSetOrdenadoCidadeMayor(){
        Set setOrdenadoCidadeMayor = new TreeSet();
        
        Map<String,Cidade> mapRegistoCidade = registoCidade.getMapRegistoCidade();
        
        //Utilizador mayor;
        //CidadeMayor cidadeMayor;
        
        for(Cidade cidadeRegistada : mapRegistoCidade.values()){
            if (cidadeRegistada == null) continue;          //Bypass a cidades nullas.
            if (cidadeRegistada.getMayor() == null) continue; //Bypass a cidades sem Mayor (sem visitas)
           setOrdenadoCidadeMayor.add( new CidadeMayor(cidadeRegistada, cidadeRegistada.getMayor()) );
        }
        
        return setOrdenadoCidadeMayor;
    }
    
    /** Cria uma linkedList de strings ordenada decrescentemente por pontos de mayor.
     *  A lista contem "cidade -> mayor com pontos pontos.
     * @return uma LinkedList de strings.
     */
    public List <String> criarListagemCidadeMayorPontos(){
        List <String> listaCidadeMayorPontos = new LinkedList<>();
        
        Set setOrdenadoCidadeMayor = this.criarSetOrdenadoCidadeMayor();
        
        Iterator iterador = setOrdenadoCidadeMayor.iterator();
        CidadeMayor cidadeMayor;
        while(iterador.hasNext()){
            cidadeMayor = (CidadeMayor) iterador.next();
            listaCidadeMayorPontos.add( 
                    cidadeMayor.getCidade().getNome() +
                    " -> " +
                    cidadeMayor.getMayor().getNome() +
                    " com " +
                    cidadeMayor.getPontos()+
                    " pontos."
                    );
        }
        
        return listaCidadeMayorPontos;
    }
    
    
}
