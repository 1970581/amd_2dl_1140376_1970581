package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.BST;
import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.ArrayList;
import java.util.Collections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hugo e Celso
 */
public class Parte31Controller {

    private Mundo mundo;
    private RegistoCidade registoCidade;
    private RegistoUtilizador registoUtilizador;
    private BST<UtilizadorPontos> bstMayor;

    public Parte31Controller(Mundo mundo) {
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
        //int nCidades = registoCidade.getMapRegistoCidade().size();  // Para evitarmos inicializarmos E[0][0] a zeros... que da erro.
        bstMayor = new BST();
        for (Cidade c : registoCidade.getMapRegistoCidade().values()) {
            if (c.getMayor() != null) {
                bstMayor.insert(new UtilizadorPontos(c.getMayor()));
            }
        }
    }

    public ArrayList<String> ordenarMayorPontos() {
        ArrayList<String> lista = new ArrayList<>();
        for (UtilizadorPontos up : bstMayor.inOrder()) {
            lista.add(up.toString());
        }
        //Collections.reverse(lista);
        return lista;
    }

    /**
     * Nested class Utilizador Pontos
     */
    public class UtilizadorPontos implements Comparable<UtilizadorPontos> {

        Utilizador u;
        int pontos;

        public UtilizadorPontos(Utilizador u) {
            this.u = u;
            this.pontos = u.getPontos();
        }

        public Utilizador getUtilizador() {
            return u;
        }

        public boolean equals(Object obj) {
            return u.equals(obj);
        }

        @Override
        public String toString() {
            return u.getNome() + ":" + pontos;
        }

        @Override
        public int compareTo(UtilizadorPontos o) {
            int compPontos =  o.pontos-this.pontos;
            if (compPontos == 0) {
                return this.getUtilizador().getNome().compareTo(o.u.getNome());
            }
            return compPontos;
        }
    }
}
