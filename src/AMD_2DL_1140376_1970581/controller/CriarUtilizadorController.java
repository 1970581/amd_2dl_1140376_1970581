/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;

/**
 *
 * @author Hugo
 */
public class CriarUtilizadorController {
    
    /* Mundo */
    private Mundo mundo;
    /* Registo de cidades do Mundo */
    private RegistoCidade registoCidade;
    /* Registo de utilizadores do mundo */
    private RegistoUtilizador registoUtilizador;
    
    
    /**
     * Construtor com parametro
     * @param mundo 
     */
    public CriarUtilizadorController(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }
    
    /**
     * Verifica se existe um utilizador com este nome.
     * @param nome
     * @return 
     */
    public boolean existeUtilizadorComNome(String nome){
        Utilizador utilizadorComNome = this.registoUtilizador.getUtilizador(nome);
        if (utilizadorComNome == null) {return false;}
        else return true;
    }
    
    /**
     * Verifica se existe um utilizador com este email
     * @param email
     * @return 
     */
    public boolean existeUtilizadorComEmail(String email){
        Utilizador utilizadorComEmail = this.registoUtilizador.getUtilizadorByEmail(email);
        if (utilizadorComEmail == null) {return false;}
        else return true;
    }
    
    /**
     * Verifica se existe uma cidade com este nome.
     * @param nome
     * @return 
     */
    public boolean existeCidadeNome(String nome){
        Cidade cidadeComNome = this.registoCidade.getCidade(nome);
        if (cidadeComNome == null) {return false;}
        else return true;
    }
    
    /**
     * Cria um novo utilizador e moveo para esta cidade inicial devolvendo o sucesso da operacao.
     * @param nome
     * @param email
     * @param cidade
     * @return 
     */
    public boolean criarUtilizadorComCidadeInicial(String nome, String email, String cidade){
        Cidade cidadeInicial = this.registoCidade.getCidade(cidade);
        if (cidadeInicial == null ) return false;
        Utilizador novoUtilizador;
        novoUtilizador = this.registoUtilizador.criarUtilizador(nome, email);
        if (novoUtilizador == null ) return false;
        return novoUtilizador.moverParaCidade(cidadeInicial);
    }
    
}
