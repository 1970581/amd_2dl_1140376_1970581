/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoAmizade;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;

/**
 *
 * @author Hugo
 */
public class LerFicheiroController {
    
    private Mundo mundo;
    private RegistoCidade registoCidade;
    private RegistoUtilizador registoUtilizador;
    private RegistoAmizade registoAmizade;
    private File ficheiroCidades;
    private File ficheiroUtilizadores;
    private String separador = ",";
    
    public LerFicheiroController(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }
    
    public boolean carregarFicheiroCidades(String filename){
        
        ficheiroCidades = new File(filename);
        if (!ficheiroCidades.exists() || !ficheiroCidades.canRead()) return false;
        
        boolean sucess = this.lerFileCidades(ficheiroCidades);
        
        return sucess;
    }
    
    public boolean lerFileCidades(File file){
        try{
        Scanner buffer = new Scanner(file);
        
        while(buffer.hasNext()){
            String aux = buffer.nextLine();
            String info[] = aux.split(this.separador);
            if(info.length == 4) {
                String nome = info[0];
                int pontos = 0; 
                float latitude = 0f;
                float longitude = 0f;
                try {
                pontos = Integer.valueOf(info[1]);    
                latitude = Float.valueOf(info[2]);
                longitude = Float.valueOf(info[3]);
                }
                catch(NumberFormatException ee){continue;}
            this.registoCidade.adicionarCidade(nome,pontos, latitude, longitude);
            }
                   
        }
        buffer.close();
        
        }
        catch(FileNotFoundException e){return false;}
        
        return true;
    }
    
    
    public boolean carregarFicheiroUtilizadores(String filename){
        
        ficheiroUtilizadores = new File(filename);
        if (!ficheiroUtilizadores.exists() || !ficheiroUtilizadores.canRead()) return false;
        
        boolean sucess = this.lerFileUtilizadores(ficheiroUtilizadores);
        boolean sucessAmigos = this.lerFileUtilizadoresAmizades(ficheiroUtilizadores);
        
        return sucess && sucessAmigos;
    }
    
    
    public boolean lerFileUtilizadores(File file){
        try{
        Scanner buffer = new Scanner(file);
        
        while(buffer.hasNext()){
            String auxUtilizador = buffer.nextLine();  //Ler linha de infoUtilizador do Utilizador
            if (!buffer.hasNext()) continue; // Se nao exitir proxima linha, nao existem amigos, portanto saltamos para o proxima iteracao que vai terminar.
            String auxAmigos = buffer.nextLine();       //Ler tinha dos amigos do Utilizador
            String info[] = auxUtilizador.split(this.separador);
            String amigos[] = auxAmigos.split(this.separador);
                        
            if (info.length < 3) continue; // Se nao tivermos pelo menos 3 campos, saltamos para a proxima iteracao.
            
            //Criar utilizador
            Utilizador novoUtilizador = this.registoUtilizador.criarUtilizador(info[0], info[1]);
            if (novoUtilizador == null ) continue; // Utilizador é repetido e foi devolvido null. Saltamos para a proxima iteracao
            
            //Registar as cidades no utilizador
            Cidade cidadeAdicionar;
            for (int i = 2; i < info.length; i++){
                cidadeAdicionar = this.registoCidade.getCidade(info[i]);
                if (cidadeAdicionar != null) novoUtilizador.moverParaCidade(cidadeAdicionar);
            }
                   
        }
        buffer.close();
            
        
        }
        catch(FileNotFoundException e){return false;}
        
        return true;
    }
    
    
    public boolean lerFileUtilizadoresAmizades(File file){
        try{
        Scanner buffer = new Scanner(file);
        
        while(buffer.hasNext()){
            String auxUtilizador = buffer.nextLine();  //Ler linha de infoUtilizador do Utilizador
            if (!buffer.hasNext()) continue; // Se nao exitir proxima linha, nao existem amigos, portanto saltamos para o proxima iteracao que vai terminar.
            String auxAmigos = buffer.nextLine();       //Ler tinha dos amigos do Utilizador
            String[] infoUtilizador = auxUtilizador.split(this.separador);
            String amigos[] = auxAmigos.split(this.separador);
                        
            if (amigos.length < 1) continue; // Se o utilizador em questao nao tiver amigos, saltamos para a proxima iteracao.
            
            //Criar utilizador
            Utilizador utilizadorPai = this.registoUtilizador.getUtilizador(infoUtilizador[0]);
            if (utilizadorPai == null ) continue; // Utilizador é repetido e foi devolvido null. Saltamos para a proxima iteracao
            for(int i = 0; i < amigos.length ; i++){
            utilizadorPai.adicionarAmizade(registoUtilizador.getUtilizador(amigos[i]));
            }
        }
        buffer.close();
            
        
        }
        catch(FileNotFoundException e){return false;}
        
        return true;
    }
    
    //public boolean 
    
    
}
