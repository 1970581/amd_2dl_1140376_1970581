/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.ArrayList;
import java.util.List;

/**
 * DevolverAmigosPorLocalizacaoController - utilizado para devolver amigos correspondentes a uma localização especifica
 * @author i140376
 */
public class DevolverAmigosPorLocalizacaoController {
        
    /* Mundo */
    private Mundo mundo;
    /* Registo de cidades do Mundo */
    private RegistoCidade registoCidade;
    /* Registo de utilizadores do mundo */
    private RegistoUtilizador registoUtilizador;
    
    /**
     * Construtor com parametro, o Mundo
     * @param mundo 
     */
    public DevolverAmigosPorLocalizacaoController(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }
    
    /**
     * Verifica se existe um utilizador com este nome.
     * @param nome
     * @return 
     */
    public boolean existeUtilizadorComNome(String nome){
        Utilizador utilizadorComNome = this.registoUtilizador.getUtilizador(nome);
        if (utilizadorComNome == null) {return false;}
        else return true;
    }
    /**
     * Verifica se existe alguma cidade guardada no Map do registo Cidades com o nome.
     * @param nome
     * @return 
     */
    public boolean existeCidadeComNome(String nome){
        return (this.registoCidade.getCidade(nome) != null );
    }
    
    /**
     * Encontra de devolve uma lista com o nome dos amigos que se encontram na cidade.
     * @param nomeUtilizador nome do utilizador
     * @param nomeCidade nome da cidade a procurar
     * @return a lista do nomes dos amigos
     */
    public List <String> encontrarAmigosEmCidade(String nomeUtilizador, String nomeCidade){
        List <String> listaNomeAmigosNaCidade = new ArrayList<>();
        Utilizador utilizador = this.registoUtilizador.getUtilizador(nomeUtilizador);
        if (utilizador == null) return null;
        Cidade cidade = this.registoCidade.getCidade(nomeCidade);
        if (cidade == null) return null;
        
        List<Utilizador> listaDeAmigos = utilizador.econtrarAmigosPorNomeCidade(cidade);
        for (Utilizador u : listaDeAmigos){
            listaNomeAmigosNaCidade.add(u.getNome());
        }
        
        
        return listaNomeAmigosNaCidade;
    }
    
    /**
     * Metodo para encontrar amigos que se encontrem em cidades proximas da coordenadas de latitude e longitude.
     * @param nomeUtilizador nome do utilizador que tem amigos
     * @param latitude latitude
     * @param longitude longitude
     * @return uma lista de nome de Utilizadores Amigo.
     */
    public List <String> encontrarAmigosEmCidadePorCoordenadas(String nomeUtilizador, float latitude, float longitude){
        List <String> listaNomeAmigosNaCidade = new ArrayList<>();
        Utilizador utilizador = this.registoUtilizador.getUtilizador(nomeUtilizador);
        if (utilizador == null) return null;
        
        List<Utilizador> listaDeAmigos = utilizador.encontrarAmigosPorCoordenadas(latitude,longitude);
        for (Utilizador u : listaDeAmigos){
            listaNomeAmigosNaCidade.add(u.getNome());
        }
        
        
        return listaNomeAmigosNaCidade;
    }
    
}
