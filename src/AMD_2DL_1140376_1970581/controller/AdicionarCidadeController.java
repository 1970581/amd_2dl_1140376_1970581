/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;

/**
 * Controllador de adicionar uma nova cidade.
 * @author 
 */
public class AdicionarCidadeController {
    
    /* Mundo */
    private Mundo mundo;
    /* Registo de cidades do Mundo */
    private RegistoCidade registoCidade;
    /* Registo de utilizadores do mundo */
    private RegistoUtilizador registoUtilizador;
    
    /**
     * Construtor com parametro, o Mundo
     * @param mundo 
     */
    public AdicionarCidadeController(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
    }
    
    /**
     * Verifica se existe alguma cidade guardada no Map do registo Cidades com o nome.
     * @param nome
     * @return 
     */
    public boolean existeCidadeComNome(String nome){
        return (this.registoCidade.getCidade(nome) != null );
    }
    
    /**
     *  Adiciona Cidade ao registo recorrendo a String nome, int pontos, float latitude, float longitude
     * @param nome nome da cidade
     * @param pontos pontos atribuidos por chegar a cidade
     * @param latitude latitude da cidade
     * @param longitude longitude da cidade
     * @return 
     */
    public boolean adicionaCidade( String nome, int pontos, float latitude, float longitude){
        return this.registoCidade.adicionarCidade(nome, pontos, latitude, longitude);
    }
    
}
