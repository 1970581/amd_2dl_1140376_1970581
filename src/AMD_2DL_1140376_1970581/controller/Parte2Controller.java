/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.AdjacencyMatrixGraph;
import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.EdgeAsDoubleGraphAlgorithms;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

/**
 * Controlador para a parte 2 do trabalho.
 * @author Hugo
 */
public class Parte2Controller {
    
     private Mundo mundo;
    private RegistoCidade registoCidade;
    private RegistoUtilizador registoUtilizador;    
    private File ficheiroDistanciaCidades;    
    private String separador = ",";
    private AdjacencyMatrixGraph<Cidade, Double> graph;
    
    public Parte2Controller(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
        int nCidades = registoCidade.getMapRegistoCidade().size();  // Para evitarmos inicializarmos E[0][0] a zeros... que da erro.
        if (nCidades > 0) this.graph = new AdjacencyMatrixGraph<>(nCidades); // inicializa a matriz com o numero de cidades.
    }
    
    /**
     * Devolver o numero de cidades na base de dados.
     * @return numero de cidades existentes.
     */
    public int getNumeroCidades(){
        return registoCidade.getMapRegistoCidade().size();
    }
    
    /**
     * Devoler o numero de utilizadores existentes na base de dados
     * @return numero de utilizadores na base de dados.
     */
    public int getNumeroUtilizadores(){
        return registoUtilizador.getMapRegistoUtilizador().size();
    }
    
    /**
     * Verifica se existe um utilizador com este nomeUtilizador.
     * @param nomeUtilizador
     * @return true se existir, senao false
     */
    public boolean existeUtilizador(String nomeUtilizador){
        return (this.registoUtilizador.getUtilizador(nomeUtilizador) != null );
    }
    
    /**
     * Carrega o ficheiro de distancia entre cidades para um AdjacencyMatrixGraph.
     * @param filename nome do ficheiro.
     * @return O numero de erros. -1 Se erro grave de nao abrir ficheiro.
     */
    public int carregarFicheiroDistancias(String filename){
        int erros = 0;
        
        File ficheiro = new File(filename);
        if (!ficheiro.exists() || !ficheiro.canRead()) return -1;
        
        //Inserir os Vertices no grafico.
        for (Cidade c : registoCidade.getMapRegistoCidade().values()){
            this.graph.insertVertex(c);
        }
        
        try {
            Scanner buffer = new Scanner(ficheiro);

            while (buffer.hasNext()) {
                String aux = buffer.nextLine();
                String info[] = aux.split(this.separador);
                if (info.length == 3) {
                    String cidade1 = info[0];
                    String cidade2 = info[1];
                    String distancia = info[2];
                    Double kms = new Double(0);

                    try {
                        kms = Double.valueOf(info[2]);
                    } catch (NumberFormatException ee) {
                        continue;
                    }
                    if (kms <= 0) { erros++; continue;}
                    Cidade c1 = this.registoCidade.getCidade(cidade1);
                    if (c1 == null) { erros++; continue;}
                    Cidade c2 = this.registoCidade.getCidade(cidade2);
                    if (c2 == null) { erros++; continue;}
                    boolean pass = this.graph.insertEdge(c1, c2, kms);  //ATENCAO NAO ACEITA DUPLICADAS. E GRAFO NAO ORIENTADO.
                    if (!pass) { erros++; continue;}
                }
            }
            buffer.close();
        }
        catch(FileNotFoundException e){return -1;}
        
        return erros;
    }
    
    /**
     * Calcula o caminho mais curto de distancia entre dois utilizadores.
     * @param u1Nome nome do user 1
     * @param u2Nome nome do user 2
     * @param caminho ArrayList de String para receber o nome das cidades.
     * @return a distancia do caminho.
     */
    public Double caminhoMaisCurto(String u1Nome, String u2Nome, ArrayList<String> caminho){
        caminho.clear();
        
        Utilizador utilizador1 = this.registoUtilizador.getUtilizador(u1Nome);
        if(utilizador1 == null)return null;
        Utilizador utilizador2 = this.registoUtilizador.getUtilizador(u2Nome);
        if(utilizador2 == null)return null;
        Cidade cidade1 = utilizador1.getUltimaCidade();
        Cidade cidade2 = utilizador2.getUltimaCidade();
        if (cidade1 == null || cidade2 == null) return null;
        
        LinkedList<Cidade> path = new LinkedList<>();
        Double distancia = EdgeAsDoubleGraphAlgorithms.shortestPath(graph, cidade1, cidade2, path);
        
        for(Cidade c : path){
            caminho.add(c.getNome());
        }
        return distancia;
    }
    
    /**
     * Metodo que determina os amigos de um utilizador, que se encontram em cidades a 
     * menos de X kms de distancia.
     * Usa o metodo de Dijkstra, que antes era privado, para calcular para uma origem
     * qual a distancia para todos os nos que sao atingiveis.
     * Melhor que o Floyd-Warshall que é O(n^3) visto este ser nAmigos x O(Dijkstra) + O(n^2)
     * @param nome nome do utilizador pai
     * @param kms  distancia a procurar
     * @param resposta  lista de Strings com a resposta.
     * @return sucesso do metodo.
     */
    public boolean amigosPerto(String nome, double kms, ArrayList<String> resposta){
        //Floyd-Warshall algorithm usa O(v^3) para cosntruir uma matriz com a resposta.
        // Dijkstra's  usa menos.
        
        Utilizador pai = this.registoUtilizador.getUtilizador(nome);
        if(pai == null)return false;
        ArrayList<Utilizador>   listaAmigo  = new ArrayList();
        ArrayList<Cidade>       listaCidade = new ArrayList();
        ArrayList<Integer>      listaIndex  = new ArrayList();
        ArrayList<Double>       distancia   = new ArrayList();
        
        
        //Vamos preencher a lista com os amigos e as cidades onde se encontram e os seus indices..
        for (Utilizador amigo : pai.getRegistoAmizade().getRegistoAmizade().values()){ // O(n^2)
            listaAmigo.add(amigo);
            listaCidade.add(amigo.getUltimaCidade());
            listaIndex.add(graph.getIndex(amigo.getUltimaCidade()));  // O(n)
        }
        
        
        
        int sourceIdx = graph.getIndex(pai.getUltimaCidade());
        boolean knownVertices[] = new boolean[graph.numVertices()];   // Inicializado a zero = FALSE.
        int verticesIndex[] = new int[graph.numVertices()];           // isto vai ser a path do outro metodo.
        double minDist[] = new double[graph.numVertices()];           // Isto vai ser a distancia do vertice origem a cada um dos outros vertices.
        
        EdgeAsDoubleGraphAlgorithms.shortestPath(graph, sourceIdx, knownVertices, verticesIndex, minDist);
        
        for (int i = 0; i < listaAmigo.size(); i++){
            int index = listaIndex.get(i);
            if(knownVertices[index] && minDist[index] <= kms){//Se o vertice foi visitado e portanto existe caminho menor que distancia.
                resposta.add( listaAmigo.get(i).getNome() +
                        " em " + listaCidade.get(i).getNome() + 
                        " a " + minDist[index] + " kms."
                        );
            }
        }
        
        return true;
    }
    
    
    public Double caminhoCurtoPassandoPorAmigos(String u1Nome, String u2Nome, ArrayList<String> caminho){
        Double distancia = 0d;
        Utilizador utilizador1 = this.registoUtilizador.getUtilizador(u1Nome);
        if(utilizador1 == null)return null;
        Utilizador utilizador2 = this.registoUtilizador.getUtilizador(u2Nome);
        if(utilizador2 == null)return null;
        Cidade cidade1 = utilizador1.getUltimaCidade();
        Cidade cidade2 = utilizador2.getUltimaCidade();
        if (cidade1 == null || cidade2 == null) return null;
        
        ArrayList<Cidade> cidades = new ArrayList<>();
        
        
        for (Cidade c : getCidadesComMaisAmigos(utilizador1)){
            cidades.add(c);
        }
        for (Cidade c : getCidadesComMaisAmigos(utilizador2)){
            if(!cidades.contains(c))cidades.add(c);         //Evitar introducao de repetidas.
        }
        ArrayList<Cidade> caminhoVertices = new ArrayList<>();
        
        distancia = EdgeAsDoubleGraphAlgorithms.constrainedShortestPath(graph, cidade1, cidade2, cidades, caminhoVertices);
        
        for (Cidade c : caminhoVertices){
            caminho.add(c.getNome());
        }
        
        
        return distancia;
    }
    
    
    /**
     * Obtem um Iterable com as cidades onde o utilizador tem mais amigos.
     * @param utilizador utilizador a ver amigos em cidades.
     * @return um Iterable com as cidades onde o utilizador tem mais amigos.
     */
    public Iterable<Cidade> getCidadesComMaisAmigos(Utilizador utilizador){
        Map<Cidade, Integer> mapUtilizador = new LinkedHashMap<>();  //MAPA  
        for (Utilizador amigo : utilizador.getRegistoAmizade().getRegistoAmizade().values()){  //Iteramos os amigos
            Cidade c = amigo.getUltimaCidade();     // Obtemons a cidade
            if (mapUtilizador.containsKey(c)){         //Se ja existir a cidade no mapa aumentamos o numero de amigos em 1
                Integer aux = mapUtilizador.get(c);        
                aux++;
                mapUtilizador.put(c, aux);
            }
            else mapUtilizador.put(c, Integer.valueOf(1));     // Senao adicionamos a cidade ao mapa.
        }
        
        Set<Cidade> setCidade = new LinkedHashSet();
        if(mapUtilizador.isEmpty()) return setCidade;           // Senao existem cidades devolvemos vazio.
        Integer max = 0;
        
        Iterator it1 = mapUtilizador.entrySet().iterator();     //Procuramos o valor maximo de cidades repetidas.
        while(it1.hasNext()){
            Map.Entry<Cidade, Integer> entry = (Map.Entry) it1.next();
            if(max < entry.getValue()) max = entry.getValue();
        }
        Iterator it2 = mapUtilizador.entrySet().iterator();     //Colocar no set as cidades com numero igual a max de amigos.
        while(it2.hasNext()){
            Map.Entry<Cidade, Integer> entry = (Map.Entry) it2.next();
            if( max.equals(entry.getValue()) ) setCidade.add(entry.getKey());
        }
        return setCidade;
    }
}
