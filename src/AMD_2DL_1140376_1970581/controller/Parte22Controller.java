/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Graph;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Edge;
import AMD_2DL_1140376_1970581.GraphMapAlgorithms;

import java.util.LinkedList;

/**
 *  Controller da segunda parte Ponto 3 Map de adjacencias.
 * @author Hugo
 */
public class Parte22Controller {
    
    private Mundo mundo;
    private RegistoCidade registoCidade;
    private RegistoUtilizador registoUtilizador;    
    private Graph<Utilizador , String> graph;
    
    /**
     * Construtor do controller
     * @param mundo mundo 
     */
    public Parte22Controller(Mundo mundo){
        this.mundo = mundo;
        this.registoCidade = mundo.getRegistoCidade();
        this.registoUtilizador = mundo.getRegistoUtilizador();
        int nUtilizadores = registoUtilizador.getMapRegistoUtilizador().size();  // Para evitarmos inicializarmos E[0][0] a zeros... que da erro.
        if (nUtilizadores > 0) this.graph = new Graph<>(false); // inicializa o mapa se existem utilizadores.
        // Vamos utilizar um graph UNDIRECTED mas nao confio no Edge.equals() que diz que a edge 1->2 != 2->1. 
    }
    
    /**
     * Devoler o numero de utilizadores existentes na base de dados
     * @return numero de utilizadores na base de dados.
     */
    public int getNumeroUtilizadores(){
        return registoUtilizador.getMapRegistoUtilizador().size();
    }
    
    /**
     * Verifica se existe um utilizador com este nomeUtilizador.
     * @param nomeUtilizador
     * @return true se existir, senao false
     */
    public boolean existeUtilizador(String nomeUtilizador){
        return (this.registoUtilizador.getUtilizador(nomeUtilizador) != null );
    }
    
    /**
     * Carrega o mapa com os utilizadores e suas amizades.
     * Grafo nao direcionado, portanto da erros a inserir B->A depois de inserir A->B
     * Contamos esses erros para fazer um checksum.
     * @return numero de amigos - numero de erros , deve ser zero.
     */
    public int carregarMapa(){
        boolean sucesso;
        int erros = 0;
        int amigos = 0;
        for(Utilizador u : registoUtilizador.getMapRegistoUtilizador().values()){
            graph.insertVertex(u);
        }
        for(Utilizador u : registoUtilizador.getMapRegistoUtilizador().values()){
            for (Utilizador amigo : u.getRegistoAmizade().getRegistoAmizade().values()){
                sucesso = graph.insertEdge(u, amigo, u.getNome()+ " & " +amigo.getNome(), 1d);
                if(sucesso) amigos++; 
                else erros ++;
            }
        }
        return amigos - erros;  //Checksum por cada amigo adicionado a ligação de amizade inversa deve dar um erro.
    }
    
    /**
     * Devolve o graph map, usado nos testes.
     * @return um Graph Map das amizades do controller.
     */
    public Graph<Utilizador , String> getGraph(){return this.graph;}
    
    /**
     * Devolve a distancia relacional entre dois utilizadores.
     * @param nome1 nome do utilizador 1
     * @param nome2 nome do utilizador 2
     * @return a distancia relacional entre dois utilizadores.
     */
    public int distanciaRelacional(String nome1, String nome2){
        double distancia = 0;
        Utilizador u1 = registoUtilizador.getUtilizador(nome1);
        Utilizador u2 = registoUtilizador.getUtilizador(nome2);
        if (u2 == null || u1 == null) return -1;
        
        int nVert = this.graph.numVertices();
        boolean visited[] = new boolean[nVert];
        double[] dist = new double[nVert];
        LinkedList<Utilizador> lista = new LinkedList<>();
       
        
        GraphMapAlgorithms.shortestPath(graph, u1, u2,  lista, visited, dist);
        
        int indexU2 = graph.getKey(u2);
        
        if(visited[indexU2] = false) return -1;
        else distancia = dist[indexU2];
        Double aux = distancia;
        return aux.intValue() -1 ;  
    }
    
    /**
     * Devolve uma lista do nome dos utilizadores que se encontram a uma distancia 
     * relacional INFERIOR (nao igual) a distancia do utilizador com nome indicado.
     * @param nome nome do utilizador origem
     * @param distancia distancia relacional a verificar.
     * @return LinkedList de Strings com o nome dos utilizadores.
     */
    public LinkedList<String> utilizadoresADistanciaRelacionalX(String nome, int distancia){        
        LinkedList<String> listaString = new LinkedList<>();
        Utilizador u1 = registoUtilizador.getUtilizador(nome);
        if (u1 == null) return listaString;
        
        int nVert = this.graph.numVertices();
        boolean visited[] = new boolean[nVert];
        double[] dist = new double[nVert];
        LinkedList<Utilizador> lista = new LinkedList<>();
       
        GraphMapAlgorithms.shortestPath(graph, u1, u1,  lista, visited, dist);
        
        
        for(Utilizador u : graph.vertices()){
            int id = graph.getKey(u);
            if ( (dist[id] -1) < distancia && dist[id] > 0) listaString.add(u.getNome());  // O shortest path devolve a distancia de relacionamento +1 Porque devolve o numero de edges de caminho.
        }
        //listaString.remove(nome); //Para nao aparecer o utilizador original.
        return listaString;
    }
    
    /**
     * Escreve numa lista os utilizadores mais centrais do grafo, ou seja os mais influentes.
     * @param info lista de Strings onde recebe a informacao.
     * @return se o grafo e conexo.
     */
    public boolean utilizadoresMaisInfluentes(LinkedList<String> info){
        boolean conexo = true;
        
        Graph<Utilizador,String> transiente;
        transiente = GraphMapAlgorithms.mapTransitiveClosureMatriz(this.graph);
        
        //Vamos agora calcular qual a centralidade de cada utilizador.
        double centralidadeMin = 0; //Centralidade do grafo 
        double uCentralidade = 0;   // Centralidade de um utilizador
        
        for (Utilizador u1: transiente.vertices() ){
            uCentralidade = 0; //Reset ao valor de maior centralidade de utilizador.
            for (Utilizador u2: transiente.vertices() ){
                Edge e = transiente.getEdge(u1, u2);
                if (!u1.equals(u2)){
                    if(e == null) conexo = false;
                    else {
                        double dist = e.getWeight();
                        if(dist > uCentralidade) uCentralidade = dist;  // Actualizacao da centralidade maxima do utilizador
                    }
                }
            }
            if(centralidadeMin == 0 ) centralidadeMin = uCentralidade;      // Ainda nao existe centralidade minima, a centralidade deste utilizador é a do grafo.
                if(centralidadeMin == uCentralidade) info.add(u1.getNome());    // Se são iguais entao este utilizador é um centro, adicionamos a lista.
                if(centralidadeMin > uCentralidade){                            // Se a centralidade do utilizador for menor que a do grafo, 
                    centralidadeMin = uCentralidade;                            //    entao essa é a nova centralidade do grafo
                    info.clear();                                               //    limpamos a lista
                    info.add(u1.getNome());                                     //    e adicionamos o utilizador.
                }
        }
        
        info.add("O raio do grafo é de " + centralidadeMin + ".");
        
        return conexo;
    }
    
    
}
