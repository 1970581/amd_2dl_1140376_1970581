package AMD_2DL_1140376_1970581;
//package graph;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF && HUGO && CELSO
 */
public class EdgeAsDoubleGraphAlgorithms {

    /**
     * Determine the shortest path to all vertices from a vertex using Dijkstra's algorithm
     * To be called by public short method
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex 
     * @param knownVertices previously discovered vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param minDist minimum distances in the path
     *
     */
    public static <V> void shortestPath(AdjacencyMatrixGraph<V,Double> graph, int sourceIdx, boolean[] knownVertices, int[] verticesIndex, double [] minDist)	{  
	//METODO DEIXOU DE SER PRIVATE PARA SER PUBLIC
        //throw new UnsupportedOperationException("Not supported yet.");	*********************************  Hugo
        
        //int dist[graph.numVertices] = new int;
        double maxDist = Double.POSITIVE_INFINITY;
        int[] path = verticesIndex;
        double[] dist = minDist;
        boolean[] visited = knownVertices;
        int vOrig = sourceIdx;
        
        for (V v : graph.vertices()){
            int vIndex = graph.toIndex(v);
            minDist[vIndex] = maxDist;
            path[vIndex] = -1;
            visited[vIndex] = false;
        }
        
        dist[vOrig] = 0;
        
        while(vOrig != -1){
            visited[vOrig] = true; // vOrig -> Visited.
            V vOrigVertex = graph.vertices.get(vOrig);
            
            for(V vAdjVertex : graph.directConnections(vOrigVertex)){
                int vAdj = graph.toIndex(vAdjVertex);
                double edgeWeight = (Double) graph.getEdge(vOrigVertex, vAdjVertex); // ASUMIMOS que E é um edge representado por um double do comprimento da Edge.
                
                if(!visited[vAdj] && dist[vAdj]> dist[vOrig]+edgeWeight){
                    dist[vAdj] = dist[vOrig] + edgeWeight;
                    path[vAdj] = vOrig;
                }
            }
            
            //vOrig = getVertMinDist(dist,visited);  TRADUÇAO PARA JAVA:
            // Vamos descobrir qual o vertice ainda nao visitado cuja distancia no vector dist (ou minDist) é menor, e selecionar-lo para a proxima iteracao.
            int newVOrig = -1;        // Nao existe vertice -1, que é o criterio de paragem do ciclo. IMPORTANTE para conseguirmos para o ciclo quando nao encontramos proximo.
            double distanciaMinima = maxDist; // Vamos usar a distancia maxima como default. Visto que os que podemos escolher ja tem uma distancia pois estao adjacentes ao vertice origem. Caso nao existam adjacentes ao vertice origem, entao todos os outros estão a infinito e nunca sao selecionados.
            for (int i = 0 ; i < dist.length; i++){
                if ( knownVertices[i] == false &&      // Ainda nao visitado
                     dist[i] < distanciaMinima  ) {    // Queremos o com menor distancia ao vertice Origem inicial.
                    newVOrig = i;
                    distanciaMinima = dist[i];
                }  
            }
            vOrig = newVOrig;  // E guardamos o novo vertice origem.
                    
        }// end while
    }// end function


    /**
     * Determine the shortest path between two vertices using Dijkstra's algorithm
     *
     * @param graph Graph object
     * @param source Source vertex 
     * @param dest Destination vertices
     * @param path Returns the vertices in the path (empty if no path)
     * @return minimum distance, -1 if vertices not in graph or no path
     *
     */
    public static <V> double shortestPath(AdjacencyMatrixGraph<V, Double> graph, V source, V dest, LinkedList<V> path){
		//throw new UnsupportedOperationException("Not supported yet.");	 *************************************** HUGO
        int sourceIdx = graph.toIndex(source);
        int destIdx = graph.toIndex(dest);
        if (sourceIdx == -1 || destIdx == -1) return -1;  // Se nao existirem os vertices, devolvermos -1.
        
        path.clear(); // Vamos limpar o path. 
        boolean knownVertices[] = new boolean[graph.numVertices];   // Inicializado a zero = FALSE.
        int verticesIndex[] = new int[graph.numVertices];           // isto vai ser a path do outro metodo.
        double minDist[] = new double[graph.numVertices];                 // Isto vai ser a distancia do vertice origem a cada um dos outros vertices.
        shortestPath(graph, sourceIdx, knownVertices, verticesIndex, minDist);
        
        //Vamos ver se o vertice destino voi Iterado (visitado). Se nao foi visitado, nao existe caminho para ele
        // e vamos devolver distancia -1 e LinkedList path vazia.
        if (knownVertices[destIdx] == false) return -1;
        //Ok, se estamos qui sabemos que existe um path. Vamos construi-lo. Primeiro a distancia.
        double distanciaMinimaCalculada = minDist[destIdx];       // Para guardarmos a distancia do caminho.
        // Agora vamos colocar os vertices do caminho andando para tras no verticesIndex (path do outro metodo que chamamos) usando metodo da prof.
        recreatePath(graph, sourceIdx, destIdx, verticesIndex, path);
        // Mas agora o caminho está ao contrario ...
        
        LinkedList<V> listaAux = new LinkedList();  // Uma nova lista temporaria.
        int pathSize = path.size();
        for (int i = 0; i < pathSize ; i++){
            listaAux.addFirst(path.removeFirst());  // Copiamoas por ordem inversa para a auxiliar
        }
        for (int i = 0; i < pathSize; i++){
            path.addFirst(listaAux.removeLast());       // Copiamos na mesma ordem para a original. IMPORTANTE para mantermos a mesma referencia. O pointer da lista pode estar referenciado em outro lugar.
        }
        
        
        return distanciaMinimaCalculada;
    }


    /**
     * Recreates the minimum path between two vertex, from the result of Dikstra's algorithm
     * 
     * @param graph Graph object
     * @param sourceIdx Source vertex 
     * @param destIdx Destination vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param Queue Vertices in the path (empty if no path)
     */
    private static <V> void recreatePath(AdjacencyMatrixGraph<V, Double> graph, int sourceIdx, int destIdx, int[] verticesIndex, LinkedList<V> path){

        path.add(graph.vertices.get(destIdx));
        if (sourceIdx != destIdx){
            destIdx = verticesIndex[destIdx];        
            recreatePath(graph, sourceIdx, destIdx, verticesIndex, path);
        }
    }

    /**
     * Creates new graph with minimum distances between all pairs
     * uses the Floyd-Warshall algorithm
     * 
     * @param graph Graph object
     * @return the new graph 
     */
    public static <V> AdjacencyMatrixGraph<V, Double> minDistGraph(AdjacencyMatrixGraph<V, Double> graph){
            //throw new UnsupportedOperationException("Not supported yet.");	 ****************************   Hugo
        int n = graph.numVertices;
        
        
        // ATENÇAO NAO FUNCIONA DIRECTO POIS NAO PERMITE REINSERSAO DE EDGES.
        // TEMOS DE REMOVER A EDGE SEMPRE QUE QUEREMOS ACTUALIZAR UMA EDGE
        
        AdjacencyMatrixGraph<V,Double> gk0  = (AdjacencyMatrixGraph<V,Double> ) graph.clone();
        AdjacencyMatrixGraph<V,Double> gk1 = (AdjacencyMatrixGraph<V,Double> ) graph.clone();
        
        for (int k = 0; k < n; k++){
            gk0 = (AdjacencyMatrixGraph<V,Double> ) gk1.clone();
            
            
            for (int i = 0; i < n ; i++){
                    for (int j = 0; j < n ; j++){
                        if ( i != j && i != k && j != k){
                            
                            if (/*" k == 0 "*/false) {;}  // Do nothing.
                            else{                                
                                if (       gk0.getEdge(gk0.vertices.get(i), gk0.vertices.get(k)) != null
                                        && gk0.getEdge(gk0.vertices.get(k), gk0.vertices.get(j)) != null
                                        )
                                        {
                                            Double x = gk0.getEdge(gk0.vertices.get(i), gk0.vertices.get(k)) + gk0.getEdge(gk0.vertices.get(k), gk0.vertices.get(j));
                                            if (gk1.getEdge(gk1.vertices.get(i), gk1.vertices.get(j)) == null){
                                                
                                                boolean p1 = gk1.insertEdge( gk1.vertices.get(i) ,gk1.vertices.get(j) ,  x.doubleValue() );
                                                System.out.println( p1 + " k" + k +" i" + i + " j" + j);
                                            }                                                                                     
                                            else{ 
                                                if ( gk1.getEdge(gk1.vertices.get(i), gk1.vertices.get(j)) > x){
                                                    System.out.print( gk1.getEdge(gk1.vertices.get(i), gk1.vertices.get(j)));
                                                    gk1.removeEdge( gk1.vertices.get(i) ,gk1.vertices.get(j));
                                                    boolean p1 = gk1.insertEdge( gk1.vertices.get(i) ,gk1.vertices.get(j) ,  new Double(  x));
                                                    System.out.println( "->" + x);
                                                    //System.out.println( p1 + " k" + k +" i" + i + " j" + j);
                                                }
                                            }
                                        }
                            }
                        }
                    }
            }
        }
        

        return gk1; 
        
    }
    
    
    /**
     * Metodo Custom de Constrained Shortest Path, pag 60 dos apontamentos.
     * Metodo para calcular o camino mais curto entre dois vertices passando por um conjuto de outros vertices.
     * Metodo recorre a Heuristicas. Nao devolver o mais curto mas um candidato a mais curto.
     * Chama o shortestPath() para calcular o mais caminho mais curto entre 2 vertices (Dijkstra's algorithm)
     * @param <V>    Vertices
     * @param graph   Matriz de Adjacencias com Edges como Double
     * @param voInf   Vertice origem  inicial
     * @param vdInf   Vertice destino final
     * @param reqVerts  Vertices que devem ser atravessados pelo caminho (Restricoes)
     * @param shortPath Caminho mais curto devolvido, um ArrayList de Vertices.
     * @return um Double com a distancia do caminho.
     */
    public static<V> Double  constrainedShortestPath(AdjacencyMatrixGraph<V, Double> graph, V voInf, V vdInf, ArrayList<V> reqVerts, ArrayList<V> shortPath){
        double minDistance = 0d;
        shortPath.add(voInf);  // Adicionamos logo o vertice origem pk durante o algoritmo, apagamos os primeiro vertice de todos os subcaminhos para evitar duplicados.
        // Visto calcular mos ex: Porto -> Madrid , Madrid -> Lisboa ; entao Madrid ia aparecer 2 vezes se nao removessemos o primeiro vertice dos subcaminhos.
        
        
        
        
        
        while (!reqVerts.isEmpty()){ // Enquanto nao passarmos por todos os vertices requeridos.
            Double minimun = Double.POSITIVE_INFINITY;
            V minVertex = null;
            LinkedList<V> minSubpath = new LinkedList<>();
            for(V vert : reqVerts){  // Calculamos qual dos vertices requeridos é o maix proximo e o seu caminho.
                LinkedList<V> subpath = new LinkedList<>();
                double aux = shortestPath(graph, voInf, vert, subpath);
                if(aux != -1 && aux < minimun ){ //Novo minimo.
                    minimun = aux;
                    minSubpath = subpath;
                    minVertex = vert;
                }
            }
            if (minimun.equals(Double.POSITIVE_INFINITY)) return -1d; // Se nao encontrarmos caminho para um dos requeridos, entao nao existe caminho.
            
            minDistance += minimun;     // Incrementamos a distancia.
            reqVerts.remove(minVertex); // Removermos o vertice processado dos requeridos
            minSubpath.removeFirst();   // REMOVER O PRIMEIRO V QUE ESTA REPETIDO!!!!
            for(V v : minSubpath){      // Colamos o caminho no fim de shortPath.
                shortPath.add(v);
            }
            voInf = minVertex;          // Colocamos o vertice processaado como vertice origem para 
        }
        // Ja processamos todos os vertices requeridos, vamos processar o vertice destino.
        LinkedList<V> subpath = new LinkedList<>();
        double aux = shortestPath(graph, voInf, vdInf, subpath);
        if (aux == -1d) return -1d; // Se nao encontrarmos caminho para o final, entao nao existe caminho.
        minDistance += aux;
        subpath.removeFirst();   // REMOVER O PRIMEIRO V QUE ESTA REPETIDO!!!!
        for(V v : subpath){      // Colamos o caminho no fim de shortPath.
            shortPath.add(v);
        }
        
        
        return minDistance;
    }
    
    
    
}
