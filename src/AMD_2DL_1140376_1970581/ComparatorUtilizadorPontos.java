/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import java.util.Comparator;

/** Classe Comparator usada para comparar pontos de utilizadores.
 * Previsto usar para ordernar Arraylist de Mayors em Cidades.
 *
 * @author 
 */
public class ComparatorUtilizadorPontos implements Comparator{

    /**
     * Compara utilizadores baseados em pontos.
     * @param o1
     * @param o2
     * @return 
     */
    @Override
    public int compare(Object o1, Object o2) {
        Utilizador u1 = (Utilizador) o1;
        Utilizador u2 = (Utilizador) o2;
        return u1.getPontos() - u2.getPontos();
        
    }
    
}
