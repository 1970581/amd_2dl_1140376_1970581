/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
/**
 *
 * @author Celso
 */
public class RegistoAmizade {
    private Map <String, Utilizador> mapRegistoAmizade;
    
    public RegistoAmizade(){
     this.mapRegistoAmizade = new LinkedHashMap<>();
    }
    /**
     * metodo que verifica se o utilizador passado por parametro já é amigo do utilizador em questao
     * @param amigo utilizador amigo
     * @return true se ja for amigo, falso caso tal nao aconteca
     */
    public boolean isAmigo(Utilizador amigo){
    if(this.mapRegistoAmizade.containsValue(amigo)) return true;
    return false;
    }
    /**
     * Metodo que adiciona relação de amizade entre o utilizador em questao e o utilizador passado por parametro
     * @param amigo utilizador que ira ser adicionado como amigo
     * @return true 
     */
    public boolean adicionarAmizade(Utilizador amigo){
        if (this.mapRegistoAmizade.containsValue(amigo)) return false;
        mapRegistoAmizade.put(amigo.getNome(), amigo);    
        return true;
    }
    
   public boolean removerAmizade(Utilizador amigo){
       if(!this.mapRegistoAmizade.containsValue(amigo)) return false;
       mapRegistoAmizade.remove(amigo.getNome(), amigo);
       return true;
   }
   
   public Utilizador getAmigo(String amigo){
    return this.mapRegistoAmizade.get(amigo);
    }
   
   public int getNumeroDeAmigos(){
       return mapRegistoAmizade.size();
   }
   
   public Map <String, Utilizador> getRegistoAmizade(){
       return this.mapRegistoAmizade;
   }
    
    // 1º verificar se chaves existe, se nao existir adicionar e criar set.
    //2º ir buscar o set associado a chaves e colocar dentro o amigo
}
