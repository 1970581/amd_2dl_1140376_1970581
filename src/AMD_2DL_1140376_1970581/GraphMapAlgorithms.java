/*
* A collection of graph algorithms.
*/
package AMD_2DL_1140376_1970581;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */

public class GraphMapAlgorithms {
    
   /**
   * Performs breadth-first search of a Graph starting in a Vertex 
   * @param g Graph instance
   * @param vInf information of the Vertex that will be the source of the search
   * @return qbfs a queue with the vertices of breadth-first search 
   */
    public static<V,E> LinkedList<V> BreadthFirstSearch(Graph<V,E> g, V vert){
    
        if (!g.validVertex(vert)) 
           return null; 
        
        LinkedList<V> qbfs = new LinkedList<>(); 
        LinkedList<V> qaux = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices()];  //default initializ.: false

        qbfs.add(vert);
        qaux.add(vert);
        int vKey=g.getKey(vert);
        visited[vKey]=true;
       
        while(!qaux.isEmpty()){
            vert=qaux.remove(); 
            for (Edge<V,E> edge : g.outgoingEdges(vert)){                
                V vAdj = g.opposite(vert, edge);
                vKey = g.getKey(vAdj);
                if (!visited[vKey]){
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                    visited[vKey]=true;
                }
            }
        }
        return qbfs;
    }
   
   /**
   * Performs depth-first search starting in a Vertex   
   * @param g Graph instance
   * @param vOrig Vertex of graph g that will be the source of the search
   * @param visited set of discovered vertices
   * @param qdfs queue with vertices of depth-first search
   */
    private static<V,E> void DepthFirstSearch(Graph<V,E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs){
          //throw new UnsupportedOperationException("Not supported yet.");   ************************************* CUSTOM
        qdfs.add(vOrig);
        visited[g.getKey(vOrig)] = true;
        for (Edge<V,E> edge : g.outgoingEdges(vOrig)){
            if (visited[g.getKey(edge.getVDest())] != true){
                DepthFirstSearch(g, edge.getVDest(), visited, qdfs);
            }
        }
        
        
    }  
  
   /**
    * DepthFirstSearch
   * @param g Graph instance
   * @param vInf information of the Vertex that will be the source of the search
   * @return qdfs a queue with the vertices of depth-first search 
   */
    public static<V,E> LinkedList<V> DepthFirstSearch(Graph<V,E> g, V vert){
            //throw new UnsupportedOperationException("Not supported yet."); ************************** CUSTOM
        
        boolean visited[] = new boolean[g.numVertices()]; // incializado a false
        LinkedList<V> qdfs = new LinkedList<>();
        if (!g.validVertex(vert)) return null;
        
        DepthFirstSearch(g, vert, visited, qdfs);
        
        
        
        return qdfs; 
    }
   
    /**
   * Returns all paths from vOrig to vDest
   * @param g Graph instance
   * @param vOrig Vertex that will be the source of the path
   * @param vDest Vertex that will be the end of the path
   * @param visited set of discovered vertices
   * @param path stack with vertices of the current path (the path is in reverse order)
   * @param paths ArrayList with all the paths (in correct order)
   */
    private static<V,E> void allPaths(Graph<V,E> g, V vOrig, V vDest, boolean[] visited, 
                                           LinkedList<V> path, ArrayList<LinkedList<V>> paths){
  
        //throw new UnsupportedOperationException("Not supported yet.");  *********************** CUSTOM
        visited[g.getKey(vOrig)] = true;
        
        path.add(vOrig);
        
        for(Edge<V,E> edge :g.outgoingEdges(vOrig)){
            V vert = edge.getVDest();
            if(vert.equals(vDest)){
                path.add(vDest);
                //LinkedList<V> path2 ;
                //path2 =  path;//.clone();
                //paths.add(path2);
                paths.add( (LinkedList<V>) path.clone());
                path.removeLast();
            }
            else{
                if(!visited[g.getKey(vert)]){
                    allPaths(g, vert, vDest, visited, path, paths);
                }
            }
        }
        visited[g.getKey(vOrig)] = false;
        path.removeLast();
    }
    
   /**
    * Metodo para determinar todos os caminhos.
   * @param g Graph instance
   * @param voInf information of the Vertex origin
   * @param vdInf information of the Vertex destination 
   * @return paths ArrayList with all paths from voInf to vdInf 
   */
    public static<V,E> ArrayList<LinkedList<V>> allPaths(Graph<V,E> g, V vOrig, V vDest){
            //throw new UnsupportedOperationException("Not supported yet.");******************** Custom
        ArrayList<LinkedList<V>> paths = new ArrayList<>();
        if( !g.validVertex(vOrig) || !g.validVertex(vDest)) return paths;
        
        boolean visited[] = new boolean[g.numVertices()];  // initcializado a false
        LinkedList<V> path = new LinkedList<>();
        
        
        allPaths(g, vOrig, vDest, visited, path, paths);
        
        return paths;
    }
    
    /**
   * Computes shortest-path distance from a source vertex to all reachable 
   * vertices of a graph g with nonnegative edge weights
   * This implementation uses Dijkstra's algorithm
   * @param g Graph instance
   * @param vOrig Vertex that will be the source of the path
   * @param visited set of discovered vertices
   * @param pathkeys minimum path vertices keys  
   * @param dist minimum distances
   */
    private static<V,E> void shortestPathLength(Graph<V,E> g, V vOrig, V[] vertices,
                                    boolean[] visited, int[] pathKeys, double[] dist){     
        //throw new UnsupportedOperationException("Not supported yet."); ****************** CUSTOM  /Hugo/Celso
        
        for (V vert : vertices){
            int vertId = g.getKey(vert);
            dist[vertId] = Double.POSITIVE_INFINITY;
            pathKeys[vertId] = -1;
            visited[vertId] = false;
        }
        dist[g.getKey(vOrig)] = 0;
        while(vOrig != null){// dfdsdds******
            int vOrigId = g.getKey(vOrig);
            visited[vOrigId] = true;
            for(Edge<V,E> edge : g.outgoingEdges(vOrig)){
                int vAdjId = g.getKey(edge.getVDest());
                if( !visited[vAdjId] && ( (dist[vAdjId]) > (dist[vOrigId] + edge.getWeight() )) ){
                    dist[vAdjId] = dist[vOrigId] + edge.getWeight();
                    pathKeys[vAdjId] = vOrigId;
                }
            }
            
            //vOrig = getVertMinDist(dist,visited);  TRADUÇAO PARA JAVA:
            // Vamos descobrir qual o vertice ainda nao visitado cuja distancia no vector dist (ou minDist) é menor, e selecionar-lo para a proxima iteracao.
            vOrig = null;        // Nao existe vertice null, que é o criterio de paragem do ciclo. IMPORTANTE para conseguirmos para o ciclo quando nao encontramos proximo.
            double distanciaMinima = Double.POSITIVE_INFINITY; // Vamos usar a distancia maxima como default. Visto que os que podemos escolher ja tem uma distancia pois estao adjacentes ao vertice origem. Caso nao existam adjacentes ao vertice origem, entao todos os outros estão a infinito e nunca sao selecionados.
            for (V ver : vertices){
                int vId = g.getKey(ver);
                if ( visited[vId] == false &&      // Ainda nao visitado
                     dist[vId] < distanciaMinima  ) {    // Queremos o com menor distancia ao vertice Origem inicial.
                    vOrig = ver;
                    distanciaMinima = dist[vId];
                }
            }
        }
        
    }
    
    /**
    * Extracts from pathKeys the minimum path between voInf and vdInf
    * The path is constructed from the end to the beginning
    * @param g Graph instance
    * @param voInf information of the Vertex origin
    * @param vdInf information of the Vertex destination 
    * @param pathkeys minimum path vertices keys  
    * @param path stack with the minimum path (correct order)
    */
    private static<V,E> void getPath(Graph<V,E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path){
           //throw new UnsupportedOperationException("Not supported yet.");  **************************************** Custom Hugo/Celso
        //path.clear();
        //if (pathKeys[g.getKey(vDest)] == -1) return;   //Desnecessario, bloquei colocar a cidade partida na lista.
        int vOrigId = g.getKey(vOrig);
        int vDestId = g.getKey(vDest);
        
        int prevId = pathKeys[vDestId]; 
        V prevV = null;
        for (V v : verts){
            if (g.getKey(v) == prevId) prevV = v;
        }
        path.add(vDest);
        if (!vOrig.equals(vDest)) getPath(g,vOrig, prevV, verts, pathKeys,path);
       
    }

    //shortest-path between voInf and vdInf
    /**
     * shortest-path between voInf and vdInf  Determine the shortest path between two vertices using Dijkstra's algorithm
     * @param <V> vertice
     * @param <E> edge
     * @param g grafo
     * @param vOrig vertice origem
     * @param vDest vertice destino
     * @param shortPath caminho
     * @return  a distancia sob double
     */
    public static<V,E> double shortestPath(Graph<V,E> g, V vOrig, V vDest, LinkedList<V> shortPath){
              //throw new UnsupportedOperationException("Not supported yet.");   ******************************** CUSTOM Hugo/Celso
        
        if( !g.validVertex(vOrig) || !g.validVertex(vDest)) return -1d;
        
        int nVert = g.numVertices();
        
        V[] vertices = (V[]) g.allkeyVerts().clone();  // = (V[])new Object[nVert];
        boolean visited[] = new boolean[nVert];
        int[] pathKeys = new int[nVert];
        double[] dist = new double[nVert];
        
        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);    // Fazer o Djirsk.
        shortPath.clear();                                                  // Limpar a lista
        if(!visited[g.getKey(vDest)]) return -1d;                           // Verificamos se visitou o destino, se nao, nao existe caminho.
        getPath(g, vOrig, vDest, vertices, pathKeys, shortPath);            // Preencher a lista dos caminhos.
        
        LinkedList<V> pathInOrder = revPath(shortPath);
        shortPath.clear();
        while(!pathInOrder.isEmpty()) shortPath.add(pathInOrder.removeFirst()); // Mover o caminho para o objecto/referencia recebida originalmente.
        
        int vDestId = g.getKey(vDest);
        if (!visited[vDestId]) return -1d;
        
        return dist[vDestId];
    }
   
    /**
     * Reverses the path
     * @param path stack with path
     */
    private static<V,E> LinkedList<V> revPath(LinkedList<V> path){ 
   
        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();
        
        while (!pathcopy.isEmpty())
            pathrev.push(pathcopy.pop());
        
        return pathrev ;
    } 
    
    /**
     * Metodo Sobrecarregado para acceder ao privado copy pastes do Original
     * @param <V> Vertice
     * @param <E> Edge
     * @param g   grafo Map adjacencias
     * @param vOrig  vertice origem
     * @param vDest  vertice destino
     * @param shortPath LinkedList de Vertices vazia
     * @param visited array boolean - apos o metodo pode-se ver aqui os visitados (true).
     * @param dist array double - apos o metodo pode-se ler aqui a distancias.
     * @return 
     */
    public static<V,E> double shortestPath(Graph<V,E> g, V vOrig, V vDest, LinkedList<V> shortPath, boolean visited[],double[] dist  ){
              //throw new UnsupportedOperationException("Not supported yet.");   ******************************** CUSTOM Hugo/Celso
        
        if( !g.validVertex(vOrig) || !g.validVertex(vDest)) return -1d;
        
        int nVert = g.numVertices();
        
        V[] vertices = (V[]) g.allkeyVerts().clone();  // = (V[])new Object[nVert];
        //boolean visited[] = new boolean[nVert];
        int[] pathKeys = new int[nVert];
        //double[] dist = new double[nVert];
        
        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);    // Fazer o Djirsk.
        shortPath.clear();                                                  // Limpar a lista
        if(!visited[g.getKey(vDest)]) return -1d;                           // Verificamos se visitou o destino, se nao, nao existe caminho.
        getPath(g, vOrig, vDest, vertices, pathKeys, shortPath);            // Preencher a lista dos caminhos.
        
        LinkedList<V> pathInOrder = revPath(shortPath);
        shortPath.clear();
        while(!pathInOrder.isEmpty()) shortPath.add(pathInOrder.removeFirst()); // Mover o caminho para o objecto/referencia recebida originalmente.
        
        int vDestId = g.getKey(vDest);
        if (!visited[vDestId]) return -1d;
        
        return dist[vDestId];
    }
  
    //Transitive closure
    /**
     * Transitive closure com recurso a Floyd-Warshal Algorithm em MAP de adjacencias. Custom Hugo/Celso
     * @param <V> vertice
     * @param <E> edge
     * @param g grafo map de adkacencias
     * @return  um grafo com um Map da Transitive closure.
     */
    public static<V,E> Graph<V,E> mapTransitiveClosure(Graph<V,E> g){
        LinkedList<V> vertices = new LinkedList<>(); //Referencia para obter os vertices mais rapidadamente por index.
        for (V vert : g.allkeyVerts()){
            vertices.add(vert);
        }
        int n = vertices.size();      // n = numero de vertices
        
        Graph <V,E> g0 = g.clone();
        Graph <V,E> g1 = g.clone();
        
        for (int k = 0; k < n; k++){
            g0 = g1.clone();
            System.out.println("A processar vertice " + k);
            
            for (int i = 0; i < n ; i++){ 
                    for (int j = 0; j < n ; j++){
                        if ( i != j && i != k && j != k){
                            V vi = vertices.get(i);
                            V vj = vertices.get(j);
                            V vk = vertices.get(k);
                            Edge eij = g0.getEdge(vi, vj);
                            Edge eik = g0.getEdge(vi, vk);
                            Edge ekj = g0.getEdge(vk, vj);   
                            if(eik != null && ekj != null){
                                if(eij == null) {
                                    g1.insertEdge(vi, vj, (E) eik.getElement() , eik.getWeight() + ekj.getWeight() );
                                }
                                else{
                                    if( eij.getWeight() > eik.getWeight() + ekj.getWeight()){
                                        g1.removeEdge(vi, vj);
                                        g1.insertEdge(vi, vj, (E) eij.getElement(), eik.getWeight() + ekj.getWeight());
                                    }
                                }
                            }
                        }
                    }
            }
        }
        
        
        return g1;
    }
    
    //Transitive closure MATRIZ NAO MAP reduz tempo de 5 min (map) para 2 seg (matriz) ao processar 300 users.
    /**
     * Transitive closure com recurso a Floyd-Warshal Algorithm em Matriz de adjacencias. Custom Hugo/Celso
     * @param <V> vertice
     * @param <E> edge
     * @param g grafo map de adkacencias
     * @return  um grafo com um Map da Transitive closure.
     */
    public static<V,E> Graph<V,E> mapTransitiveClosureMatriz(Graph<V,E> g){
        LinkedList<V> vertices = new LinkedList<>(); //Referencia para obter os vertices mais rapidadamente por index.
        for (V vert : g.allkeyVerts()){
            vertices.add(vert);
        }
        int n = vertices.size();      // n = numero de vertices
        E dummyElement = null;
        
        for (Edge e : g.edges()){
            dummyElement = (E) e.getElement();
            if (dummyElement != null) break;  // mal uma nao seja nula saimos. Apenas para nao usarmos uma edge nula como dummy.
        }
        
        
        Double matriz[][] = new Double[n][n];
        
        /* Criar uma matriz replica do map */
        int a = 0;
        int b = 0;
        for(V v1 :vertices){
            b = 0;
            for(V v2 : vertices){
                Edge e = g.getEdge(v1, v2);
                if (e != null) { matriz[a][b] = e.getWeight(); }
                    b++;
            }
            a++;
        }
        
        Double m0[][] = matriz.clone();
        Double m1[][] = matriz.clone();
        
        for (int k = 0; k < n; k++){
            m0 = m1.clone();
            for (int i = 0; i < n ; i++){
                       for (int j = 0; j < n ; j++){
                           if ( i != j && i != k && j != k){
                               if (m0[i][k] != null && m0[k][j] != null){
                                   if (m0[i][j] == null){
                                       m1[i][j] = m0[i][k].doubleValue() + m0[k][j].doubleValue();
                                   }
                                   else {
                                       if( m0[i][j] > m0[i][k] + m0[k][j] ){
                                           m1[i][j] = m0[i][k].doubleValue() + m0[k][j].doubleValue();
                                       }
                                   }
                               }
                           }
                       }
            }
        }
        
        Graph <V,E> gReturn = g.clone();
        
        a = 0;
        b = 0;
        for(V v1 :vertices){
            b = 0;
            for(V v2 : vertices){
                gReturn.removeEdge(v1, v2);
                if (matriz[a][b] != null) gReturn.insertEdge(v1, v2, dummyElement, matriz[a][b].doubleValue());
                
                //Edge e = g.getEdge(v1, v2);  // DEBUG
                //if (e != null) { matriz[a][b] = e.getWeight(); }  /DEBUG
                b++;
            }
            a++;
        }
        
        return gReturn;
    }
    
}