/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/** Classe CIDADE que contem os dados e modela determinada cidade.
 *
 * @author 
 */
public class Cidade implements Comparable {
    
    private String nome;
    private int pontos;
    private float latitude;
    private float longitude;
    private int numeroMaximoDeVisitas;     // Numero de visitas a esta cidade do utilizador que mais vezes visitou esta cidade.
    
    public static float RAIO_CIDADE = 10000;  // Numero medio de raio de uma cidade em m
    public static String NOME_DEFAULT = ""; //Nome para criacao de cidade default. 
    public static int PONTOS_DEFAULT_ZERO = 0; //Pontos para a para criacao de cidade default. 
    public static float COORDENADAS_DEFAULT = 0f; //Latitude e longitude para criacao de cidade default. 
    private Utilizador utilizadorMayor;         // MAYOR da cidade.
    
    /**
     * Construtor sem parametros, usado para construir uma cidade fantasma.
     */
    public Cidade(){
        this.nome = Cidade.NOME_DEFAULT;
        this.pontos = Cidade.PONTOS_DEFAULT_ZERO;
        this.latitude = Cidade.COORDENADAS_DEFAULT;
        this.longitude = Cidade.COORDENADAS_DEFAULT;
        this.numeroMaximoDeVisitas = Cidade.PONTOS_DEFAULT_ZERO;
        
    }
    
    /**
     * Construtor de cidade
     * @param nome nome da cidade
     * @param pontos pontos a atribuir ao visitar
     * @param latitude latitude da cidade
     * @param longitude longitude da cidade
     */
    public Cidade(String nome, int pontos, float latitude, float longitude){
        this.nome = nome;
        this.pontos = pontos;
        this.latitude = latitude;
        this.longitude = longitude;
        this.numeroMaximoDeVisitas = 0;
        
    }
            
    public String getNome(){ return this.nome;}
    public int getPontos(){ return this.pontos;}
    public float getLatitude(){ return this.latitude;}
    public float getLongitude(){ return this.longitude;}
    
    
    
    
    /** Indica a cidade que um novo utilizador se moveu para a mesma, e que se tem de actualizar o Mayor.
     * 
     * @param utilizador 
     */
    public void utilizadorMoveuParaCidadeActualizarMayor(Utilizador utilizadorVisitante){
        
        //Calculamos o numero de visitas deste utilizador a esta cidade.
        int numeroVisitasDoUtilizadorVisitante = utilizadorVisitante.calcularNumeroDeVisitasACidade(this); 
        
        //Se for zero saimos. Utilizado quando recalculamos apos apagar utilizador.
        if(numeroVisitasDoUtilizadorVisitante == 0) return; 
        
        if(numeroVisitasDoUtilizadorVisitante > this.numeroMaximoDeVisitas){  //Se o novo tiver mais visitas fica mayor.
            this.utilizadorMayor = utilizadorVisitante;
            this.numeroMaximoDeVisitas = numeroVisitasDoUtilizadorVisitante;
        }
        // Se teem o mesmo numero, entao vamos compara os utilizadores, e o Maior fica mayor. Compara os nomes lexicographically.
        if(  numeroVisitasDoUtilizadorVisitante == this.numeroMaximoDeVisitas && utilizadorVisitante.compareTo(utilizadorMayor) > 0 ){
            this.utilizadorMayor = utilizadorVisitante;
        }
    }
    
    /**
     * Metodo para fazer reset ao mayor e ao numero de visitas do utilizador que mais visitou a cidade. 
     * Utilizado ao apagar um utilizador.
     */
    public void resetMayorENumeroMaxVisitasANull(){
        this.utilizadorMayor = null;
        this.numeroMaximoDeVisitas = 0;
    }
    
    
    /**
     * Verifica se a cidade se encontra no raio das coordenadas
     * @param latitude - latitude a verificar
     * @param longitude - longitude a verificar
     * @param raio - raio no qual procurar
     * @return verdadeiro se as coordenadas estiverem dentro do raio da cidade, falso caso contrario
     */
    public boolean verificaSeCidadeEncontraNoRaio(float latitude, float longitude, float raio){
        float distancia = Utils.calcularDistanciaEntreDuasCoordenadas(this.latitude, this.longitude, latitude, longitude);
        return (distancia <= raio);
    }
    
    
    /**
     * Metodo equals, compara apenas por nome.
     * @param obj objecto a comparar
     * @return true se as cidades tiverem o mesmo nome ou a mesma referencia.
     */
    @Override
    public boolean equals(Object obj){
        if (obj == null) return false;
        if (this == obj) return true;
        if ( !( obj instanceof Cidade) ) return false;
        Cidade that = (Cidade)obj;
        if (this.getNome().equals(that.getNome())) return true;
        else return false;
        
    }
    
    /**
     *  Hash code baseado no hash code do nome String. Objects.hashCode(this.nome)
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.nome);
        return hash;
    }
    
    /** Devolve o Mayor da cidade.
     *  Ordena a lista de mayors e devolve o do topo. Null se vazia. 
     * @return Utilizador que e o mayor
     */
    public Utilizador getMayor(){
        return this.utilizadorMayor;
    }
    
    /**
     * Comapara 2 cidades, usando o compareTo do String Nome
     * @param cidade a comparar com this.
     * @return +1 se maior, 0 se igual, -1 se menor.
     */
    @Override
    public int compareTo(Object cidade) {
        Cidade that = (Cidade) cidade;
        return this.getNome().compareTo(that.getNome());
    }
    
    /**
     * Devolve o nome da cidade.
     * @return nome da cidade.
     */
    @Override
    public String toString(){
        return this.nome;
    }

    
}

/*    Codigo Antigo já NAO USADO */

/*
public void utilizadorMoveuParaCidadeActualizarMayor(Utilizador utilizador){
        UtilizadorVisitante visitante;
        visitante = this.mapUtilizadoresQueVisitaram.get(utilizador.getNome());
        //Se ainda nao existe, o visitante esta a null.
        if(visitante == null) {
            visitante = new UtilizadorVisitante(utilizador);
            this.mapUtilizadoresQueVisitaram.put(utilizador.getNome(), visitante);
        }
        visitante.incrementarVisitasPorUm();
        
        if (numeroMaximoDeVisitas < visitante.getNumeroVisitas()){
            this.numeroMaximoDeVisitas++;
            this.listaMayor.clear();
            this.listaMayor.add(utilizador);
        }
        else {
            if (numeroMaximoDeVisitas == visitante.getNumeroVisitas()) this.listaMayor.add(utilizador);
        }
    }

public Utilizador getMayor(){

        if (this.listaMayor.isEmpty()) return null;             // ArrayList.Sort usa mergeSort, de ordem N.log_N .
        this.listaMayor.sort(new ComparatorUtilizadorPontos()); // Ordena a lista de mayors por ordem crescente de pontos.
        return this.listaMayor.get(this.listaMayor.size()-1);  // Retorna da lista de Mayors o que tem mais pontos.
    }



    /**
     * Classe privada que cria um objecto que associa o Obj Utilizador ao numero
     * de vezes que visitou a cidade onde esta guardado. Usado para guardar no map de visitantes.
     *
    private class UtilizadorVisitante{
        
        private Utilizador utilizador;
        private int numeroVisitas;
        
        public UtilizadorVisitante(Utilizador utilizador){
            this.utilizador = utilizador;
            this.numeroVisitas = 0;
        }
        
        public Utilizador getUtilizador(){return this.utilizador;}
        public int getNumeroVisitas(){ return this.numeroVisitas;}
        public void incrementarVisitasPorUm() {this.numeroVisitas++;}  
        
        @Override
        public boolean equals(Object obj){
            if (obj == null) return false;
            if (this == obj) return true;
            if ( !( obj instanceof UtilizadorVisitante) ) return false;
            UtilizadorVisitante that = (UtilizadorVisitante)obj;
            if (this.getUtilizador().equals(that.getUtilizador())) return true;
            else return false;
        }

        /** hash code baseado apenas no utilizador.hashCode()
         * 
         * @return 
         *
        @Override
        public int hashCode() {
            int hash = 7;
            //hash = 97 * hash + Objects.hashCode(this.utilizador);
            hash = 97 * hash + this.utilizador.hashCode();
            return hash;
        }
        
    } //Fim de classe privada
    
    // Nao escrever metodos aqui, deixar a classe privada ser a ultima.




*/

