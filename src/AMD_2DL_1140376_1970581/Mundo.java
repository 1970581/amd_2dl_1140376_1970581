/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/** Classe MAE que agrega os o Registo Utilizador e Registo Cidade.
 *
 * @author 
 */
public class Mundo {
    /**
     * Registo de cidades, classe RegistoCidade
     */
    private RegistoCidade cidades;
    /**
     * Registo de Utilizadores, classe RegistoUtilizadores
     */
    private RegistoUtilizador utilizadores;
    
    /**
     * Construtor sem parametros
     */
    public Mundo(){
        this.cidades = new RegistoCidade();
        this.utilizadores = new RegistoUtilizador();
    }
    /**
     * get o RegistoCidade
     * @return RegistoCidade
     */
    public RegistoCidade getRegistoCidade(){return this.cidades;};
    /**
     * get o RegistoUtilizador
     * @return RegistoUtilizador
     */
    public RegistoUtilizador getRegistoUtilizador(){return this.utilizadores;};
    
        
}
