/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.AdicionarCidadeController;
import java.util.Scanner;

/**
 * UI da opcao de addicionar uma nova cidade ao programa.
 * @author 
 */
public class AdicionarCidadeUI {
    
    /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    AdicionarCidadeController controller;
    
    /**
     * Construtor
     * @param mundo 
     */
    public AdicionarCidadeUI(Mundo mundo){
        this.mundo = mundo;
        controller = new AdicionarCidadeController(mundo);
    }
    
    /**
     * Modulo principal do UI.
     */
    public void run(){
        
        Scanner scannerLeitura = new Scanner(System.in);
        String nomeCidade = "";
        int pontosCidade  = 0;
        float latitudeCidade = 0f;
        float longitudeCidade = 0f;
        
        System.out.println("\nCRIACAO DE MOVA CIDADE");
        System.out.print("Nome: ");
        nomeCidade = scannerLeitura.nextLine();
        if (nomeCidade.equals("") || controller.existeCidadeComNome(nomeCidade)){
            System.out.println("Nome invalido ou duplicado.");
            return;
        }
        
        System.out.print("Numero de pontos: ");
        String stringPontosCidade = scannerLeitura.nextLine();
        try{
            pontosCidade = Integer.parseInt(stringPontosCidade);
        }
        catch (NumberFormatException e){
            System.out.println("Numero invalido ou desformatado.");
            return;
        }
        
        System.out.print("Latitude: ");
        String stringLatitudeCidade = scannerLeitura.nextLine();
        try{
            latitudeCidade = Float.parseFloat(stringLatitudeCidade);
        }
        catch (NumberFormatException e){
            System.out.println("Numero invalido ou desformatado.");
            return;
        }
        
        System.out.print("Longitude: ");
        String stringLongitudeCidade = scannerLeitura.nextLine();
        try{
            longitudeCidade = Float.parseFloat(stringLongitudeCidade);
        }
        catch (NumberFormatException e){
            System.out.println("Numero invalido ou desformatado.");
            return;
        }
        
        boolean sucesso = this.controller.adicionaCidade(nomeCidade, pontosCidade, latitudeCidade, longitudeCidade);
        
        if (sucesso){
            System.out.println("Nova cidade adicionada com sucesso.");
        }
        else{
            System.out.println("Erro ao criar cidade.");
        }
        
    }
    
    
}
