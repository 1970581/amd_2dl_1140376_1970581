/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.AdicionarLigacaoAmizadeController;
import java.util.Scanner;

/**
 *
 * @author i140376
 */
public class AdicionarLigacaoAmizadeUI {

    /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    AdicionarLigacaoAmizadeController controller;

    /**
     * Construtor
     *
     * @param mundo
     */
    public AdicionarLigacaoAmizadeUI(Mundo mundo) {
        this.mundo = mundo;
        controller = new AdicionarLigacaoAmizadeController(mundo);
    }

    /**
     * Modulo principal do UI.
     */
    public void run() {
        Scanner scannerLeitura = new Scanner(System.in);
        String nomeUtilizadorAmigo = "";
        String nomeUtilizador = "";

        System.out.println("\nAdicionar amizade entre utilizadores");
        System.out.print("Nome de utilizador: ");
        nomeUtilizador = scannerLeitura.nextLine();
        if (nomeUtilizador.equals("") || (!controller.isUtilizador(nomeUtilizador))) {
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        System.out.println("\nQual o nome do amigo a adicionar?");
        nomeUtilizadorAmigo = scannerLeitura.nextLine();
        if (nomeUtilizadorAmigo.equals("") || !controller.isUtilizador(nomeUtilizadorAmigo)) {
            System.out.println("Nome invalido ou existente");
        }else if (controller.isAmigo(nomeUtilizador, nomeUtilizadorAmigo)){
            System.out.println("A relação de amizade já existe");
            return;
        }
        boolean sucesso;
        sucesso = controller.criarAmizade(nomeUtilizador, nomeUtilizadorAmigo);
        if(sucesso){
            System.out.println("A operação de adicionar amizade entre os dois utilizadores foi bem sucedida");
        }else
            System.out.println("ERRO na adição de amizade entre os dois utilizadores");
    }
}
