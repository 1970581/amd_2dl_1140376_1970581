/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.DevolverPontosECidadesVisitadasDeUtilizadorController;
import java.util.Scanner;


/** Ui para devolver os pontos de um utilizador de as cidade visitadas.
 *
 * @author 
 */
public class DevolverPontosECidadesVisitadasDeUtilizadorUI {
    
        /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    DevolverPontosECidadesVisitadasDeUtilizadorController controller;
    
    /**
     * Construtor
     * @param mundo 
     */
    public DevolverPontosECidadesVisitadasDeUtilizadorUI(Mundo mundo){
        this.mundo = mundo;
        controller = new DevolverPontosECidadesVisitadasDeUtilizadorController(mundo);
    }
    
    /**
     * Modulo principal do UI.
     */
    public void run(){
        
        Scanner scannerLeitura = new Scanner(System.in);
        String nomeUtilizador = "";
        int pontos = 0;
        
        System.out.println("\nPontos e Cidades visitadas por um utilizador");
        System.out.print("Nome de utilizador: ");
        nomeUtilizador = scannerLeitura.nextLine();
        if (nomeUtilizador.equals("") || ( controller.obterPontosDeUtilizador(nomeUtilizador) == null ) ){
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        
        System.out.println("\n O utilizador " + nomeUtilizador + " tem " + controller.obterPontosDeUtilizador(nomeUtilizador) );
        
        String[] arrayDeNomeDeCidadesVisitadas = controller.obterStringArrayComNomeCidadesVisitadasPeloUtilizador(nomeUtilizador);
        if ( arrayDeNomeDeCidadesVisitadas == null ){
            System.out.println("Dado corrumpidos, sem cidades visitadas. Devia existir pelo menos uma");
            return;
        }
        
            System.out.println("Cidades visitadas:");
        for (int i= 0; i< arrayDeNomeDeCidadesVisitadas.length; i++){
            System.out.println(arrayDeNomeDeCidadesVisitadas[i]);
        }
    }
}
