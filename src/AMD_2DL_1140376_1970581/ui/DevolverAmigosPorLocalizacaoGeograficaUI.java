/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.Utilizador;
import AMD_2DL_1140376_1970581.controller.DevolverAmigosPorLocalizacaoController;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author KAMMIKAZI
 */
public class DevolverAmigosPorLocalizacaoGeograficaUI {
    /* Mundo */

    Mundo mundo;
    /* controlador acedido pelo ui */
    DevolverAmigosPorLocalizacaoController controller;

    /**
     * Construtor
     *
     * @param mundo
     */
    public DevolverAmigosPorLocalizacaoGeograficaUI(Mundo mundo) {
        this.mundo = mundo;
        controller = new DevolverAmigosPorLocalizacaoController(mundo);
    }

    /**
     * Modulo principal do UI.
     */
    public void run() {
        Scanner leitura = new Scanner(System.in);
        Scanner leituraNumeros = new Scanner(System.in);
        Scanner opcao = new Scanner(System.in);
        String opcaoLeitura = "";
        String utilizador = "";
        String cidade = "";
        List <String> listaAmigos = new ArrayList<>();
        float latitude = 0;
        float longitude = 0;

        System.out.println("Qual a opção que pretender selecionar para localizar geograficamente os amigos de um utilizador?");
        System.out.print(
                "\n * Localizar geograficamente amigos. * \n"
                + "[1]  Atraves do nome da cidade.\n"
                + "[2]  Atraves de coordenadas geograficas\n"
        );
        opcaoLeitura = opcao.nextLine();

        switch (opcaoLeitura) {
            case "1":
                System.out.println("Qual o nome do utilizador cujos amigos pretende devolver?");
                utilizador = leitura.nextLine();
                if (utilizador.equals("") || (!controller.existeUtilizadorComNome(utilizador))) {
                    System.out.println("Nome invalido ou inexistente.");
                    return;
                }
                System.out.println("Qual o nome da cidade que pretende ?");
                cidade = leitura.nextLine();
                if (cidade.equals("") || (!controller.existeCidadeComNome(cidade))) {
                    System.out.println("Cidade invalida ou inexistente.");
                    return;
                }
                listaAmigos = controller.encontrarAmigosEmCidade(utilizador, cidade);
                if(listaAmigos.isEmpty()) System.out.println("Lista vazia");
                for(String amigo : listaAmigos){
                    System.out.println("\n "+amigo+" \n");
                }
                System.out.println("\nLista de amigos apresentanda com sucesso\n");
                break;
            case "2":
                System.out.println("Qual o nome do utilizador cujos amigos pretende devolver?");
                utilizador = leitura.nextLine();
                if (utilizador.equals("") || (!controller.existeUtilizadorComNome(utilizador))) {
                    System.out.println("Nome invalido ou inexistente.");
                    return;
                }
                System.out.println("Qual a latitaude da cidade que pretende?");
                latitude = leituraNumeros.nextFloat();
                if(latitude < 0 || latitude > 90){ 
                    System.out.println("Valores de latitude invalidos");
                    return;
                }
                System.out.println("Qual a longitude da cidade que pretende?");
                longitude = leituraNumeros.nextFloat();
                if(longitude < 0 || longitude > 180){
                    System.out.println("valores de longitude invalidos");
                    return;
                }
                listaAmigos = controller.encontrarAmigosEmCidadePorCoordenadas(utilizador, latitude, longitude);
                if(listaAmigos.isEmpty()) System.out.println("Lista vazia");
                for(String amigo : listaAmigos){
                    System.out.println("\n "+amigo+" \n");
                }
                System.out.println("\nLista de amigos apresentanda com sucesso\n");
                break;
        }
    }
}
