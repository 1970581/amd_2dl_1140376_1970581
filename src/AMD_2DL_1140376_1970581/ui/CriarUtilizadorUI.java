/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.CriarUtilizadorController;
import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class CriarUtilizadorUI {
    
    /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    CriarUtilizadorController controller;
    
    /**
     * Construtor
     * @param mundo 
     */
    public CriarUtilizadorUI(Mundo mundo){
        this.mundo = mundo;
        controller = new CriarUtilizadorController(mundo);
    }
    
    /**
     * Modulo principal do UI.
     */
    public void run(){
        Scanner scannerLeitura = new Scanner(System.in);
        String nome = "";
        String email = "";
        String cidade = "";
        Cidade objectoCidadeInicial;
        
        System.out.println("\nCRIACAO DE UTILIZADOR");
        System.out.print("Nome: ");
        nome = scannerLeitura.nextLine();
        if (nome.equals("") || controller.existeUtilizadorComNome(nome)){
            System.out.println("Nome invalido ou duplicado.");
            return;
        }
        
        System.out.print("Email: ");
        email = scannerLeitura.nextLine();
        if (email.equals("") || controller.existeUtilizadorComEmail(email)){
            System.out.println("Email invalido ou duplicado.");
            return;
        }
        
        System.out.print("Cidade actual: ");
        cidade = scannerLeitura.nextLine();
        if (cidade.equals("") || !controller.existeCidadeNome(cidade)){
            System.out.println("Cidade com nome invalido ou inexistente.");
            return;
        }
        
        System.out.println("A criar utilizador na cidade.");
        boolean sucesso = controller.criarUtilizadorComCidadeInicial(nome, email, cidade);
        if(sucesso){
            System.out.println("Utilizador nome: " +nome+ " email: "+email+" em "+cidade+" criado com sucesso.");
        }
        else {
            System.out.println("Operacao falhou.");
        }
        
        
        
    }
    
    
}
