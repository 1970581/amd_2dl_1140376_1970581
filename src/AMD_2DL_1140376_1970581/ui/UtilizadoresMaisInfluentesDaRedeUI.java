/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.UtilizadoresMaisInfluentesDaRedeController;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author KAMMIKAZI
 */
public class UtilizadoresMaisInfluentesDaRedeUI {
        Mundo mundo;
    /* controlador acedido pelo ui */
    UtilizadoresMaisInfluentesDaRedeController controller;

    /**
     * Construtor
     *
     * @param mundo
     */
    public UtilizadoresMaisInfluentesDaRedeUI(Mundo mundo) {
        this.mundo = mundo;
        controller = new UtilizadoresMaisInfluentesDaRedeController(mundo);
    }

    /**
     * Modulo principal do UI.
     */
    public void run() {
        List <String> utilizadoresMaisInfulentes = new ArrayList<>();
        utilizadoresMaisInfulentes = controller.obterUtilizadoresMaisInfulentesDaRede();
        if(controller.isEmpty()) System.out.println("Nao existem utilizadores no registo do programa");
        else if (utilizadoresMaisInfulentes.isEmpty()) System.out.println("A lista de utilizadores mais influentes encontrasse vazia");
        else{
        for(String utilizador : utilizadoresMaisInfulentes){
            System.out.println(utilizador);
        }
           
        }
        System.out.println("Estes sao os utilizadores mais influentes da rede");
        
    }
        
        
}
