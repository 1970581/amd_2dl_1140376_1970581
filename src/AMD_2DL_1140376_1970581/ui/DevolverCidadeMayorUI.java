/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.DevolverCidadeMayorController;
import java.util.Iterator;
import java.util.List;

/** UI para devolver as cidades e os mayors.
 *
 * @author 
 */
public class DevolverCidadeMayorUI {
    
    /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    DevolverCidadeMayorController controller;
    
    /**
     * Construtor
     * @param mundo 
     */
    public DevolverCidadeMayorUI(Mundo mundo){
        this.mundo = mundo;
        controller = new DevolverCidadeMayorController(mundo);
    }
    
    /**
     * Modulo principal do UI.
     */
    public void run(){
        System.out.println("Cidade   - > Mayor com valor de pontos.");
        List <String> listaImprimir = controller.criarListagemCidadeMayorPontos();
        Iterator iterador = listaImprimir.iterator();
        while (iterador.hasNext()){
            System.out.println((String) iterador.next());
        }
    }
}
