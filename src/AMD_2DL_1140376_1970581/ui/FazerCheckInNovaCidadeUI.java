/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.FazerCheckInNovaCidadeController;
import java.util.Scanner;

/** UI para fazer check in numa cidade
 *
 * @author 
 */
public class FazerCheckInNovaCidadeUI {
    
     /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    FazerCheckInNovaCidadeController controller;
    
    /**
     * Construtor
     * @param mundo 
     */
    public FazerCheckInNovaCidadeUI(Mundo mundo){
        this.mundo = mundo;
        controller = new FazerCheckInNovaCidadeController(mundo);
    }
    
    /**
     * Modulo principal do UI.
     */
    public void run(){
        
        Scanner scannerLeitura = new Scanner(System.in);
        String nomeCidadeOndeQueroIr = "";
        String nomeUtilizador = "";
        
        System.out.println("\nFazer check in em Nova Cidade");
        System.out.print("Nome de utilizador: ");
        nomeUtilizador = scannerLeitura.nextLine();
        if (nomeUtilizador.equals("") || ( !controller.existeUtilizadorComNome(nomeUtilizador) ) ){
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        
        System.out.println("Bem vindo. Neste momento encontra-se em:" + controller.nomeDaCidadeOndeEstou(nomeUtilizador) +".");
        
        System.out.print("Nome da cidade onde quer fazer novo checkIn: ");
        nomeCidadeOndeQueroIr = scannerLeitura.nextLine();
        if (nomeCidadeOndeQueroIr.equals("") || ( !controller.existeCidadeComNome(nomeCidadeOndeQueroIr) ) ){
            System.out.println("Nome de cidade invalido ou inexistente.");
            return;
        }
        
        if ( controller.nomeDaCidadeOndeEstou(nomeUtilizador).equals(nomeCidadeOndeQueroIr) ){
            System.out.println("Ja esta nesta cidade.");
            return;
        }
        
        
        
        boolean sucesso = controller.moverUtilizadorParaCidade(nomeUtilizador,nomeCidadeOndeQueroIr );
        
        if (sucesso) {
            System.out.println("Utilizador movido com sucesso.");
        }
        else{
            System.out.println("ERRO AO MOVER UTILIZADOR.");
        }
        
        
    }
}
