/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.Parte22Controller;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class Parte22UI {
    /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    Parte22Controller controller;
    
    /**
     * Construtor
     * @param mundo 
     */
    public Parte22UI(Mundo mundo){
        this.mundo = mundo;
        controller = new Parte22Controller(mundo);
    }
    
    /**
     * Modulo principal do UI.
     */
    public void run(){
        
        // Primeiro vamos comfirmar que carregamos os ficheiros de utilizadores.
        
        int numeroUtilizadores = controller.getNumeroUtilizadores();
        System.out.println("O sistema contem "+ numeroUtilizadores + " utilizadores.");
        if (numeroUtilizadores == 0){
            System.out.println("Sem dados Utilizadores suficientes. Carrege os ficheiros de dados correctamente.");
            return;
        }
        // Vamos constuir o grafo.
        int erros = controller.carregarMapa();
        System.out.println("Mapa carregado. " + erros + " erros encontrados.");
        
        
        String opcao = "";
        Scanner buffer = new Scanner(System.in);/* buffer de leitura do Scanner*/
        
        while (!opcao.equals("0")) { //Submenu Opcoes.
            
            System.out.print(
                "\n * Menu 2ª parte.  MAP ADJACENCIA * \n"+
                "[b]  Utilizadores a distancia de relacionamento menor que X de outro utilizador.\n" +
                "[c]  Distancia de relacionamento entre 2 utilizadores.\n" +
                "[f]  Utilizador ou Utilizadores mais influentes da rede.\n" +                
                "[0] Sair \n" +
                "Selecione a sua opção:  "
            );
            opcao = buffer.nextLine(); // Mostra o menu e Le opcao do teclado.

            switch (opcao) {
                
                case "b":
                    utilizadoresAMenorDistanciaDeRelacionamentoQueXDeUtilizador();
                    break;
                
                case "c":
                    distanciaRelacionalDoisUtilizadores();
                    break;    
                    
                case "f":
                    utilizadoresMaisInfluentes();
                    break;

                case "0":
                    System.out.println("A sair.");
                    break;

                default:
                    System.out.println("Não existe esssa opcao.");
                    break;
            }
        }
    }
    
    /**
     * Opcao de Distancia Relacional entre dois utilizadores.
     */
    public void distanciaRelacionalDoisUtilizadores(){
        //Pedimos o nome dos utilizadores.
        Scanner reader = new Scanner(System.in);
        System.out.println("\nDistancia Relacional entre dois utilizadores.");
        System.out.print("Utilizador1: ");
        String u1Nome = reader.nextLine();
        if (u1Nome.equals("") || (!controller.existeUtilizador(u1Nome))) {
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        System.out.print("Utilizador2: ");
        String u2Nome = reader.nextLine();
        if (u2Nome.equals("") || (!controller.existeUtilizador(u2Nome))) {
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        if(u1Nome.equals(u2Nome)) {
            System.out.println("Sao o mesmo utilizador...");
            return;
        }
        int distancia = controller.distanciaRelacional(u1Nome, u2Nome);
        System.out.println( u1Nome + " e " + u2Nome + " encontram-se a uma distancia relacional de " + distancia + ".");
        System.out.println( "Ou seja existem " + (distancia +1 )+ " ligações de amizade de distancia entre os dois.");
    }
    
    /**
     * opcao de Utilizadores a distancia relacional inferior a X.
     */
    public void utilizadoresAMenorDistanciaDeRelacionamentoQueXDeUtilizador(){
        //Pedimos o nome dos utilizadores.
        Scanner reader = new Scanner(System.in);
        System.out.println("\nUtilizadores a distancia relacional inferior a X.");
        System.out.print("Utilizador: ");
        String u1Nome = reader.nextLine();
        if (u1Nome.equals("") || (!controller.existeUtilizador(u1Nome))) {
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        int distancia = 0;
        System.out.print("Distancia relacional: ");
        String distString = reader.nextLine();
        try {distancia = Integer.parseInt(distString);}
        catch (Exception e){
            System.out.println("Numero invalido. A voltar.");
            return;
        }
        LinkedList<String> lista = controller.utilizadoresADistanciaRelacionalX(u1Nome, distancia);
        System.out.println("\nExistem " + lista.size() + " utilizadores a distancia relacional inferior (NÃO IGUAL) a " + distancia + ".");
        for (String s : lista){
            System.out.println(s);
        }
        System.out.println();
    }
    
    /**
     * Mostra a lista dos utilizadores mais influentes da rede social.
     * Mosta os utilizadores com raio (centralidade) igual ao raio do grafo (menor centralidade dos vertices)
     */
    public void utilizadoresMaisInfluentes(){
        System.out.println("\nUtilizadores mais influentes da rede.");
        LinkedList<String> info = new LinkedList<>();
        long start = System.currentTimeMillis();
        boolean conexo = controller.utilizadoresMaisInfluentes(info);
        long end = System.currentTimeMillis();
        System.out.println("Dados processados em: "+ (end-start) +" ms.");
        if(!conexo) System.out.println("O grafo NAO È CONEXO.");
        for (String s : info){
            System.out.println(s);
        }
        
    }
    
    
}
