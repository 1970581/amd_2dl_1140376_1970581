/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.Parte31Controller;
import java.util.ArrayList;

/**
 *
 * @author KAMMIKAZI
 */
public class Parte31UI {
    /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    Parte31Controller controller;
    /**
     * 
     */
    private static String NOME_FICHEIRO_DEFAULT = "data/files300/cityConnections300.txt";
    
    /**
     * Construtor
     * @param mundo 
     */
    public Parte31UI(Mundo mundo){
        this.mundo = mundo;
        controller = new Parte31Controller(mundo);
    }
    
    /**
     * Modulo principal do UI.
     */
    public void run(){
        System.out.println("Lista de mayores ordenada de maneira decrescente");
        ArrayList<String> list = controller.ordenarMayorPontos();
        for(String s : list){
            System.out.println(s);
        }
    }
}
