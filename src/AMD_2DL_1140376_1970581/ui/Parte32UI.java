/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.Parte32Controller;
import java.util.ArrayList;

/**
 *
 * @author KAMMIKAZI
 */
public class Parte32UI {
    /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    Parte32Controller controller;
    
    /**
     * Construtor
     * @param mundo 
     */
    public Parte32UI(Mundo mundo){
        this.mundo = mundo;
        controller = new Parte32Controller(mundo);
    }
    public void run(){
        System.out.println("Lista de cidades ordenada por ordem crescente de utilizadores registados");
        ArrayList<String> list = controller.ordenarCidadeVisitas();
        for(String s : list){
            System.out.println(s);
        }
    }
}
