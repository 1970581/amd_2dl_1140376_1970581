/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.RemoverUtilizadorController;
import java.util.Scanner;

/** UI para remover utilizador
 *
 * @author 
 */
public class RemoverUtilizadorUI {
    
    /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    RemoverUtilizadorController controller;
    
    /**
     * Construtor
     * @param mundo 
     */
    public RemoverUtilizadorUI(Mundo mundo){
        this.mundo = mundo;
        controller = new RemoverUtilizadorController(mundo);
    }
    
    /**
     * Modulo principal do UI.
     */
    public void run(){
        
        Scanner scannerLeitura = new Scanner(System.in);
        String nomeUtilizador = "";
        
        System.out.println("\nRemover Utilizador");
        System.out.print("Nome de utilizador: ");
        nomeUtilizador = scannerLeitura.nextLine();
        if (nomeUtilizador.equals("") || ( !controller.existeUtilizadorComNome(nomeUtilizador) ) ){
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        
        System.out.println("A remover utilizador.");
        long start = System.currentTimeMillis();
        boolean sucess = controller.removerUtilizador(nomeUtilizador);
        long end   = System.currentTimeMillis();
        if(!sucess) {
            System.out.println("ERRO. Ocorreu um erro a remover utilizador.");
            return;
        }
        
        System.out.println("Utilizador removido em: "+ (end-start) +" ms.");
        
    }
}
