/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.LerFicheiroController;
import java.io.File;
import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class LerFicheiroUI {
    
    private Mundo mundo;
    private String ficheiroCidadesDefault = "data/files300/cities300.txt";
    private String ficheiroUtilizadoresDefault = "data/files300/users300.txt";
    
    
    public LerFicheiroUI(Mundo mundo){
        this.mundo = mundo;
    }
    
    
    public void run(){
        
        System.out.println("Opção de carregar dados de ficheiros de texto.");
        
        LerFicheiroController controller = new LerFicheiroController(mundo);
        
        Scanner reader = new Scanner(System.in);
        
        
        //Indicar ficheiro de cidades
        System.out.print("Qual o nome do ficheiro das cidades: ");
        String cidadesFilename;
        cidadesFilename = reader.nextLine();
                
        if (cidadesFilename.equals("")) cidadesFilename = ficheiroCidadesDefault;
        System.out.println(" A verificar: "+ cidadesFilename);
        
        
        if ( !( (new File(cidadesFilename).exists()) && (new File(cidadesFilename).canRead()) ) ){ //Esta verificacao devia ser no controller.
            System.out.println("File not found."); return;
        }
        
        
        
        System.out.print("Qual o nome do ficheiro dos utilizadores ");
        String utilizadoresFilename = reader.nextLine();
        if (utilizadoresFilename.equals("")) utilizadoresFilename = ficheiroUtilizadoresDefault;
        System.out.println(" A verificar: "+ utilizadoresFilename);
        
         if ( !( (new File(utilizadoresFilename).exists()) && (new File(utilizadoresFilename).canRead()) ) ){ //Esta verificacao devia ser no controller.
            System.out.println("File not found."); return;
        }
        
         
        long start = System.currentTimeMillis();
        
        Boolean leuCidadesSemErros = controller.carregarFicheiroCidades(cidadesFilename);
        if(leuCidadesSemErros) {System.out.println("Cidades do ficheiro "+cidadesFilename+ " lidas.");}
        else {System.out.println("Cidades do ficheiro "+cidadesFilename+ " lido COM ERROS.");}
        
        Boolean leuUtilizadoresSemErros = controller.carregarFicheiroUtilizadores(utilizadoresFilename);
        if(leuUtilizadoresSemErros) {System.out.println("Utilizadores do ficheiro "+cidadesFilename+ " lidas.");}
        else {System.out.println("Utilizadores do ficheiro "+cidadesFilename+ " lido COM ERROS.");}
        
        long end = System.currentTimeMillis();
        System.out.println("Dados lidos em: "+ (end-start) +" ms.");
        
    }
    
    
    
    
}
