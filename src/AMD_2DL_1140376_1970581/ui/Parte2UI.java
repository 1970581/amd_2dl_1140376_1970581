/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.ui;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.controller.Parte2Controller;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * UI para a parte 2 do trabalho
 * @author Hugo
 */
public class Parte2UI {
    
    /* Mundo */
    Mundo mundo;
    /* controlador acedido pelo ui */
    Parte2Controller controller;
    /**
     * 
     */
    private static String NOME_FICHEIRO_DEFAULT = "data/files300/cityConnections300.txt";
    
    /**
     * Construtor
     * @param mundo 
     */
    public Parte2UI(Mundo mundo){
        this.mundo = mundo;
        controller = new Parte2Controller(mundo);
    }
    
    /**
     * Modulo principal do UI.
     */
    public void run(){
        
        // Primeiro vamos comfirmar que carregamos os ficheiros de cidades e utilizadores.
        int numeroCidades = controller.getNumeroCidades();
        int numeroUtilizadores = controller.getNumeroUtilizadores();
        System.out.println("O sistema contem "+ numeroUtilizadores + " utilizadores e "+ numeroCidades + " cidades.");
        if (numeroCidades == 0 || numeroUtilizadores == 0){
            System.out.println("Sem dados Cidades/Utilizadores suficientes. Carrege os ficheiros de dados correctamente.");
            return;
        }
        // Vamos constuir o grafo.
        boolean pass = construirGrafo();
        if (!pass) {
            System.out.println("A voltar.");
            return;
        }
        
        String opcao = "";
        Scanner buffer = new Scanner(System.in);/* buffer de leitura do Scanner*/
        
        while (!opcao.equals("0")) { //Submenu Opcoes.
            
            System.out.print(
                "\n * Menu 2ª parte. * \n"+
                "[b]  Apresentar amigos de utilizador a menos de X km.\n" +
                "[c]  Caminho mais curto entre dois utilizadores.\n" +
                "[d]  Caminho mais curto passando por cidades com mais amigos.\n" +                
                "[0] Sair \n" +
                "Selecione a sua opção:  "
            );
            opcao = buffer.nextLine(); // Mostra o menu e Le opcao do teclado.

            switch (opcao) {
                
                case "b":
                    amigosProximidades();
                    break;
                
                case "c":
                    caminhoMaisCurto();
                    break;    
                    
                case "d":
                    caminhoMaisCurtoPassandoPorAmigos();
                    break;

                case "0":
                    System.out.println("A sair.");
                    break;

                default:
                    System.out.println("Não existe esssa opcao.");
                    break;
            }
        }
    }
    
    /**
     * Pergunta o nome do ficheiro e constroi o grafo.
     * @return se consegiu construir um grafo.
     */
    public boolean construirGrafo(){
        //Pedimos o nome do ficheiro e verificamos se existe file.
        System.out.println("\n");
        System.out.println("Default: data/files300/cityConnections300.txt");
        System.out.print("Indique o ficheiro de distancia entre cidades:");
        Scanner reader = new Scanner(System.in);
        String nomeFicheiro;
        nomeFicheiro = reader.nextLine();
        if (nomeFicheiro.equals("")) nomeFicheiro = Parte2UI.NOME_FICHEIRO_DEFAULT;
        System.out.println(" A verificar: "+ nomeFicheiro);
        if ( !( (new File(nomeFicheiro).exists()) && (new File(nomeFicheiro).canRead()) ) ){ //Esta verificacao devia ser no controller.
            System.out.println("File not found."); return false;
        }
        
        long start = System.currentTimeMillis();
        int erros = controller.carregarFicheiroDistancias(nomeFicheiro);
        long end = System.currentTimeMillis();
        System.out.println("Dados lidos em: "+ (end-start) +" ms.");
        if (erros < 0) {
            System.out.println("ERROS GRAVES NA LEITURA DE DADOS!!!");
            return false;
        }
        if (erros > 0) System.out.println("Detectados " + erros + " erros durante a leitura");
        return true;
    }
    
    /**
     * Permite ao utilizador visualizar o caminho mais curto entre dois utilizadores.
     */
    public void caminhoMaisCurto(){
        //Pedimos o nome dos utilizadores.
        Scanner reader = new Scanner(System.in);
        System.out.println("\nCaminho mais curto entre dois utilizadores.");
        System.out.print("Utilizador1: ");
        String u1Nome = reader.nextLine();
        if (u1Nome.equals("") || (!controller.existeUtilizador(u1Nome))) {
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        System.out.print("Utilizador2: ");
        String u2Nome = reader.nextLine();
        if (u2Nome.equals("") || (!controller.existeUtilizador(u2Nome))) {
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        
        ArrayList<String> caminho = new ArrayList<>();
        Double distancia = controller.caminhoMaisCurto(u1Nome, u2Nome, caminho );
        
        if(distancia == -1){
            System.out.println("Não existe caminho entre as cidades dos dois utilizadores.");
            return;
        }
        
        System.out.println("O utilizadores estão a " + distancia.doubleValue() + " kms de distancia.");
        System.out.println("Caminho:");
        for (String s : caminho){
            System.out.println(s);
        }
    }
    
    /**
     * Permite ver quais os amigos na proximidade de X km de um utilizador
     */
    public void amigosProximidades(){
        //Pedimos o nome do utilizador.
        Scanner reader = new Scanner(System.in);
        System.out.println("\nApresentar amigos de utilizador a menos de X km.");
        System.out.print("Utilizador: ");
        String u1Nome = reader.nextLine();
        if (u1Nome.equals("") || (!controller.existeUtilizador(u1Nome))) {
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        //Depois a distancia.
        System.out.print("Distancia: ");
        String distancia = reader.nextLine();
        double kms = 0;
        try{
            kms = Double.parseDouble(distancia);
        }
        catch(Exception e){
            System.out.println("Distancia invalida.");
            return;
        }
        if (kms <=0){
            System.out.println("Distancia invalida.");
            return;
        }
        
        ArrayList<String> resposta = new ArrayList<>();
        //Pedimos para calcular, e recebemos na resposta.
        controller.amigosPerto(u1Nome, kms, resposta);
        
        System.out.println("Amigos que se encontram a menos de " + distancia + " kms.");
        for (String s : resposta){
            System.out.println(s);
        }
    }
    
    /**
     * Caminho mais curto entre dois utilizadores que passa pelas cidades onde tem mais amigos.
     */
    public void caminhoMaisCurtoPassandoPorAmigos(){
        //Pedimos o nome dos utilizadores.
        Scanner reader = new Scanner(System.in);
        System.out.println("\nCaminho mais curto entre dois utilizadores passando pelas cidades onde tem mais amigos.");
        System.out.print("Utilizador1: ");
        String u1Nome = reader.nextLine();
        if (u1Nome.equals("") || (!controller.existeUtilizador(u1Nome))) {
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        System.out.print("Utilizador2: ");
        String u2Nome = reader.nextLine();
        if (u2Nome.equals("") || (!controller.existeUtilizador(u2Nome))) {
            System.out.println("Nome invalido ou inexistente.");
            return;
        }
        
        ArrayList<String> caminho = new ArrayList<>();
        Double distancia = controller.caminhoCurtoPassandoPorAmigos(u1Nome, u2Nome, caminho );
        
        if(distancia == -1){
            System.out.println("Não existe caminho possivel para este caso.");
            return;
        }
        
        System.out.println("O caminho tem " + distancia.doubleValue() + " kms de distancia.");
        System.out.println("Caminho:");
        for (String s : caminho){
            System.out.println(s);
        }
    }
    
    
}
