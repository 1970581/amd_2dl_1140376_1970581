/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import AMD_2DL_1140376_1970581.controller.LerFicheiroController;
import AMD_2DL_1140376_1970581.ui.AdicionarCidadeUI;
import AMD_2DL_1140376_1970581.ui.AdicionarLigacaoAmizadeUI;
import AMD_2DL_1140376_1970581.ui.CriarUtilizadorUI;
import AMD_2DL_1140376_1970581.ui.DevolverAmigosPorLocalizacaoGeograficaUI;
import AMD_2DL_1140376_1970581.ui.DevolverCidadeMayorUI;
import AMD_2DL_1140376_1970581.ui.DevolverPontosECidadesVisitadasDeUtilizadorUI;
import AMD_2DL_1140376_1970581.ui.FazerCheckInNovaCidadeUI;
import AMD_2DL_1140376_1970581.ui.LerFicheiroUI;
import AMD_2DL_1140376_1970581.ui.Parte22UI;
import AMD_2DL_1140376_1970581.ui.Parte2UI;
import AMD_2DL_1140376_1970581.ui.Parte31UI;
import AMD_2DL_1140376_1970581.ui.Parte32UI;
import AMD_2DL_1140376_1970581.ui.RemoverLigacaoAmizadeUI;
import AMD_2DL_1140376_1970581.ui.RemoverUtilizadorUI;
import AMD_2DL_1140376_1970581.ui.UtilizadoresMaisInfluentesDaRedeUI;

import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Objecto mundo */
        Mundo mundo = new Mundo();
        /* Opcao do menu */
        String opcao = "";
        /* buffer de leitura do Scanner*/
        Scanner buffer = new Scanner(System.in);
        
        
        
        //Menu Principal
        while (!opcao.equals("0")){
            
            opcao = mostrarMenuPrincipal(buffer); // Mostra o menu e Le opcao do teclado.
            
            switch(opcao){
                
                case "1" : 
                    LerFicheiroUI ui1 = new LerFicheiroUI(mundo);
                    ui1.run();
                    break;
                case "2" :
                    CriarUtilizadorUI ui2 = new CriarUtilizadorUI(mundo);
                    ui2.run();
                    break;
                case "3" :
                    AdicionarLigacaoAmizadeUI ui3 = new AdicionarLigacaoAmizadeUI(mundo);
                    ui3.run();
                    break;
                case "4" :
                    RemoverLigacaoAmizadeUI ui4 = new RemoverLigacaoAmizadeUI(mundo);
                    ui4.run();
                    break;
                case "5" :
                    RemoverUtilizadorUI ui5 = new RemoverUtilizadorUI(mundo);
                    ui5.run();
                    break;
                case "6" :
                    AdicionarCidadeUI ui6 = new AdicionarCidadeUI(mundo);
                    ui6.run();
                    break;
                case "7" :
                    DevolverCidadeMayorUI ui7 = new DevolverCidadeMayorUI(mundo);
                    ui7.run();
                    break;
                case "8" :
                    FazerCheckInNovaCidadeUI ui8 = new FazerCheckInNovaCidadeUI(mundo);
                    ui8.run();
                    break;
                case "9" :
                    DevolverPontosECidadesVisitadasDeUtilizadorUI ui9 = new DevolverPontosECidadesVisitadasDeUtilizadorUI(mundo);
                    ui9.run();
                    break;
                case "10" :
                    DevolverAmigosPorLocalizacaoGeograficaUI ui10 = new DevolverAmigosPorLocalizacaoGeograficaUI(mundo);
                    ui10.run();
                    break;
                case "11" :
                    UtilizadoresMaisInfluentesDaRedeUI ui11 = new UtilizadoresMaisInfluentesDaRedeUI(mundo);
                    ui11.run();
                    break;
                case "a" :
                    Parte2UI uiP2 = new Parte2UI(mundo);
                    uiP2.run();
                    break;
                case "b" :
                    Parte22UI uiP22 = new Parte22UI(mundo);
                    uiP22.run();
                    break;
                case "12":
                    Parte31UI uiP31 = new Parte31UI(mundo);
                    uiP31.run();
                    break;
                case "13":
                    Parte32UI uiP32 = new Parte32UI(mundo);
                    uiP32.run();
                    break;
                case "0" :
                    System.out.println("A sair.");
                    break;
                    
                    
                default:
                    System.out.println("Não existe esssa opcao.");
                    break;
                    
            }
            
            
        }
        
        
        
        
        
        
        
        
        
        //LerFicheiroController lerFicheiroController = new LerFicheiroController(mundo);
        
        //lerFicheiroController.carregarFicheiroCidades("data/files10/cities10.txt");
        //lerFicheiroController.carregarFicheiroCidades("data/files300/cities300.txt");
        //lerFicheiroController.carregarFicheiroUtilizadores("data/files10/users10.txt");
        //lerFicheiroController.carregarFicheiroUtilizadores("data/files300/users300.txt");
        
        
        
        
    }
    
    /**
     * Mostra o menu e le a opcao do utilizador.
     * @param buffer
     * @return 
     */
    private static String mostrarMenuPrincipal(Scanner buffer){
        System.out.print(
                "\n * Menu Principal. * \n"+
                "[1]  Carregar dados de ficheiro.\n" +
                "[2]  Inserir novo utilizador\n" +
                "[3]  Adicionar ligacao de amizade\n" +
                "[4]  Remover ligacao de amizade\n" +
                "[5]  Remover utilizador\n" +
                "[6]  Adicionar cidade\n" +
                "[7]  Devolver cidades e mayors\n" +
                "[8]  Fazer checkin em nova cidade\n" +
                "[9]  Devolver numero de pontos de utilizador, e cidades visitadas\n" +
                "[10] Devolver amigos de utilizador por localizacao geografica.\n" +
                "[11] Devolver utilizadores com maior numero de amigos.\n" +
                "\n" +
                "[a]  Aceder a PARTE 2 (2-Matriz)\n"+        
                "[b]  Aceder a PARTE 2 (3-Map)\n"+
                 "\n"+
                "[12] Apresentar mayores pelo numero decrescente dos seus pontos \n"+
                "[13] Apresentar cidades por ordem crescente de utilizadores registados\n"+
                "[0] Sair \n" +
                "Selecione a sua opção:  "
        );
        return buffer.nextLine();
    }
    
    
    
    
}
