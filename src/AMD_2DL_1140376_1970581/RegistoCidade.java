/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import java.util.LinkedHashMap;
import java.util.Map;

/** Registo onde se guardam as ciades sob a forma de um map.
 *
 * @author 
 */
public class RegistoCidade {
    
    private Map <String, Cidade> cidadesKeyNome;
    
    public RegistoCidade(){
        this.cidadesKeyNome = new LinkedHashMap<>();
    }
    
    /**
     * Cria e adiciona uma nova cidade ao registo
     * @param nome nome da Cidade, Unico
     * @param pontos pontos da Cidade
     * @param Latitude latitude da cidade
     * @param Longitude longitude da cidade
     * @return sucesso da operaçao. True adicionou, false ja existe cidade com essa key e nao cria.
     */
    public boolean adicionarCidade(String nome, int pontos, float Latitude, float Longitude){
        
        if (this.cidadesKeyNome.containsKey(nome)) return false;
        Cidade novaCidade = new Cidade(nome, pontos, Latitude, Longitude);
        Cidade lixo = this.cidadesKeyNome.put(nome, novaCidade);  //Possivel condensar para tornar mais rapido.
        if (lixo != null )return false;  //Desnecerasio, apenas como seguranca     
        
        return true;
    }
    
    /**
     * Procura no map uma cidade com o nome nomeCidade e devolvea.
     * Se nao encontrar devolve null.
     * @param nomeCidade
     * @return 
     */
    public Cidade getCidade(String nomeCidade){
        return this.cidadesKeyNome.get(nomeCidade);            
    }
    
    /**
     * Retorna o mapa das cidades guardado neste registo,.
     * @return um map Map String, Cidade .
     */
    public Map <String, Cidade> getMapRegistoCidade(){
        return this.cidadesKeyNome;
    }
   
}

/* COMPARACAO DOS TRES TIPOS DE MAPS em java.
╔══════════════╦═════════════════════╦═══════════════════╦══════════════════════╗
║   Property   ║       HashMap       ║      TreeMap      ║     LinkedHashMap    ║
╠══════════════╬═════════════════════╬═══════════════════╬══════════════════════╣
║              ║  no guarantee order ║ sorted according  ║                      ║
║   Order      ║ will remain constant║ to the natural    ║    insertion-order   ║
║              ║      over time      ║    ordering       ║                      ║
╠══════════════╬═════════════════════╬═══════════════════╬══════════════════════╣
║  Get/put     ║                     ║                   ║                      ║
║   remove     ║         O(1)        ║      O(log(n))    ║         O(1)         ║
║ containsKey  ║                     ║                   ║                      ║
╠══════════════╬═════════════════════╬═══════════════════╬══════════════════════╣
║              ║                     ║   NavigableMap    ║                      ║
║  Interfaces  ║         Map         ║       Map         ║         Map          ║
║              ║                     ║    SortedMap      ║                      ║
╠══════════════╬═════════════════════╬═══════════════════╬══════════════════════╣
║              ║                     ║                   ║                      ║
║     Null     ║       allowed       ║    only values    ║       allowed        ║
║ values/keys  ║                     ║                   ║                      ║
╠══════════════╬═════════════════════╩═══════════════════╩══════════════════════╣
║              ║   Fail-fast behavior of an iterator cannot be guaranteed       ║
║   Fail-fast  ║ impossible to make any hard guarantees in the presence of      ║
║   behavior   ║           unsynchronized concurrent modification               ║
╠══════════════╬═════════════════════╦═══════════════════╦══════════════════════╣
║              ║                     ║                   ║                      ║
║Implementation║      buckets        ║   Red-Black Tree  ║    double-linked     ║
║              ║                     ║                   ║       buckets        ║
╠══════════════╬═════════════════════╩═══════════════════╩══════════════════════╣
║      Is      ║                                                                ║
║ synchronized ║              implementation is not synchronized                ║
╚══════════════╩════════════════════════════════════════════════════════════════╝
FROM: http://stackoverflow.com/questions/2889777/difference-between-hashmap-linkedhashmap-and-treemap

	
Relativamente a velocidade:
FROM: http://stackoverflow.com/questions/12998568/hashmap-vs-linkedhashmap-performance-in-iteration-over-values
I wrote a little profiling program creating 1 million keys (Integer) vs Boolean.TRUE, repeating 100 times. Found the following:

HashMap:-
Create:  3.7sec
Iterate: 1.1sec
Access:  1.5sec
Total:   6.2sec

LinkedHashMap:-
Create:  4.7sec   (30% slower)
Iterate: 0.5sec   (50% faster)
Access:  0.8sec   (50% faster)
Total :  6.0sec

Garbage collection NOT done so pollutes the numbers somewhat, however I think LinkedHashMap has the edge over HashMap and I will be using that in future code.

OPCAO: LinkedHashmap

*/