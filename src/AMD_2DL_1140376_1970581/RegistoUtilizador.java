/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author Hugo
 */
public class RegistoUtilizador {

    private Map<String, Utilizador> mapRegistoUtilizador;

    public RegistoUtilizador() {
        this.mapRegistoUtilizador = new LinkedHashMap<>();
    }

    /**
     * Devolve o mapa com os registos de utilizadores.
     *
     * @return Map string, Utilizador
     */
    public Map<String, Utilizador> getMapRegistoUtilizador() {
        return this.mapRegistoUtilizador;
    }

    /**
     * Devolve o utilizador com o nickname nomeUtilizador. Se nao existir
     * devolve null.
     *
     * @param nomeUtilizador
     * @return
     */
    public Utilizador getUtilizador(String nomeUtilizador) {
        return this.mapRegistoUtilizador.get(nomeUtilizador);
    }

    /**
     * Devolve o utilizador existente com o email indicado. MAIS LENTO, usar o
     * get por nickname Devolve null se nao existir.
     *
     * @param email
     * @return
     */
    public Utilizador getUtilizadorByEmail(String email) {
        for (Utilizador utilizador : this.mapRegistoUtilizador.values()) {
            if (utilizador.getEmail().equals(email)) return utilizador;
        }
        return null;
    }

    /**
     * Criar um novo utilizador e se este nao for repetido adiciona ao mapa de
     * utilizadores
     *
     * @param nickName
     * @param email
     * @return o novo utilizador
     */
    public Utilizador criarUtilizador(String nickName, String email) {

        Utilizador novoUtilizador = new Utilizador(nickName, email);
        if (mapRegistoUtilizador.containsKey("nickName")) {
            return null;
        }
        if (this.getUtilizadorByEmail(email) != null) {
            return null;
        }

        this.mapRegistoUtilizador.put(nickName, novoUtilizador);
        return novoUtilizador;

    }



    
    
    /**
     * Metedo para listar utilizadores com mais amigos
     * @return lista de utilizadores com mais amigos.
     */
    public List <Utilizador>  listarUtilizadoresComMaisAmigos(){
        List <Utilizador> listUtilizadoresComMaisAmigos = new ArrayList<>();
        int nMaxAmigos = -1;
        int nAmigos;
        
        for (Utilizador utilizador : this.mapRegistoUtilizador.values()){
            nAmigos = utilizador.getNumeroDeAmigos();
            if (nAmigos == nMaxAmigos){
                listUtilizadoresComMaisAmigos.add(utilizador);
            }
            if (nAmigos > nMaxAmigos) {
                listUtilizadoresComMaisAmigos.clear();
                listUtilizadoresComMaisAmigos.add(utilizador);
                nMaxAmigos = nAmigos;
            }
            //Se for menor, nao fazemos nada.
        }
        
        return listUtilizadoresComMaisAmigos;
    }
    
    

}
