/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import java.util.Objects;

/** Classe para construir objecto que relacione uma Cidade Com um Mayor(utilizador)
 *  Usada para ordenar a relacao cidade - mayor por pontos.
 *  Nao usamos mayor = cidade.getMayor() para diminuir dependencias. Construimos com base no utilizador passado.
 *  Assim permitimos futuras altercaoes a obtencao do mayor sem invalidar esta classe.
 * DEPENDE DE Utilizador.getPontos();
 * @author 
 */
public class CidadeMayor implements Comparable{
    private Cidade cidade;
    private Utilizador mayor;
    
    /**Construtor por parametro.
     * Nao usamos mayor = cidade.getMayor() no construtor para diminuir dependencias.
     * Construimos com base no utilizador passado por parametro, que deve ser o mayor.
     * Assim permitimos futuras altercaoes a obtencao do mayor sem invalidar esta classe.
     * @param cidade cidade
     * @param mayor utilizador Mayor da cidade
     */
    public CidadeMayor(Cidade cidade, Utilizador mayor){
        this.cidade = cidade;
        this.mayor = mayor;
    }

    /**
     * @return the cidade
     */
    public Cidade getCidade() {
        return cidade;
    }
    /**
     * @return the pontos do mayor
     */
    public int getPontos() {
        return mayor.getPontos();
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the mayor
     */
    public Utilizador getMayor() {
        return mayor;
    }

    /**
     * @param mayor the mayor to set
     */
    public void setMayor(Utilizador mayor) {
        this.mayor = mayor;
    }
    
    /**
     * Compara dois objectos CidadeMayor
     * @param obj outra CidadeMayor
     * @return Se Cidade e Mayor forem iguais, true, senao falso.
     */
    @Override
    public boolean equals(Object obj){
        if (obj == null) return false;
        if (this == obj) return true;
        if ( !(obj instanceof CidadeMayor) ) return false;
        CidadeMayor that = (CidadeMayor) obj;
        return this.getCidade().equals(that.getCidade()) && this.getMayor().equals(that.getMayor());
        // Se Cidade e Mayor forem iguais, true, senao falso.
    }

    /**
     * HashCode baseado em Cidade e Utilizador
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.cidade);
        hash = 37 * hash + Objects.hashCode(this.mayor);
        return hash;
    }

    /** Compara dois objectos CidadeMayor
     *  
     *  1º Compara os pontos. 
     *  2ª Se os pontos iguais, compara os utilizadores, (que deve comparar o nome do utilizador).
     *  3ª Se os utilizadores forem iguais, compara as cidades (nome).
     * @param o
     * @return Se this maior que that devolve negativo, se this menor que that devolve positivo , se iguais zero1
     */
    @Override
    public int compareTo(Object o) {
        CidadeMayor that = (CidadeMayor) o;
        
        //if (this.getCidade().equals(that.getCidade())) return 0;  //REmovido devido ao hashcode contract.
        
        int result = that.getMayor().getPontos() - this.getMayor().getPontos();
        if (result == 0){                                           // PONTOS IGUAIS
            result = this.getMayor().compareTo(that.getMayor());
            if (result == 0){                                     // NOMES IGUAIS
                result = this.getCidade().compareTo(that.getCidade()); //Compara as cidades
            }   
        }
        
        return result;
    }
    
    
    
    
}
