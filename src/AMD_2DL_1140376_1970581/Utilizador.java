/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author
 */
public class Utilizador implements Comparable {

    private String nickname;
    private String email;
    private List<Cidade> cidadesVisitadas;
    private Cidade ultimaCidade;
    private int pontos;
    private RegistoAmizade registoAmigos;
    public Utilizador(String nome, String email) {
        this.nickname = nome;
        this.email = email;
        cidadesVisitadas = new LinkedList<>();
        ultimaCidade = null;
        this.pontos = 0;
        registoAmigos = new RegistoAmizade();
    }

    public String getNome() {
        return this.nickname;
    }

    public String getEmail() {
        return this.email;
    }

    public int getPontos() {
        return this.pontos;
    }

    public Cidade getUltimaCidade() {
        return this.ultimaCidade;
    }
    
    /**
     * Retorna uma linked list de cidades visitadas
     * @return List LinkedList de Cidade cidadesVisitadas;
     */
    public List<Cidade> getListaCidadesVisitadas() {
        return this.cidadesVisitadas;
    }

    /**
     * Mover utilizador para uma cidade. Vai a cidade e actualiza o mayor.
     * Devolve false se nao consegue mover para a cidade.
     *
     * @param cidade
     * @return
     */
    public boolean moverParaCidade(Cidade cidade) {
        //System.out.print(this.getNome() + ":" + cidade.getNome() + " - ");//Debug
        if (cidade == null) {
            return false;
        }
        if (cidade.equals(ultimaCidade)) {
            return false;
        }
        this.cidadesVisitadas.add(cidade);
        this.ultimaCidade = cidade;
        this.pontos += cidade.getPontos();
        cidade.utilizadorMoveuParaCidadeActualizarMayor(this);
        //System.out.println(this.getNome() + ":" + cidade.getNome());//DEbug
        return true;
    }

    /**
     * Ira adicionar a amizade bidirecional entre o utilizador em questao e o
     * utilizador passado por parametro
     *
     * @param amigo - Utilizador passado por parametro
     * @return true caso a adicao de amizades seja bem sucedida
     */
    public boolean adicionarAmizade(Utilizador amigo) {

        if (registoAmigos.isAmigo(amigo)) {
            return false;
        }
        this.registoAmigos.adicionarAmizade(amigo); // adiciona amigo ao registo deste utilizador
        amigo.getRegistoAmizade().adicionarAmizade(this); // adicioan este utilizador como amigo ao registo do amigo
        return true;
    }
    
    /**
     * Remove um amigo deste utilizador
     * @param amigo utilizador a remover
     * @return sucesso da operacao
     */
    public boolean removerAmizade(Utilizador amigo) {

        if (!registoAmigos.isAmigo(amigo)) {
            return false;
        }
        this.registoAmigos.removerAmizade(amigo);
        amigo.getRegistoAmizade().removerAmizade(this);
        return true;
    }

    /**
     * Ira retorna o registo de amizade deste utilizador
     *
     * @return o registo de amizade deste utilizador
     */
    public RegistoAmizade getRegistoAmizade() {
        return this.registoAmigos;
    }

    public List<Utilizador> encontrarAmigosPorCoordenadas(float latitude, float longitude) {
        List<Utilizador> listaAmigos = new ArrayList<>();
        float raio = Cidade.RAIO_CIDADE;
        for (Utilizador amigo : this.registoAmigos.getRegistoAmizade().values()) {
            if (amigo.getUltimaCidade().verificaSeCidadeEncontraNoRaio(latitude, longitude, raio)) {
                listaAmigos.add(amigo);
            }
        }
        return listaAmigos;
    }
    
    /**
     *  Devolve uma lista de amigos existente na cidade
     * @param cidade cidade a verificar
     * @return List de Utilizador amigos que estao na cidade
     */
    public List<Utilizador> econtrarAmigosPorNomeCidade(Cidade cidade) {
        List<Utilizador> listaAmigos = new ArrayList<>();
        for (Utilizador amigo : this.registoAmigos.getRegistoAmizade().values()) {
            if (amigo.getUltimaCidade().equals(cidade)) {
                listaAmigos.add(amigo);
            }
        }
        return listaAmigos;
    }

    public int getNumeroDeAmigos() {
        return registoAmigos.getNumeroDeAmigos();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Utilizador)) {
            return false;
        }
        Utilizador that = (Utilizador) obj;
        if (this.getNome().equals(that.getNome()) || this.getEmail().equals(that.getEmail())) {
            return true;
        } else {
            return false;
        }

    }
    
    /**
     * Hashcode baseado apenas no nome, nao permitimos nomes repetidos.
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.nickname);
        return hash;
    }
    
    /**
     * Devolver informacao sobre a cidade sob a forma de string.
     * @return this.getNome() + " " + this.getEmail() + " " + this.pontos + " em " + (this.ultimaCidade == null ? "null" : this.ultimaCidade.getNome())
     */
    @Override
    public String toString() {
        return this.getNome() + " " + this.getEmail() + " " + this.pontos + " em " + (this.ultimaCidade == null ? "null" : this.ultimaCidade.getNome());
    }

    /**
     * Compara os utilizadores recorrendo ao nome. Compares os nomes lexicographically.
     * Compara char a char. Se um char for maior devolve maior. Se um nao tiver index seguinte retorna esse como menor.
     * @param utilizador outro utilizador a comparar
     * @return +1 se this for lexicographically maior, 0 se nomes iguais, -1 se menor
     */
    @Override
    public int compareTo(Object utilizador) {
        Utilizador that = (Utilizador) utilizador;
        return this.getNome().compareTo(that.getNome());
    }

    /**
     * Devolve o numero de vezes que o utilizador visitou a cidade
     *
     * @param cidade
     * @return o numero de visitas
     */
    public int calcularNumeroDeVisitasACidade(Cidade cidade) {
        Iterator iterador = this.cidadesVisitadas.iterator();
        int numeroDeVisitas = 0;
        Cidade cidadeIterada = null;
        while (iterador.hasNext()) {
            cidadeIterada = (Cidade) iterador.next();
            if (cidadeIterada.equals(cidade)) {
                numeroDeVisitas++;
            }
        }
        return numeroDeVisitas;
    }
    
}
