/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 
 */
public class ComparatorUtilizadorPontosTest {
    static Utilizador u1;
    static Utilizador u2;
    static Cidade c1;
    
    public ComparatorUtilizadorPontosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("u1", "u1@email.com");
        u2 = new Utilizador("u2", "u2@email.com");
        c1 = new Cidade("c1", 5, 1f, 1f);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class ComparatorUtilizadorPontos.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        Object o1 = u1;
        Object o2 = u2;
        u1.moverParaCidade(c1);
        ComparatorUtilizadorPontos instance = new ComparatorUtilizadorPontos();
        int expResult = 5;
        int result = instance.compare(o1, o2);
        assertEquals(expResult, result);
        
    }
    
}
