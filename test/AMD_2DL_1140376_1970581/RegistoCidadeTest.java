/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class RegistoCidadeTest {
    
    static RegistoCidade registoCidade;
    static Cidade cidade1;
    
    
    public RegistoCidadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        registoCidade = new RegistoCidade();
        registoCidade.adicionarCidade("cidade1", 1, 2f, 3f);
        cidade1 = registoCidade.getCidade("cidade1");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of adicionarCidade method, of class RegistoCidade.
     */
    @Test
    public void testAdicionarCidade() {
        System.out.println("adicionarCidade");
        String nome = cidade1.getNome();
        int pontos = cidade1.getPontos();
        float Latitude = cidade1.getLatitude();
        float Longitude = cidade1.getLongitude();
        RegistoCidade instance = new RegistoCidade();
        boolean add = instance.adicionarCidade(nome, pontos, Latitude, Longitude);
        boolean add2 = instance.adicionarCidade(nome, pontos, Latitude, Longitude);
        
        boolean expResult = add && !add2;
        boolean result = true;
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getCidade method, of class RegistoCidade.
     */
    @Test
    public void testGetCidade() {
        System.out.println("getCidade");
        String nomeCidade = cidade1.getNome();
        RegistoCidade instance = registoCidade;
        Cidade expResult = cidade1;
        Cidade result = instance.getCidade(nomeCidade);
        assertEquals(expResult, result);        
    }
    
}
