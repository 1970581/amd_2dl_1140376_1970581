/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author i140376
 */
public class UtilizadorTest {
    
    static Utilizador u1;
    static Utilizador u2;
    static Utilizador u3;
    static Cidade c1;
    static Cidade c2;
    static Cidade c3;
    
    public UtilizadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("u1", "u11965@hotmail.com");
        u2 = new Utilizador("u2", "u21985@hotmail.com");
        u3 = new Utilizador("u3", "u31925@hotmail.com");
        c1 = new Cidade("c1", 1, 1f, 1f);
        c2 = new Cidade("c2", 2, 2f, 2f);
        c3 = new Cidade("c3", 3, 3f, 3f);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNome method, of class Utilizador.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Utilizador instance = u1;
        String expResult = "u1";
        String result = instance.getNome();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getUltimaCidade method, of class Utilizador.
     */
    @Test
    public void testGetUltimaCidade() {
        System.out.println("getUltimaCidade");
        Utilizador instance = u1;
        Cidade expResult = c1;
        u1.moverParaCidade(c1);
        u1.moverParaCidade(c2);
        u1.moverParaCidade(c1);
        
        Cidade result = instance.getUltimaCidade();
        assertEquals(expResult, result);
        
    }

   

    /**
     * Test of moverParaCidade method, of class Utilizador.
     */
    @Test
    public void testMoverParaCidade() {
        System.out.println("moverParaCidade");
        Cidade cidade = c2;
        Utilizador instance = u1;
        boolean expResult = true;
        boolean result = instance.moverParaCidade(c1);
        assertEquals(expResult, result);
        assertEquals(c1,u1.getUltimaCidade());
   
    }

    /**
     * Test of adicionarAmizade method, of class Utilizador.
     */
    @Test
    public void testAdicionarAmizade() {
        System.out.println("adicionarAmizade");
        Utilizador amigo = u2;
        Utilizador instance = u1;
        boolean expResult = true;
        boolean result = instance.adicionarAmizade(amigo);
        assertEquals(expResult, result);
        assertEquals(true, u1.getRegistoAmizade().isAmigo(amigo));
        
        assertEquals(false, u1.getRegistoAmizade().isAmigo(u3));//FALSE
        
        
    }

    /**
     * Test of removerAmizade method, of class Utilizador.
     */
    @Test
    public void testRemoverAmizade() {
        System.out.println("removerAmizade");
        Utilizador amigo = u3;
        Utilizador instance = u1;
        boolean expResult = true;
        u1.adicionarAmizade(u2);
        u1.adicionarAmizade(u3);
        assertEquals(true, u1.getRegistoAmizade().isAmigo(u3));
        
        boolean result = instance.removerAmizade(amigo);
        assertEquals(expResult, result);
        assertEquals(false, u1.getRegistoAmizade().isAmigo(u3));//FALSE
        assertEquals(false, u1.removerAmizade(amigo));//FALSE
    }



    /**
     * Test of getNumeroDeAmigos method, of class Utilizador.
     */
    @Test
    public void testGetNumeroDeAmigos() {
        System.out.println("getNumeroDeAmigos");
        Utilizador instance = u1;
        int expResult = 2;
        u1.adicionarAmizade(u2);
        u1.adicionarAmizade(u3);
        
        int result = instance.getNumeroDeAmigos();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = u1;
        Utilizador instance = u2;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);   // testamos false
        
        assertEquals(true, u1.equals(new Utilizador(u1.getNome(), u1.getEmail())));   // testar o true
        
    }

    /**
     * Test of hashCode method, of class Utilizador.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Utilizador instance = u1;
        
        assertEquals(u1.hashCode(), (new Utilizador(u1.getNome(),u1.getEmail())).hashCode());
        assertEquals(false, u1.hashCode() == (new Utilizador(u2.getNome(),u1.getEmail())).hashCode());//FALSE
        
        
        
    }

    /**
     * Test of toString method, of class Utilizador.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador instance = u1;
        String expResult = u1.getNome() + " " + u1.getEmail() + " " + u1.getPontos() + " em " + (u1.getUltimaCidade() == null ? "null" : u1.getUltimaCidade().getNome());
        String result = instance.toString();
        assertEquals(expResult, result);
        u1.moverParaCidade(c1);
        assertEquals("u1 u11965@hotmail.com 1 em c1", u1.toString());
        
    }

    /**
     * Test of compareTo method, of class Utilizador.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Object utilizador = u1;
        Utilizador instance = u2;
        int expResult = u2.getNome().compareTo(u1.getNome());
        int result = instance.compareTo(utilizador);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of calcularNumeroDeVisitasACidade method, of class Utilizador.
     */
    @Test
    public void testCalcularNumeroDeVisitasACidade() {
        System.out.println("calcularNumeroDeVisitasACidade");
        Cidade cidade = c1;
        Utilizador instance = u1;
        int expResult = 3;
        
        List lista = u1.getListaCidadesVisitadas();
        lista.add(c1);
        lista.add(c3);
        lista.add(c3);
        lista.add(c1);
        lista.add(c2);
        lista.add(c1);
        lista.add(c3);
           
                
        int result = instance.calcularNumeroDeVisitasACidade(cidade);
        assertEquals(expResult, result);
        
    }

    
    /**
     * Test of encontrarAmigosPorCoordenadas method, of class Utilizador.
     */
    @Test
    public void testEncontrarAmigosPorCoordenadas() {
        System.out.println("encontrarAmigosPorCoordenadas");
        
        float latitudeA = 41.173486F;
        float longitudeA = -8.621292F;  //Porto
        float latitudeB = 38.718733F;   //Lisboa
        float longitudeB = -9.141540F;
        
        //41.155606, -8.636196
        //41.158671, -8.673204   Distam menos de 10km e mais de 1km
        
        Mundo mundo = new Mundo();
        mundo.getRegistoCidade().adicionarCidade("cc1", 1, 41.155606f, -8.636196f);
        mundo.getRegistoCidade().adicionarCidade("cc2", 1, 1000f, 1000f);
        mundo.getRegistoUtilizador().criarUtilizador("uu1", "uu1@email.com");
        mundo.getRegistoUtilizador().criarUtilizador("uu2", "uu2@email.com");
        mundo.getRegistoUtilizador().criarUtilizador("uu3", "uu3@email.com");
        
        Utilizador uu1 = mundo.getRegistoUtilizador().getUtilizador("uu1");
        Utilizador uu2 = mundo.getRegistoUtilizador().getUtilizador("uu2");
        Utilizador uu3 = mundo.getRegistoUtilizador().getUtilizador("uu3");
        
        Cidade cc1 = mundo.getRegistoCidade().getCidade("cc1");
        Cidade cc2 = mundo.getRegistoCidade().getCidade("cc2");
        
        
        uu1.adicionarAmizade(uu2);
        uu1.adicionarAmizade(uu3);
        
        uu1.moverParaCidade(cc1);
        uu2.moverParaCidade(cc1);
        uu3.moverParaCidade(cc2);
        
        
        float latitude = 41.158671f;
        float longitude = -8.673204f;
        Utilizador instance = uu1;
        List<Utilizador> expResult = new ArrayList();
        expResult.add(uu2);
        
        List<Utilizador> result = instance.encontrarAmigosPorCoordenadas(latitude, longitude);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of econtrarAmigosPorNomeCidade method, of class Utilizador.
     */
    @Test
    public void testEcontrarAmigosPorNomeCidade() {
        System.out.println("econtrarAmigosPorNomeCidade");
        Cidade cidade = c1;
        Utilizador instance = u1;
        u1.moverParaCidade(c1);
        u2.moverParaCidade(c1);
        u3.moverParaCidade(c2);
        
        u1.adicionarAmizade(u2);
        u1.adicionarAmizade(u3);
        
        
        List<Utilizador> expResult = new ArrayList();
        expResult.add(u2);
        List<Utilizador> result = instance.econtrarAmigosPorNomeCidade(cidade);
        assertEquals(expResult, result);
        assertEquals(false, expResult.isEmpty());
    }
    
}
