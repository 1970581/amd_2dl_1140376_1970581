/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Mundo;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author KAMMIKAZI
 */
public class Parte32ControllerTest {
    Mundo mundo;
    Parte32Controller controller;
    public Parte32ControllerTest() {
        mundo = new Mundo();
        LerFicheiroController lerController = new LerFicheiroController(mundo);
        lerController.carregarFicheiroCidades("data/files10/cities10.txt");
        lerController.carregarFicheiroUtilizadores("data/files10/users10.txt");
        controller = new Parte32Controller(mundo); // Tem de ser o ultimo para ja termos as cidades/Utilizadores carregadas.
    }
        
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /**
     * Test of ordenarMayorPontos method, of class Parte31Controller.
     */
    @Test
    public void ordenarCidadeVisitas(){
        ArrayList<String> resultados = new ArrayList<>();
        resultados.add("city0:1");
        resultados.add("city1:1");
        resultados.add("city2:1");
        resultados.add("city3:2");
        resultados.add("city5:2");
        resultados.add("city6:2");
        resultados.add("city7:2");
        resultados.add("city4:3");
        resultados.add("city8:3");
        resultados.add("city9:3");
    System.out.println("ordenarMayorPontos");
        Parte32Controller instance = controller;
        ArrayList<String> expResult = resultados;
        ArrayList<String> result = instance.ordenarCidadeVisitas();
        assertEquals(expResult, result);
    }
    
}
/*

city0:1
city1:1
city2:1
city3:2
city4:3
city5:2
city6:2
city7:2
city8:3
city9:3



nick7,mail_7_@sapo.pt,city9
nick5,nick3,nick2,nick0
nick8,mail_8_@sapo.pt,city4,city7,city3
nick1,nick2,nick3,nick5,nick9
nick9,mail_9_@sapo.pt,city3,city8
nick5,nick8,nick4*/
