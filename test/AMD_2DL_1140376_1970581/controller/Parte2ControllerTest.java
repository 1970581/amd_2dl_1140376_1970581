/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class Parte2ControllerTest {
    
    Mundo mundo;
    Parte2Controller controller;
    
    public Parte2ControllerTest() {
        mundo = new Mundo();
        
        LerFicheiroController lerController = new LerFicheiroController(mundo);
        lerController.carregarFicheiroCidades("data/files10/cities10.txt");
        lerController.carregarFicheiroUtilizadores("data/files10/users10.txt");
        controller = new Parte2Controller(mundo); // Tem de ser o ultimo para ja termos as cidades/Utilizadores carregadas.
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNumeroCidades method, of class Parte2Controller.
     */
    @Test
    public void testGetNumeroCidades() {
        System.out.println("getNumeroCidades");
        Parte2Controller instance = controller;
        int expResult = 10;
        int result = instance.getNumeroCidades();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumeroUtilizadores method, of class Parte2Controller.
     */
    @Test
    public void testGetNumeroUtilizadores() {
        System.out.println("getNumeroUtilizadores");
        Parte2Controller instance = controller;
        int expResult = 10;
        int result = instance.getNumeroUtilizadores();
        assertEquals(expResult, result);
    }

    /**
     * Test of carregarFicheiroDistancias method, of class Parte2Controller.
     */
    @Test
    public void testCarregarFicheiroDistancias() {
        System.out.println("carregarFicheiroDistancias");
        String filename = "data/files10/cityConnections10.txt";
        Parte2Controller instance = controller;
        int expResult = 0; // 0 erros.
        expResult = 5 ;  //O ficheiro tem 5 ligaçoes repetidas. 2->3 e depois 3->2.
        int result = instance.carregarFicheiroDistancias(filename);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of existeUtilizador method, of class Parte2Controller.
     */
    @Test
    public void testExisteUtilizador() {
        System.out.println("existeUtilizador");
        String nomeUtilizador = "nick0";
        Parte2Controller instance = controller;
        boolean expResult = true;
        boolean result = instance.existeUtilizador(nomeUtilizador);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of caminhoMaisCurto method, of class Parte2Controller.
     */
    @Test
    public void testCaminhoMaisCurto() {
        System.out.println("caminhoMaisCurto");
        
        String u1Nome = "nick0";
        String u2Nome = "nick1";
        ArrayList<String> caminho = new ArrayList<String>();
        Parte2Controller instance = controller;
        instance.carregarFicheiroDistancias("data/files10/cityConnections10.txt");
        
        // Caminho 6-7-4 => 29 + 29 = 58 
        
        Double expResult = 58d;
        Double result = instance.caminhoMaisCurto(u1Nome, u2Nome, caminho);
        assertEquals(expResult, result);
        assertEquals(3, caminho.size());
        assertEquals("city6", caminho.get(0));
        assertEquals("city7", caminho.get(1));
        assertEquals("city4", caminho.get(2));
        
    }  

    /**
     * Test of amigosPerto method, of class Parte2Controller.
     */
    @Test
    public void testAmigosPerto() {
        System.out.println("amigosPerto"); //da cidade 6
        String nome = "nick0";  //Amigo de nick7 em c9 n4 em c8 e n3 em8 
        double kms = 1.0;
        ArrayList<String> resposta = new ArrayList();
        Parte2Controller instance = controller;
        instance.carregarFicheiroDistancias("data/files10/cityConnections10.txt");
        boolean expResult = true;
        boolean result = instance.amigosPerto(nome, kms, resposta);
        assertEquals(expResult, result);
        assertEquals(0, resposta.size());   // Nao devem existir amigos tao perto.
        
        kms = 77d;
        result = instance.amigosPerto(nome, kms, resposta);
        
        assertEquals(1, resposta.size());
        ArrayList<String> check = new ArrayList();
        assertTrue(resposta.get(0).contains("nick7"));
        assertTrue(resposta.get(0).contains("66.0 kms"));//66 kms  path [6 -> 7 -> 9] = 29 + 37 = 66 km
        
        kms = 1000d;
        resposta.clear();
        check = new ArrayList();
        result = instance.amigosPerto(nome, kms, resposta);
        assertEquals(3, resposta.size());
        assertTrue(resposta.get(0).contains("nick7"));
        assertTrue(resposta.get(0).contains("66.0 kms")); //66 kms 3->6 = 29   // 6 - 7 - 9 = 29 + 37 = 66
        assertTrue(resposta.get(1).contains("nick4"));
        assertTrue(resposta.get(1).contains("92.0 kms"));
        assertTrue(resposta.get(2).contains("nick3"));
        assertTrue(resposta.get(2).contains("92.0 kms"));
    }

    /**
     * Test of caminhoCurtoPassandoPorAmigos method, of class Parte2Controller.
     */
    @Test
    public void testCaminhoCurtoPassandoPorAmigos() {
        System.out.println("caminhoCurtoPassandoPorAmigos");
        String u1Nome = "nick0";
        String u2Nome = "nick4";  //Caso muito expecial onde as restricoes dos amigos tambem correspondem as cidades inicial e final.
        ArrayList<String> caminho = new ArrayList();
        Parte2Controller instance = controller;
        instance.carregarFicheiroDistancias("data/files10/cityConnections10.txt");
        
        Double result = instance.caminhoCurtoPassandoPorAmigos(u1Nome, u2Nome, caminho);
        assertTrue("Tem de existir Caminho", result.doubleValue()!= -1);
        
        
        assertTrue("Tem de Comecar na city6", caminho.get(0).equals("city6"));
        assertTrue("Tem de Terminar em city8", caminho.get(caminho.size()-1).equals("city8"));
        boolean contains = false;
        for(String s : caminho){
            if(s.equals("city6")) contains = true;
        }
        assertTrue("Tem de conter em city6", contains);
        contains = false;
        for(String s : caminho){
            if(s.equals("city8")) contains = true;
        }
        assertTrue("Tem de conter a city8", contains);
        
        
    }

    /**
     * Test of getCidadesComMaisAmigos method, of class Parte2Controller.
     */
    @Test
    public void testGetCidadesComMaisAmigos() {
        System.out.println("getCidadesComMaisAmigos");
        
        // nick0 é amigo de 4(8) 7(9) e 3(8)
        
        Parte2Controller instance = controller;
        Utilizador u0 = this.mundo.getRegistoUtilizador().getUtilizador("nick0");
        Cidade cidade8 = this.mundo.getRegistoCidade().getCidade("city8");
        
        LinkedHashSet<Cidade> expResult = new LinkedHashSet();
        expResult.add(cidade8);
        Iterable<Cidade> result = instance.getCidadesComMaisAmigos(u0);
        assertEquals(expResult, result);
        
        //Vamos agora adicionar amizade de nick5(9) ao nick0 para tambem aparecer a cidade 9 com 2 amigos.
        
        Utilizador u5 = this.mundo.getRegistoUtilizador().getUtilizador("nick5");
        u0.adicionarAmizade(u5);
        Cidade cidade9 = this.mundo.getRegistoCidade().getCidade("city9");
        expResult.add(cidade9);
        result = instance.getCidadesComMaisAmigos(u0);
        assertEquals(expResult, result);
        
        
        // Agora vamos limpar todas as amizades.
        u0.removerAmizade(this.mundo.getRegistoUtilizador().getUtilizador("nick4"));
        u0.removerAmizade(this.mundo.getRegistoUtilizador().getUtilizador("nick7"));
        u0.removerAmizade(this.mundo.getRegistoUtilizador().getUtilizador("nick3"));
        u0.removerAmizade(this.mundo.getRegistoUtilizador().getUtilizador("nick5"));
        expResult.clear();
        result = instance.getCidadesComMaisAmigos(u0);
        assertEquals(expResult, result);
        
        // Vamos repor as amizades de volta ....
        u0.adicionarAmizade(this.mundo.getRegistoUtilizador().getUtilizador("nick4"));
        u0.adicionarAmizade(this.mundo.getRegistoUtilizador().getUtilizador("nick7"));
        u0.adicionarAmizade(this.mundo.getRegistoUtilizador().getUtilizador("nick3"));
        
        
    }
    
}

/*                  Cidade : Ligacoes  *(repetida)
nick0 -> cidade6        0   3(38)  4(75)     
nick1 -> cidade4        1   9(68)  7(41)
nick2 -> cidade2        2   3(51)  6(55)  7(59)
nick3 -> cidade8        3   *2(59) 6(29)
nick4 -> cidade8        4   7 *0 9 8
nick5 -> cidade9        5   7 9 0 
nick6 -> cidade5        6   *3 1(69)
nick7 -> cidade9        7   9 8 0 6
nick8 -> cidade3        8   *4 *7 5 1 9
nick9 -> cidade8        9   6(76) 0

city0,city3,38
city0,city4,75
city1,city9,68
city1,city7,41
city2,city3,51
city2,city6,55
city2,city7,59
city3,city2,59
city3,city6,29
city4,city7,29
city4,city0,36
city4,city9,24 *
city4,city8,34
city5,city7,16
city5,city9,57
city5,city0,73
city6,city3,29 *
city6,city1,69
city7,city9,37
city7,city8,90
city7,city0,41
city7,city6,29
city8,city4,114
city8,city7,52
city8,city5,79
city8,city1,89
city8,city9,48 *
city9,city6,76
city9,city0,75


nick0,mail_0_@sapo.pt,city4,city6
nick7,nick4,nick3
nick1,mail_1_@sapo.pt,city5,city9,city4
nick6,nick2,nick8
nick2,mail_2_@sapo.pt,city0,city2
nick6,nick7,nick8,nick1
nick3,mail_3_@sapo.pt,city8
nick0,nick8,nick7,nick6,nick5
nick4,mail_4_@sapo.pt,city6,city8
nick9,nick0
nick5,mail_5_@sapo.pt,city7,city9
nick8,nick7,nick6,nick3,nick9
nick6,mail_6_@sapo.pt,city1,city5
nick3,nick5,nick1,nick2
nick7,mail_7_@sapo.pt,city9
nick5,nick3,nick2,nick0
nick8,mail_8_@sapo.pt,city4,city7,city3
nick1,nick2,nick3,nick5,nick9
nick9,mail_9_@sapo.pt,city3,city8
nick5,nick8,nick4


    */
