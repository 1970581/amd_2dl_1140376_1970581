/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoAmizade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author KAMMIKAZI
 */
public class UtilizadoresMaisInfluentesDaRedeControllerTest {
    
    static Mundo mundo;
    static RegistoUtilizador registoUtilizador;
    static Utilizador u1;
    static Utilizador u2;
    static Utilizador u3;
    static Utilizador u4;
    static Utilizador u5;
    static RegistoAmizade ra1;
    static RegistoAmizade ra2;
    static RegistoAmizade ra3;
    static RegistoAmizade ra4;
    static UtilizadoresMaisInfluentesDaRedeController controller;
    public UtilizadoresMaisInfluentesDaRedeControllerTest() {
    
    }
    
    @BeforeClass
    public static void setUpClass() {
    
    
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    mundo = new Mundo();
    registoUtilizador = mundo.getRegistoUtilizador();
    
    registoUtilizador.criarUtilizador("u1", "u1@email.com");
    registoUtilizador.criarUtilizador("u2", "u2@email.com");
    registoUtilizador.criarUtilizador("u3", "u3@email.com");
    registoUtilizador.criarUtilizador("u4", "u4@email.com");
    registoUtilizador.criarUtilizador("u5", "u5@email.com");
    
    u1 = registoUtilizador.getUtilizador("u1");
    u2 = registoUtilizador.getUtilizador("u2");
    u3 = registoUtilizador.getUtilizador("u3");
    u4 = registoUtilizador.getUtilizador("u4");
    u5 = registoUtilizador.getUtilizador("u5");
    /**
     * u1 tem 4 amizades, u2 tem 2 amizades, u3 tem 2 amizades, u4 tem duas amizades e u5 tem uma amizade;
     */
    u1.adicionarAmizade(u2);
    u1.adicionarAmizade(u3);
    u1.adicionarAmizade(u4);
    u1.adicionarAmizade(u5);
    
    u2.adicionarAmizade(u3);
    u2.adicionarAmizade(u1);
    
    u3.adicionarAmizade(u4);
    
    
    
    
    controller = new UtilizadoresMaisInfluentesDaRedeController(mundo);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isEmpty method, of class UtilizadoresMaisInfluentesDaRedeController.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        UtilizadoresMaisInfluentesDaRedeController instance = controller;
        boolean expResult = false;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of obterUtilizadoresMaisInfulentesDaRede method, of class UtilizadoresMaisInfluentesDaRedeController.
     */
    @Test
    public void testObterUtilizadoresMaisInfulentesDaRede() {
        System.out.println("obterUtilizadoresMaisInfulentesDaRede");
        UtilizadoresMaisInfluentesDaRedeController instance = controller;
        ArrayList utilizadoresEsperados = new ArrayList<>();
        utilizadoresEsperados.add(u1.getNome());
        ArrayList expResult = utilizadoresEsperados;
        ArrayList result = instance.obterUtilizadoresMaisInfulentesDaRede();
        assertEquals(expResult, result);
        
    }
    
}
