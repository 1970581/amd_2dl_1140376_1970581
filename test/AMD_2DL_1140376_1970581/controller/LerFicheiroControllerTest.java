/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.Utilizador;
import java.io.File;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class LerFicheiroControllerTest {
    
    Mundo mundo;
    LerFicheiroController controller;
    
    public LerFicheiroControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mundo = new Mundo();
        controller = new LerFicheiroController(mundo);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of carregarFicheiroCidades method, of class LerFicheiroController.
     */
    @Test
    public void testCarregarFicheiroCidades() {
        System.out.println("carregarFicheiroCidades");
        String filename = "data/files10/cities10.txt";
        LerFicheiroController instance = controller;
        boolean expResult = true;
        boolean result = instance.carregarFicheiroCidades(filename);
        assertEquals(expResult, result);
        
        assertEquals(false, instance.carregarFicheiroCidades("lixo"));
        
    }

    /**
     * Test of lerFileCidades method, of class LerFicheiroController.
     */
    @Test
    public void testLerFileCidades() {
        System.out.println("lerFileCidades");
        File file = new File("data/files10/cities10.txt");
        LerFicheiroController instance = controller;
        boolean expResult = true;
        boolean result = instance.lerFileCidades(file);
        assertEquals(expResult, result);
        
        /*
        city0,28,41.243345,-8.674084
        city1,72,41.237364,-8.846746
        city2,81,40.519841,-8.085113
        city3,42,41.118700,-8.589700
        city4,64,41.467407,-8.964340
        city5,74,41.337408,-8.291943
        city6,80,41.314965,-8.423371
        city7,11,40.822244,-8.794953
        city8,7,40.781886,-8.697502
        city9,65,40.851360,-8.136585
        */
        
        assertEquals(true,mundo.getRegistoCidade().getCidade("city0").equals(new Cidade("city0",28,41.243345f,-8.674084f)));
        assertEquals(true,mundo.getRegistoCidade().getCidade("city1").equals(new Cidade("city1",28,41.243345f,-8.674084f)));
        assertEquals(true,mundo.getRegistoCidade().getCidade("city2").equals(new Cidade("city2",28,41.243345f,-8.674084f)));
        assertEquals(true,mundo.getRegistoCidade().getCidade("city3").equals(new Cidade("city3",28,41.243345f,-8.674084f)));
        assertEquals(true,mundo.getRegistoCidade().getCidade("city4").equals(new Cidade("city4",28,41.243345f,-8.674084f)));
        assertEquals(true,mundo.getRegistoCidade().getCidade("city5").equals(new Cidade("city5",28,41.243345f,-8.674084f)));
        assertEquals(true,mundo.getRegistoCidade().getCidade("city6").equals(new Cidade("city6",28,41.243345f,-8.674084f)));
        assertEquals(true,mundo.getRegistoCidade().getCidade("city7").equals(new Cidade("city7",28,41.243345f,-8.674084f)));
        assertEquals(true,mundo.getRegistoCidade().getCidade("city8").equals(new Cidade("city8",28,41.243345f,-8.674084f)));
        assertEquals(true,mundo.getRegistoCidade().getCidade("city9").equals(new Cidade("city9",28,41.243345f,-8.674084f)));
        
        //city5,74,41.337408,-8.291943
        assertEquals(74,mundo.getRegistoCidade().getCidade("city5").getPontos() );
        assertEquals(41.337408f,mundo.getRegistoCidade().getCidade("city5").getLatitude(),0.00001);
        assertEquals(-8.291943f,mundo.getRegistoCidade().getCidade("city5").getLongitude(), 0.00001);
        //Amosragem.
        
        
        
        //NAO FAZ SENTIDO NO TESTE REESCREVER O CONTROLLER
        /*
        Scanner scanner;
        int numeroCidades = 0;
        String linha = "";
        try{
            scanner = new Scanner(file);
            while(scanner.hasNext())
            linha = scanner.nextLine();
            
        }
        catch(Exception e){assertEquals(false,true);}//falhar se apanahr exceptcao
        */
        
    }

    /**
     * Test of carregarFicheiroUtilizadores method, of class LerFicheiroController.
     */
    @Test
    public void testCarregarFicheiroUtilizadores() {
        System.out.println("carregarFicheiroUtilizadores");
        String filename = "data/files10/users10.txt";
        LerFicheiroController instance = controller;
        instance.carregarFicheiroCidades("data/files10/cities10.txt");
        boolean expResult = true;
        boolean result = instance.carregarFicheiroUtilizadores(filename);
        assertEquals(expResult, result);
        
        assertEquals(false, instance.carregarFicheiroUtilizadores("lixo"));
        
    }

    /**
     * Test of lerFileUtilizadores method, of class LerFicheiroController.
     */
    @Test
    public void testLerFileUtilizadores() {
        System.out.println("lerFileUtilizadores");
        File file = new File("data/files10/users10.txt");
        LerFicheiroController instance = controller;
        instance.carregarFicheiroCidades("data/files10/cities10.txt");
        boolean expResult = true;
        boolean result = instance.lerFileUtilizadores(file);
        assertEquals(expResult, result);
        
        int size = mundo.getRegistoUtilizador().getMapRegistoUtilizador().size();
        
        String name = "";
        String email = "";
        for(int i = 0; i< 0; i++ ){
            name = "nick" + i;
            assertEquals(name, mundo.getRegistoUtilizador().getUtilizador(name).getNome() );
            email = "mail_"+ i +"_@sapo.pt";
            assertEquals(email, mundo.getRegistoUtilizador().getUtilizador(name).getEmail());
        }
        
        /*
        nick0,mail_0_@sapo.pt,city4,city6
        nick7,nick4,nick3
        nick1,mail_1_@sapo.pt,city5,city9,city4
        nick6,nick2,nick8
        nick2,mail_2_@sapo.pt,city0,city2
        nick6,nick7,nick8,nick1
        nick3,mail_3_@sapo.pt,city8
        nick0,nick8,nick7,nick6,nick5
        nick4,mail_4_@sapo.pt,city6,city8
        nick9,nick0
        nick5,mail_5_@sapo.pt,city7,city9
        nick8,nick7,nick6,nick3,nick9
        nick6,mail_6_@sapo.pt,city1,city5
        nick3,nick5,nick1,nick2
        nick7,mail_7_@sapo.pt,city9
        nick5,nick3,nick2,nick0
        nick8,mail_8_@sapo.pt,city4,city7,city3
        nick1,nick2,nick3,nick5,nick9
        nick9,mail_9_@sapo.pt,city3,city8
        nick5,nick8,nick4
        */
        
    }

    /**
     * Test of lerFileUtilizadoresAmizades method, of class LerFicheiroController.
     */
    @Test
    public void testLerFileUtilizadoresAmizades() {
        System.out.println("lerFileUtilizadoresAmizades");
        File file = new File("data/files10/users10.txt");;
        LerFicheiroController instance = controller;
        instance.carregarFicheiroCidades("data/files10/cities10.txt");
        instance.lerFileUtilizadores(file);
        boolean expResult = true;
        boolean result = instance.lerFileUtilizadoresAmizades(file);
        assertEquals(expResult, result);
        
        Utilizador u0 = mundo.getRegistoUtilizador().getUtilizador("nick0");
        Utilizador u1 = mundo.getRegistoUtilizador().getUtilizador("nick1");
        Utilizador u2 = mundo.getRegistoUtilizador().getUtilizador("nick2");
        Utilizador u3 = mundo.getRegistoUtilizador().getUtilizador("nick3");
        Utilizador u4 = mundo.getRegistoUtilizador().getUtilizador("nick4");
        Utilizador u5 = mundo.getRegistoUtilizador().getUtilizador("nick5");
        Utilizador u6 = mundo.getRegistoUtilizador().getUtilizador("nick6");
        Utilizador u7 = mundo.getRegistoUtilizador().getUtilizador("nick7");
        Utilizador u8 = mundo.getRegistoUtilizador().getUtilizador("nick8");
        Utilizador u9 = mundo.getRegistoUtilizador().getUtilizador("nick9");
        
        
        assertEquals(true,u0.getRegistoAmizade().isAmigo(u7));
        assertEquals(true,u0.getRegistoAmizade().isAmigo(u4));
        assertEquals(true,u0.getRegistoAmizade().isAmigo(u3));
               
        
        //nick1,mail_1_@sapo.pt,city5,city9,city4
        //nick6,nick2,nick8
        
        assertEquals(true,u1.getRegistoAmizade().isAmigo(u6));
        assertEquals(true,u1.getRegistoAmizade().isAmigo(u2));
        assertEquals(true,u1.getRegistoAmizade().isAmigo(u8));
        
        
        //nick2,mail_2_@sapo.pt,city0,city2
        //nick6,nick7,nick8,nick1
        assertEquals(true,u2.getRegistoAmizade().isAmigo(u6));
        assertEquals(true,u2.getRegistoAmizade().isAmigo(u7));
        assertEquals(true,u2.getRegistoAmizade().isAmigo(u8));
        assertEquals(true,u2.getRegistoAmizade().isAmigo(u1));
        
        //nick3,mail_3_@sapo.pt,city8
        //nick0,nick8,nick7,nick6,nick5
        assertEquals(true,u3.getRegistoAmizade().isAmigo(u0));
        assertEquals(true,u3.getRegistoAmizade().isAmigo(u8));
        assertEquals(true,u3.getRegistoAmizade().isAmigo(u7));
        assertEquals(true,u3.getRegistoAmizade().isAmigo(u6));
        assertEquals(true,u3.getRegistoAmizade().isAmigo(u5));
        
        //nick4,mail_4_@sapo.pt,city6,city8
        //nick9,nick0
        
        assertEquals(true,u4.getRegistoAmizade().isAmigo(u9));
        assertEquals(true,u4.getRegistoAmizade().isAmigo(u0));
        
        //nick5,mail_5_@sapo.pt,city7,city9
        //nick8,nick7,nick6,nick3,nick9
        assertEquals(true,u5.getRegistoAmizade().isAmigo(u8));
        assertEquals(true,u5.getRegistoAmizade().isAmigo(u7));
        assertEquals(true,u5.getRegistoAmizade().isAmigo(u6));
        assertEquals(true,u5.getRegistoAmizade().isAmigo(u3));
        assertEquals(true,u5.getRegistoAmizade().isAmigo(u9));
        
        //nick6,mail_6_@sapo.pt,city1,city5
        //nick3,nick5,nick1,nick2
        assertEquals(true,u6.getRegistoAmizade().isAmigo(u3));
        assertEquals(true,u6.getRegistoAmizade().isAmigo(u5));
        assertEquals(true,u6.getRegistoAmizade().isAmigo(u1));
        assertEquals(true,u6.getRegistoAmizade().isAmigo(u2));
        
        //nick7,mail_7_@sapo.pt,city9
        //nick5,nick3,nick2,nick0
        assertEquals(true,u7.getRegistoAmizade().isAmigo(u5));
        assertEquals(true,u7.getRegistoAmizade().isAmigo(u3));
        assertEquals(true,u7.getRegistoAmizade().isAmigo(u2));
        assertEquals(true,u7.getRegistoAmizade().isAmigo(u0));
        
        //nick8,mail_8_@sapo.pt,city4,city7,city3
        //nick1,nick2,nick3,nick5,nick9
        assertEquals(true,u8.getRegistoAmizade().isAmigo(u1));
        assertEquals(true,u8.getRegistoAmizade().isAmigo(u2));
        assertEquals(true,u8.getRegistoAmizade().isAmigo(u3));
        assertEquals(true,u8.getRegistoAmizade().isAmigo(u5));
        assertEquals(true,u8.getRegistoAmizade().isAmigo(u9));
        
        
        //nick9,mail_9_@sapo.pt,city3,city8
        //nick5,nick8,nick4
        assertEquals(true,u9.getRegistoAmizade().isAmigo(u5));
        assertEquals(true,u9.getRegistoAmizade().isAmigo(u8));
        assertEquals(true,u9.getRegistoAmizade().isAmigo(u4));
        
        
        assertEquals(false,u9.getRegistoAmizade().isAmigo(u9));//FALSE
        assertEquals(false,u9.getRegistoAmizade().isAmigo(u0));//FALSE
    }
    
}
