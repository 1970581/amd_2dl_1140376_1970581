/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class AdicionarLigacaoAmizadeControllerTest {
    static Mundo mundo;
    static RegistoCidade registoCidade;
    static RegistoUtilizador registoUtilizador;
    static Utilizador u1;
    static Utilizador u2;
    static Utilizador u3;
    static Utilizador u4;
    static Cidade c1;
    static Cidade c2;
    static Cidade c3;
    static Cidade c4;
    static Cidade c5;
    static Cidade c6;
    static AdicionarLigacaoAmizadeController controller;
    
    
    public AdicionarLigacaoAmizadeControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mundo = new Mundo();
        registoUtilizador = mundo.getRegistoUtilizador();
        registoCidade = mundo.getRegistoCidade();
        registoUtilizador.criarUtilizador("u1", "u1@email.com");
        registoUtilizador.criarUtilizador("u2", "u2@email.com");
        registoUtilizador.criarUtilizador("u3", "u3@email.com");
        registoUtilizador.criarUtilizador("u4", "u4@email.com");
        
        registoCidade.adicionarCidade("c1", 1, 1f,2f);
        registoCidade.adicionarCidade("c2", 2, 2f,2f);
        registoCidade.adicionarCidade("c3", 3, 3f,2f);
        registoCidade.adicionarCidade("c4", 4, 4f,2f);
        registoCidade.adicionarCidade("c5", 5, 5f,2f);
        registoCidade.adicionarCidade("c6", 5, 5f,2f);
        
        u1 = registoUtilizador.getUtilizador("u1");
        u2 = registoUtilizador.getUtilizador("u2");
        u3 = registoUtilizador.getUtilizador("u3");
        u4 = registoUtilizador.getUtilizador("u4");
        
        c1 = registoCidade.getCidade("c1");
        c2 = registoCidade.getCidade("c2");
        c3 = registoCidade.getCidade("c3");
        c4 = registoCidade.getCidade("c4");
        c5 = registoCidade.getCidade("c5");
        c6 = registoCidade.getCidade("c6");
        
        controller = new AdicionarLigacaoAmizadeController(mundo);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isUtilizador method, of class AdicionarLigacaoAmizadeController.
     */
    @Test
    public void testIsUtilizador() {
        System.out.println("isUtilizador");
        String nome = u1.getNome();
        AdicionarLigacaoAmizadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.isUtilizador(nome);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isAmigo method, of class AdicionarLigacaoAmizadeController.
     */
    @Test
    public void testIsAmigo() {
        System.out.println("isAmigo");
        String nomeUtilizador = u1.getNome();
        String nomeAmigo = u2.getNome();
        
        u1.adicionarAmizade(u2);
        
        AdicionarLigacaoAmizadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.isAmigo(nomeUtilizador, nomeAmigo);
        assertEquals(expResult, result);
        assertEquals(false, instance.isAmigo(u1.getNome(), u3.getNome()));
        
    }

    /**
     * Test of criarAmizade method, of class AdicionarLigacaoAmizadeController.
     */
    @Test
    public void testCriarAmizade() {
        System.out.println("criarAmizade");
        String nomeUtilizador = u1.getNome();
        String nomeAmigo = u2.getNome();
        AdicionarLigacaoAmizadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.criarAmizade(nomeUtilizador, nomeAmigo);
        assertEquals(expResult, result);
        assertEquals(false, instance.criarAmizade(u1.getNome(), "lixo"));
        
    }
    
}
