/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author KAMMIKAZI
 */
public class RemoverLigacaoAmizadeControllerTest {
    static Mundo mundo;
    static RegistoUtilizador registoUtilizador;
    static Utilizador u1;
    static Utilizador u2;
    static Utilizador u3;
    static Utilizador u4;
    static Utilizador u5;
    static RemoverLigacaoAmizadeController controller;
    
    public RemoverLigacaoAmizadeControllerTest() {
    
    
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    mundo = new Mundo();
    registoUtilizador = mundo.getRegistoUtilizador();
    
    registoUtilizador.criarUtilizador("u1", "u1@email.com");
    registoUtilizador.criarUtilizador("u2", "u2@email.com");
    registoUtilizador.criarUtilizador("u3", "u3@email.com");
    registoUtilizador.criarUtilizador("u4", "u4@email.com");
    registoUtilizador.criarUtilizador("u5", "u5@email.com");
    
    u1 = registoUtilizador.getUtilizador("u1");
    u2 = registoUtilizador.getUtilizador("u2");
    u3 = registoUtilizador.getUtilizador("u3");
    u4 = registoUtilizador.getUtilizador("u4");
    u5 = registoUtilizador.getUtilizador("u5");
    /**
     * u1 tem 4 amizades, u2 tem 2 amizades, u3 tem 2 amizades, u4 tem duas amizades e u5 tem uma amizade;
     */
    u1.adicionarAmizade(u2);
    u1.adicionarAmizade(u3);
    u1.adicionarAmizade(u4);
    u1.adicionarAmizade(u5);
    
    u2.adicionarAmizade(u3);
    u2.adicionarAmizade(u1);
    
    u3.adicionarAmizade(u4);
    
    controller = new RemoverLigacaoAmizadeController(mundo);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isUtilizador method, of class RemoverLigacaoAmizadeController.
     */
    @Test
    public void testIsUtilizador() {
        System.out.println("isUtilizador");
        String nome = "u3";
        RemoverLigacaoAmizadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.isUtilizador(nome);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isAmigo method, of class RemoverLigacaoAmizadeController.
     */
    @Test
    public void testIsAmigo() {
        System.out.println("isAmigo");
        String nomeUtilizador = "u1";
        String nomeAmigo = "u2";
        RemoverLigacaoAmizadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.isAmigo(nomeUtilizador, nomeAmigo);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of removerAmizade method, of class RemoverLigacaoAmizadeController.
     */
    @Test
    public void testRemoverAmizade() {
        System.out.println("removerAmizade");
        String nomeUtilizador = "u1";
        String nomeAmigo = "u2";
        RemoverLigacaoAmizadeController instance = controller;
        boolean expResult = false;
        boolean result = instance.removerAmizade(nomeUtilizador, nomeAmigo);
        assertEquals(expResult, result);
        
    }
    
}
