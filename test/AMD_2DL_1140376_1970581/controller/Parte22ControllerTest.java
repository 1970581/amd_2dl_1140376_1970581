/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Graph;
import AMD_2DL_1140376_1970581.GraphMapAlgorithms;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class Parte22ControllerTest {
    
    Mundo mundo;
    Parte22Controller controller;
    
    public Parte22ControllerTest() {
        mundo = new Mundo();
        
        LerFicheiroController lerController = new LerFicheiroController(mundo);
        lerController.carregarFicheiroCidades("data/files10/cities10.txt");
        lerController.carregarFicheiroUtilizadores("data/files10/users10.txt");
        controller = new Parte22Controller(mundo); // Tem de ser o ultimo para ja termos as cidades/Utilizadores carregadas.
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNumeroUtilizadores method, of class Parte22Controller.
     */
    @Test
    public void testGetNumeroUtilizadores() {
        System.out.println("getNumeroUtilizadores");
        Parte22Controller instance = controller;
        int expResult = 10;
        int result = instance.getNumeroUtilizadores();
        assertEquals(expResult, result);
    }

    /**
     * Test of existeUtilizador method, of class Parte22Controller.
     */
    @Test
    public void testExisteUtilizador() {
        System.out.println("existeUtilizador");
        String nomeUtilizador = "nick0";
        Parte22Controller instance = controller;
        boolean expResult = true;
        boolean result = instance.existeUtilizador(nomeUtilizador);
        assertEquals(expResult, result);
    }

    /**
     * Test of carregarMapa method, of class Parte22Controller.
     */
    @Test
    public void testCarregarMapa() {
        System.out.println("carregarMapa");
        Parte22Controller instance = controller;
        int expResult = 0;
        int result = instance.carregarMapa();
        assertEquals(expResult, result); //Erros = 0
        
        assertEquals(10,controller.getGraph().numVertices());
        
        Utilizador u0 = mundo.getRegistoUtilizador().getUtilizador("nick0");
        boolean fail = false;
        
        for(Utilizador u : controller.getGraph().vertices()){
            for(Utilizador amigo : u.getRegistoAmizade().getRegistoAmizade().values()){
                 if(controller.getGraph().getEdge(u, amigo) == null) fail = true;
            }
        }
        assertFalse("Teste exaustivo de amigos falhou.", fail);
        
    }

    /**
     * Test of distanciaRelacional method, of class Parte22Controller.
     */
    @Test
    public void testDistanciaRelacional() {
        System.out.println("distanciaRelacional");
        String nome1 = "nick0";
        String nome2 = "";
        Parte22Controller instance = controller;
        controller.carregarMapa();
        int expResult = 0;
        int result = instance.distanciaRelacional(nome1, nome2);
        
        assertEquals(0,instance.distanciaRelacional("nick0", "nick4"));
        assertEquals(-1,instance.distanciaRelacional("nick0", "nick0"));
        assertEquals(1,instance.distanciaRelacional("nick0", "nick6"));
        
    }

    /**
     * Test of getGraph method, of class Parte22Controller.
     */
    @Test
    public void testGetGraph() {
        System.out.println("getGraph");
        Parte22Controller instance = controller;
        controller.carregarMapa();
        Graph<Utilizador, String> result = instance.getGraph();
        assertEquals(10, result.numVertices());
        
    }

    /**
     * Test of utilizadoresADistanciaRelacionalX method, of class Parte22Controller.
     */
    @Test
    public void testUtilizadoresADistanciaRelacionalX() {
        System.out.println("utilizadoresADistanciaRelacionalX");
        String nome = "nick0";
        int distancia = 0;
        Parte22Controller instance = controller;
        controller.carregarMapa();
        LinkedList<String> expResult = new LinkedList<>();
        LinkedList<String> result = instance.utilizadoresADistanciaRelacionalX(nome, distancia);
        assertEquals(expResult, result);
        // distancia relacional 0  - amigos directos.  - dist edge 1
        // distancia relacional 1  - amigos de amigos directos.  - dist edge 2
        // distancia relacional 2  - amigos de amigos de amigos directos. - dist edge 3
        // distancia sempre -1 para converter distancia em edges em distancia relacional. 
        assertEquals( 0, instance.utilizadoresADistanciaRelacionalX("nick0", 1-1).size()); //Dist relacional < 0 , zero nao entra.
        assertEquals( 3, instance.utilizadoresADistanciaRelacionalX("nick0", 2-1).size()); // Distancia relaciona < 1, ou seja 0
        result = instance.utilizadoresADistanciaRelacionalX("nick0", 2-1);  // Distancia relacional <1 , ou seja 0, todos os amigos diretos.
        assertTrue(result.contains("nick7"));
        assertTrue(result.contains("nick4"));
        assertTrue(result.contains("nick3"));
        assertFalse(result.contains("nick0"));
        result = instance.utilizadoresADistanciaRelacionalX("nick0", 3-1); //Distancia relacional < 2, ou seja amigos de amigos diretos. 2 edges de max dist.
        assertTrue(result.contains("nick7"));
        assertTrue(result.contains("nick4"));
        assertTrue(result.contains("nick3"));
        assertFalse(result.contains("nick0"));
        assertTrue(result.contains("nick9"));
    }

    /**
     * Test of utilizadoresMaisInfluentes method, of class Parte22Controller.
     */
    @Test
    public void testUtilizadoresMaisInfluentes() {
        System.out.println("utilizadoresMaisInfluentes");
        LinkedList<String> info = new LinkedList<>();
        Parte22Controller instance = controller;
        controller.carregarMapa();
        boolean expResult = true;
        boolean result = instance.utilizadoresMaisInfluentes(info);
        assertEquals(expResult, result);
        
        //Graph <Utilizador,String> gt = GraphMapAlgorithms.mapTransitiveClosure(controller.getGraph());
        //System.out.println(gt);
        //Basta extrair a informação do transitive clousure, ver abaixo para confirmarmos os que devem tar na lista.
        //nick 3 5 7 8 9 com max de 2.
        assertTrue(info.contains("nick3"));
        assertTrue(info.contains("nick5"));
        assertTrue(info.contains("nick7"));
        assertTrue(info.contains("nick8"));
        assertTrue(info.contains("nick9"));
        assertTrue(info.size() == 6);
        assertTrue(info.getLast().contains("2"));
        
        
        
        /*  TESTE COM FICHEIRO DE 300 users, nao usar. LENTO!!!!  300+ Segundos. 5 min.
        mundo = new Mundo();
        LerFicheiroController lerController = new LerFicheiroController(mundo);
        lerController.carregarFicheiroCidades("data/files300/cities300.txt");
        lerController.carregarFicheiroUtilizadores("data/files300/users300.txt");
        controller = new Parte22Controller(mundo);
        controller.carregarMapa();
        info.clear();
        result = controller.utilizadoresMaisInfluentes(info);
        assertEquals(expResult, result);
        */
    }

}


    /*
nick0,mail_0_@sapo.pt,city4,city6
nick7,nick4,nick3
nick1,mail_1_@sapo.pt,city5,city9,city4
nick6,nick2,nick8
nick2,mail_2_@sapo.pt,city0,city2
nick6,nick7,nick8,nick1
nick3,mail_3_@sapo.pt,city8
nick0,nick8,nick7,nick6,nick5
nick4,mail_4_@sapo.pt,city6,city8
nick9,nick0
nick5,mail_5_@sapo.pt,city7,city9
nick8,nick7,nick6,nick3,nick9
nick6,mail_6_@sapo.pt,city1,city5
nick3,nick5,nick1,nick2
nick7,mail_7_@sapo.pt,city9
nick5,nick3,nick2,nick0
nick8,mail_8_@sapo.pt,city4,city7,city3
nick1,nick2,nick3,nick5,nick9
nick9,mail_9_@sapo.pt,city3,city8
nick5,nick8,nick4
    */


/*
Graph: 10 vertices, 90 edges
nick0 mail_0_@sapo.pt 144 em city6 (0): 
      (nick0 & nick3) - 3.0 - nick1 mail_1_@sapo.pt 203 em city4
      (nick0 & nick3) - 2.0 - nick2 mail_2_@sapo.pt 109 em city2
      (nick0 & nick3) - 1.0 - nick3 mail_3_@sapo.pt 7 em city8
      (nick0 & nick4) - 1.0 - nick4 mail_4_@sapo.pt 87 em city8
      (nick0 & nick3) - 2.0 - nick5 mail_5_@sapo.pt 76 em city9
      (nick0 & nick3) - 2.0 - nick6 mail_6_@sapo.pt 146 em city5
      (nick0 & nick7) - 1.0 - nick7 mail_7_@sapo.pt 65 em city9
      (nick0 & nick3) - 2.0 - nick8 mail_8_@sapo.pt 117 em city3
      (nick0 & nick4) - 2.0 - nick9 mail_9_@sapo.pt 49 em city8

nick1 mail_1_@sapo.pt 203 em city4 (1): 
      (nick0 & nick3) - 3.0 - nick0 mail_0_@sapo.pt 144 em city6
      (nick1 & nick2) - 1.0 - nick2 mail_2_@sapo.pt 109 em city2
      (nick1 & nick6) - 2.0 - nick3 mail_3_@sapo.pt 7 em city8
      (nick1 & nick6) - 3.0 - nick4 mail_4_@sapo.pt 87 em city8
      (nick1 & nick6) - 2.0 - nick5 mail_5_@sapo.pt 76 em city9
      (nick1 & nick6) - 1.0 - nick6 mail_6_@sapo.pt 146 em city5
      (nick1 & nick2) - 2.0 - nick7 mail_7_@sapo.pt 65 em city9
      (nick1 & nick8) - 1.0 - nick8 mail_8_@sapo.pt 117 em city3
      (nick1 & nick6) - 2.0 - nick9 mail_9_@sapo.pt 49 em city8

nick2 mail_2_@sapo.pt 109 em city2 (2): 
      (nick0 & nick3) - 2.0 - nick0 mail_0_@sapo.pt 144 em city6
      (nick1 & nick2) - 1.0 - nick1 mail_1_@sapo.pt 203 em city4
      (nick2 & nick6) - 2.0 - nick3 mail_3_@sapo.pt 7 em city8
      (nick2 & nick6) - 3.0 - nick4 mail_4_@sapo.pt 87 em city8
      (nick2 & nick6) - 2.0 - nick5 mail_5_@sapo.pt 76 em city9
      (nick2 & nick6) - 1.0 - nick6 mail_6_@sapo.pt 146 em city5
      (nick2 & nick7) - 1.0 - nick7 mail_7_@sapo.pt 65 em city9
      (nick2 & nick8) - 1.0 - nick8 mail_8_@sapo.pt 117 em city3
      (nick2 & nick6) - 2.0 - nick9 mail_9_@sapo.pt 49 em city8

nick3 mail_3_@sapo.pt 7 em city8 (3): 
      (nick0 & nick3) - 1.0 - nick0 mail_0_@sapo.pt 144 em city6
      (nick1 & nick6) - 2.0 - nick1 mail_1_@sapo.pt 203 em city4
      (nick2 & nick6) - 2.0 - nick2 mail_2_@sapo.pt 109 em city2
      (nick0 & nick3) - 2.0 - nick4 mail_4_@sapo.pt 87 em city8
      (nick3 & nick5) - 1.0 - nick5 mail_5_@sapo.pt 76 em city9
      (nick3 & nick6) - 1.0 - nick6 mail_6_@sapo.pt 146 em city5
      (nick3 & nick7) - 1.0 - nick7 mail_7_@sapo.pt 65 em city9
      (nick3 & nick8) - 1.0 - nick8 mail_8_@sapo.pt 117 em city3
      (nick0 & nick3) - 2.0 - nick9 mail_9_@sapo.pt 49 em city8

nick4 mail_4_@sapo.pt 87 em city8 (4): 
      (nick0 & nick4) - 1.0 - nick0 mail_0_@sapo.pt 144 em city6
      (nick1 & nick6) - 3.0 - nick1 mail_1_@sapo.pt 203 em city4
      (nick2 & nick6) - 3.0 - nick2 mail_2_@sapo.pt 109 em city2
      (nick0 & nick3) - 2.0 - nick3 mail_3_@sapo.pt 7 em city8
      (nick0 & nick3) - 2.0 - nick5 mail_5_@sapo.pt 76 em city9
      (nick0 & nick3) - 3.0 - nick6 mail_6_@sapo.pt 146 em city5
      (nick0 & nick4) - 2.0 - nick7 mail_7_@sapo.pt 65 em city9
      (nick0 & nick3) - 2.0 - nick8 mail_8_@sapo.pt 117 em city3
      (nick4 & nick9) - 1.0 - nick9 mail_9_@sapo.pt 49 em city8

nick5 mail_5_@sapo.pt 76 em city9 (5): 
      (nick0 & nick3) - 2.0 - nick0 mail_0_@sapo.pt 144 em city6
      (nick1 & nick6) - 2.0 - nick1 mail_1_@sapo.pt 203 em city4
      (nick2 & nick6) - 2.0 - nick2 mail_2_@sapo.pt 109 em city2
      (nick3 & nick5) - 1.0 - nick3 mail_3_@sapo.pt 7 em city8
      (nick0 & nick3) - 2.0 - nick4 mail_4_@sapo.pt 87 em city8
      (nick5 & nick6) - 1.0 - nick6 mail_6_@sapo.pt 146 em city5
      (nick5 & nick7) - 1.0 - nick7 mail_7_@sapo.pt 65 em city9
      (nick5 & nick8) - 1.0 - nick8 mail_8_@sapo.pt 117 em city3
      (nick5 & nick9) - 1.0 - nick9 mail_9_@sapo.pt 49 em city8

nick6 mail_6_@sapo.pt 146 em city5 (6): 
      (nick0 & nick3) - 2.0 - nick0 mail_0_@sapo.pt 144 em city6
      (nick1 & nick6) - 1.0 - nick1 mail_1_@sapo.pt 203 em city4
      (nick2 & nick6) - 1.0 - nick2 mail_2_@sapo.pt 109 em city2
      (nick3 & nick6) - 1.0 - nick3 mail_3_@sapo.pt 7 em city8
      (nick0 & nick3) - 3.0 - nick4 mail_4_@sapo.pt 87 em city8
      (nick5 & nick6) - 1.0 - nick5 mail_5_@sapo.pt 76 em city9
      (nick2 & nick6) - 2.0 - nick7 mail_7_@sapo.pt 65 em city9
      (nick1 & nick6) - 2.0 - nick8 mail_8_@sapo.pt 117 em city3
      (nick0 & nick3) - 2.0 - nick9 mail_9_@sapo.pt 49 em city8

nick7 mail_7_@sapo.pt 65 em city9 (7): 
      (nick0 & nick7) - 1.0 - nick0 mail_0_@sapo.pt 144 em city6
      (nick1 & nick2) - 2.0 - nick1 mail_1_@sapo.pt 203 em city4
      (nick2 & nick7) - 1.0 - nick2 mail_2_@sapo.pt 109 em city2
      (nick3 & nick7) - 1.0 - nick3 mail_3_@sapo.pt 7 em city8
      (nick0 & nick4) - 2.0 - nick4 mail_4_@sapo.pt 87 em city8
      (nick5 & nick7) - 1.0 - nick5 mail_5_@sapo.pt 76 em city9
      (nick2 & nick6) - 2.0 - nick6 mail_6_@sapo.pt 146 em city5
      (nick2 & nick7) - 2.0 - nick8 mail_8_@sapo.pt 117 em city3
      (nick0 & nick4) - 2.0 - nick9 mail_9_@sapo.pt 49 em city8

nick8 mail_8_@sapo.pt 117 em city3 (8): 
      (nick0 & nick3) - 2.0 - nick0 mail_0_@sapo.pt 144 em city6
      (nick1 & nick8) - 1.0 - nick1 mail_1_@sapo.pt 203 em city4
      (nick2 & nick8) - 1.0 - nick2 mail_2_@sapo.pt 109 em city2
      (nick3 & nick8) - 1.0 - nick3 mail_3_@sapo.pt 7 em city8
      (nick0 & nick3) - 2.0 - nick4 mail_4_@sapo.pt 87 em city8
      (nick5 & nick8) - 1.0 - nick5 mail_5_@sapo.pt 76 em city9
      (nick1 & nick6) - 2.0 - nick6 mail_6_@sapo.pt 146 em city5
      (nick2 & nick7) - 2.0 - nick7 mail_7_@sapo.pt 65 em city9
      (nick8 & nick9) - 1.0 - nick9 mail_9_@sapo.pt 49 em city8

nick9 mail_9_@sapo.pt 49 em city8 (9): 
      (nick0 & nick4) - 2.0 - nick0 mail_0_@sapo.pt 144 em city6
      (nick1 & nick6) - 2.0 - nick1 mail_1_@sapo.pt 203 em city4
      (nick2 & nick6) - 2.0 - nick2 mail_2_@sapo.pt 109 em city2
      (nick0 & nick3) - 2.0 - nick3 mail_3_@sapo.pt 7 em city8
      (nick4 & nick9) - 1.0 - nick4 mail_4_@sapo.pt 87 em city8
      (nick5 & nick9) - 1.0 - nick5 mail_5_@sapo.pt 76 em city9
      (nick0 & nick3) - 2.0 - nick6 mail_6_@sapo.pt 146 em city5
      (nick0 & nick4) - 2.0 - nick7 mail_7_@sapo.pt 65 em city9
      (nick8 & nick9) - 1.0 - nick8 mail_8_@sapo.pt 117 em city3
*/