/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class CriarUtilizadorControllerTest {
    static Mundo mundo;
    static RegistoCidade registoCidade;
    static RegistoUtilizador registoUtilizador;
    static Utilizador utilizador1;
    static Utilizador utilizador2;
    static Cidade cidade1;
    static Cidade cidade2;
    static CriarUtilizadorController controller;
    
    
    public CriarUtilizadorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mundo = new Mundo();
        registoUtilizador = mundo.getRegistoUtilizador();
        registoCidade = mundo.getRegistoCidade();
        utilizador1 = new Utilizador("u1", "u1@email.com");
        utilizador2 = new Utilizador("u2", "u2@email.com");
        cidade1 = new Cidade("cidade1", 1, 2f,3f);
        cidade2 = new Cidade("cidade2", 1, 2f,3f);
        registoUtilizador.criarUtilizador(utilizador1.getNome(), utilizador1.getEmail());
        registoCidade.adicionarCidade(cidade1.getNome(), cidade1.getPontos(), cidade1.getLatitude(),cidade1.getLongitude());
        controller = new CriarUtilizadorController(mundo);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of existeUtilizadorComNome method, of class CriarUtilizadorController.
     */
    @Test
    public void testExisteUtilizadorComNome() {
        System.out.println("existeUtilizadorComNome");
        String nome = utilizador1.getNome();
        CriarUtilizadorController instance = controller;
        boolean expResult = true;
        boolean result = instance.existeUtilizadorComNome(nome);
        assertEquals(expResult, result && !instance.existeUtilizadorComNome("lixo"));
        
    }

    /**
     * Test of existeUtilizadorComEmail method, of class CriarUtilizadorController.
     */
    @Test
    public void testExisteUtilizadorComEmail() {
        System.out.println("existeUtilizadorComEmail");
        String email = utilizador1.getEmail();
        CriarUtilizadorController instance = controller;
        boolean expResult = true;
        boolean result = instance.existeUtilizadorComEmail(email);
        assertEquals(expResult, result && ! instance.existeUtilizadorComEmail("lixo"));
        
    }

    /**
     * Test of existeCidadeNome method, of class CriarUtilizadorController.
     */
    @Test
    public void testExisteCidadeNome() {
        System.out.println("existeCidadeNome");
        String nome = cidade1.getNome();
        CriarUtilizadorController instance = controller;
        boolean expResult = true;
        boolean result = instance.existeCidadeNome(nome);
        assertEquals(expResult, result && !instance.existeCidadeNome("lixo"));
        
    }

    /**
     * Test of criarUtilizadorComCidadeInicial method, of class CriarUtilizadorController.
     */
    @Test
    public void testCriarUtilizadorComCidadeInicial() {
        System.out.println("criarUtilizadorComCidadeInicial");
        String nome = utilizador2.getNome();
        String email = utilizador2.getEmail();
        String cidade = cidade1.getNome();
        CriarUtilizadorController instance = controller;
        
        
        
        
        boolean expResult = true;
        boolean result = instance.criarUtilizadorComCidadeInicial(nome, email, cidade); //Basico
        assertEquals(expResult, result);
        assertEquals(false, instance.criarUtilizadorComCidadeInicial(nome, email, cidade2.getNome())); // Deve falhar devido a usarmos cidade nao listada no registoCidades
        boolean test3 = instance.criarUtilizadorComCidadeInicial(utilizador1.getNome(), email, cidade1.getNome()); //Deve falhar pois o nome é de um utilizador ja existente.
        assertEquals(false, test3);
        
        
    }
    
}
