/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author KAMMIKAZI
 */
public class DevolverAmigosPorLocalizacaoControllerTest {

    static Mundo mundo;
    static RegistoUtilizador registoUtilizador;
    static RegistoCidade registoCidade;
    static Utilizador u1;
    static Utilizador u2;
    static Utilizador u3;
    static Utilizador u4;
    static Utilizador u5;
    static Utilizador u6;
    static Utilizador u7;
    static Utilizador u8;
    static Cidade c1;
    static Cidade c2;
    static Cidade c3;
    static Cidade c4;
    static Cidade c5;
    static Cidade c6;
    static DevolverAmigosPorLocalizacaoController controller;

    public DevolverAmigosPorLocalizacaoControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        mundo = new Mundo();
        registoUtilizador = mundo.getRegistoUtilizador();
        registoCidade = mundo.getRegistoCidade();
        registoUtilizador.criarUtilizador("u1", "u1@email.com");
        registoUtilizador.criarUtilizador("u2", "u2@email.com");
        registoUtilizador.criarUtilizador("u3", "u3@email.com");
        registoUtilizador.criarUtilizador("u4", "u4@email.com");
        registoUtilizador.criarUtilizador("u5", "u5@email.com");
        registoUtilizador.criarUtilizador("u6", "u6@email.com");
        registoUtilizador.criarUtilizador("u7", "u7@email.com");
        registoUtilizador.criarUtilizador("u8", "u8@email.com");

        u1 = registoUtilizador.getUtilizador("u1");
        u2 = registoUtilizador.getUtilizador("u2");
        u3 = registoUtilizador.getUtilizador("u3");
        u4 = registoUtilizador.getUtilizador("u4");
        u5 = registoUtilizador.getUtilizador("u5");
        u6 = registoUtilizador.getUtilizador("u6");
        u7 = registoUtilizador.getUtilizador("u7");
        u8 = registoUtilizador.getUtilizador("u8");
        /**
         * u1 tem 4 amizades, u2 tem 2 amizades, u3 tem 2 amizades, u4 tem duas
         * amizades e u5 tem uma amizade;
         */
        u1.adicionarAmizade(u2);
        u1.adicionarAmizade(u3);
        u1.adicionarAmizade(u4);
        u1.adicionarAmizade(u5);

        u2.adicionarAmizade(u3);
        u2.adicionarAmizade(u1);

        u3.adicionarAmizade(u4);

        registoCidade.adicionarCidade("c1", 1, 1f, 4f);
        registoCidade.adicionarCidade("c2", 2, 2f, 2f);
        registoCidade.adicionarCidade("c3", 3, 3f, 5f);
        registoCidade.adicionarCidade("c4", 4, 4f, 3f);
        registoCidade.adicionarCidade("c5", 5, 6f, 9f);
        registoCidade.adicionarCidade("c6", 5, 7f, 1f);
        
        c1 = registoCidade.getCidade("c1");
        c2 = registoCidade.getCidade("c2");
        c3 = registoCidade.getCidade("c3");
        c4 = registoCidade.getCidade("c4");
        c5 = registoCidade.getCidade("c5");
        c6 = registoCidade.getCidade("c6");
        
        u1.moverParaCidade(c1);
        u2.moverParaCidade(c1);
        u3.moverParaCidade(c1);
        u4.moverParaCidade(c2);
        u5.moverParaCidade(c3);
        u6.moverParaCidade(c6);
        u7.moverParaCidade(c3);
        u8.moverParaCidade(c4);
        
        controller = new DevolverAmigosPorLocalizacaoController(mundo);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of existeUtilizadorComNome method, of class
     * DevolverAmigosPorLocalizacaoController.
     */
    @Test
    public void testExisteUtilizadorComNome() {
        System.out.println("existeUtilizadorComNome");
        String nome = "u10";
        DevolverAmigosPorLocalizacaoController instance = controller;
        boolean expResult = false;
        boolean result = instance.existeUtilizadorComNome(nome);
        assertEquals(expResult, result);
    }

    /**
     * Test of existeCidadeComNome method, of class
     * DevolverAmigosPorLocalizacaoController.
     */
    @Test
    public void testExisteCidadeComNome() {
        System.out.println("existeCidadeComNome");
        String nome = "c10";
        DevolverAmigosPorLocalizacaoController instance = controller;
        boolean expResult = false;
        boolean result = instance.existeCidadeComNome(nome);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of encontrarAmigosEmCidade method, of class
     * DevolverAmigosPorLocalizacaoController.
     */
    @Test
    public void testEncontrarAmigosEmCidade() {
        System.out.println("encontrarAmigosEmCidade");
        String nomeUtilizador = "u1";
        String nomeCidade = "c1";
        DevolverAmigosPorLocalizacaoController instance = controller;
        List<String> amigosCidade = new ArrayList<>();
        amigosCidade.add(u2.getNome());
        amigosCidade.add(u3.getNome());
        List<String> expResult = amigosCidade;
        List<String> result = instance.encontrarAmigosEmCidade(nomeUtilizador, nomeCidade);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of encontrarAmigosEmCidadePorCoordenadas method, of class
     * DevolverAmigosPorLocalizacaoController.
     */
    @Test
    public void testEncontrarAmigosEmCidadePorCoordenadas() {
        System.out.println("encontrarAmigosEmCidadePorCoordenadas");
        String nomeUtilizador = "u1";
        float latitude = 2.0F;
        float longitude = 2.0F;
        DevolverAmigosPorLocalizacaoController instance = controller;
        List<String> amigosLocalizao = new ArrayList<>();
        amigosLocalizao.add(u4.getNome());
        List<String> expResult = amigosLocalizao;
        List<String> result = instance.encontrarAmigosEmCidadePorCoordenadas(nomeUtilizador, latitude, longitude);
        assertEquals(expResult, result);
        
    }

}
