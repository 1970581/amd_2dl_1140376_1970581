/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.CidadeMayor;
import AMD_2DL_1140376_1970581.Mundo;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class DevolverCidadeMayorControllerTest {
    Mundo mundo;
    LerFicheiroController lerController;
    
    public DevolverCidadeMayorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mundo = new Mundo();
        lerController = new LerFicheiroController(mundo);
        lerController.carregarFicheiroCidades("data/files10/cities10.txt");
        lerController.carregarFicheiroUtilizadores("data/files10/users10.txt");
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of criarSetOrdenadoCidadeMayor method, of class DevolverCidadeMayorController.
     */
    @Test
    public void testCriarSetOrdenadoCidadeMayor() {
        System.out.println("criarSetOrdenadoCidadeMayor");
        DevolverCidadeMayorController instance = new DevolverCidadeMayorController(mundo);
        Set<CidadeMayor> expResult = null;
        Set<CidadeMayor> result = instance.criarSetOrdenadoCidadeMayor();
        
        CidadeMayor auxCidadeMayor;
        Iterator iterador = result.iterator();
        int size = mundo.getRegistoCidade().getMapRegistoCidade().size();
        assertEquals(size, result.size());
        int pontos = Integer.MAX_VALUE;  //Funciona.
        while(iterador.hasNext()){
            auxCidadeMayor = (CidadeMayor) iterador.next();
            assertEquals(mundo.getRegistoCidade().getCidade(auxCidadeMayor.getCidade().getNome()), auxCidadeMayor.getCidade());
            assertEquals(mundo.getRegistoUtilizador().getUtilizador(auxCidadeMayor.getMayor().getNome()), auxCidadeMayor.getMayor());
            assertEquals(true, pontos >= auxCidadeMayor.getPontos());
            pontos = auxCidadeMayor.getPontos();
        }
        System.out.println("Set continha "+ size + "cidades.");
        
        
    }

    /**
     * Test of criarListagemCidadeMayorPontos method, of class DevolverCidadeMayorController.
     */
    @Test
    public void testCriarListagemCidadeMayorPontos() {
        System.out.println("criarListagemCidadeMayorPontos");
        DevolverCidadeMayorController instance = new DevolverCidadeMayorController(mundo);
        List<String> expResult = new LinkedList();
        
        /*listaCidadeMayorPontos.add( 
                    cidadeMayor.getCidade().getNome() +
                    " -> " +
                    cidadeMayor.getMayor().getNome() +
                    " com " +
                    cidadeMayor.getPontos()+
                    " pontos."
                    );
        */
        
        /*
                Cidade   - > Mayor com valor de pontos.
                city1 -> nick6 com 146 pontos.
                city5 -> nick6 com 146 pontos.
                city4 -> nick8 com 117 pontos.
                city7 -> nick8 com 117 pontos.
                city0 -> nick2 com 109 pontos.
                city2 -> nick2 com 109 pontos.
                city6 -> nick4 com 87 pontos.
                city9 -> nick7 com 65 pontos.
                city3 -> nick9 com 49 pontos.
                city8 -> nick9 com 49 pontos.
                */
        
                expResult.add("city1 -> nick6 com 146 pontos.");
                expResult.add("city5 -> nick6 com 146 pontos.");
                expResult.add("city4 -> nick8 com 117 pontos.");
                expResult.add("city7 -> nick8 com 117 pontos.");
                expResult.add("city0 -> nick2 com 109 pontos.");
                expResult.add("city2 -> nick2 com 109 pontos.");
                expResult.add("city6 -> nick4 com 87 pontos.");
                expResult.add("city9 -> nick7 com 65 pontos.");
                expResult.add("city3 -> nick9 com 49 pontos.");
                expResult.add("city8 -> nick9 com 49 pontos.");
        
        
        
        List<String> result = instance.criarListagemCidadeMayorPontos();
        assertEquals(expResult, result);
        
    }
    
}
