/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class RemoverUtilizadorControllerTest {
    static Mundo mundo;
    static RegistoCidade registoCidade;
    static RegistoUtilizador registoUtilizador;
    static Utilizador u1;
    static Utilizador u2;
    static Utilizador u3;
    static Utilizador u4;
    static Cidade c1;
    static Cidade c2;
    static Cidade c3;
    static Cidade c4;
    static Cidade c5;
    static Cidade c6;
    static RemoverUtilizadorController controller;
    
    
    public RemoverUtilizadorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mundo = new Mundo();
        registoUtilizador = mundo.getRegistoUtilizador();
        registoCidade = mundo.getRegistoCidade();
        registoUtilizador.criarUtilizador("u1", "u1@email.com");
        registoUtilizador.criarUtilizador("u2", "u2@email.com");
        registoUtilizador.criarUtilizador("u3", "u3@email.com");
        registoUtilizador.criarUtilizador("u4", "u4@email.com");
        
        registoCidade.adicionarCidade("c1", 1, 1f,2f);
        registoCidade.adicionarCidade("c2", 2, 2f,2f);
        registoCidade.adicionarCidade("c3", 3, 3f,2f);
        registoCidade.adicionarCidade("c4", 4, 4f,2f);
        registoCidade.adicionarCidade("c5", 5, 5f,2f);
        registoCidade.adicionarCidade("c6", 5, 5f,2f);
        
        u1 = registoUtilizador.getUtilizador("u1");
        u2 = registoUtilizador.getUtilizador("u2");
        u3 = registoUtilizador.getUtilizador("u3");
        u4 = registoUtilizador.getUtilizador("u4");
        
        c1 = registoCidade.getCidade("c1");
        c2 = registoCidade.getCidade("c2");
        c3 = registoCidade.getCidade("c3");
        c4 = registoCidade.getCidade("c4");
        c5 = registoCidade.getCidade("c5");
        c6 = registoCidade.getCidade("c6");
        
        u1.moverParaCidade(c1);
        u1.moverParaCidade(c5);  //unica.
        u1.moverParaCidade(c2);
        u1.moverParaCidade(c1);
        u1.moverParaCidade(c3);
        u1.moverParaCidade(c1);
        u1.moverParaCidade(c4);
        u1.moverParaCidade(c1); //Mayor de c1
        
        u2.moverParaCidade(c1);
        u2.moverParaCidade(c6);
        u3.moverParaCidade(c1);
        u3.moverParaCidade(c2);
        u3.moverParaCidade(c6);
        u3.moverParaCidade(c1);//Segundo mayor de c1
        u3.moverParaCidade(c3);
        u4.moverParaCidade(c3);
        u4.moverParaCidade(c6);
        u2.moverParaCidade(c2);
        u2.moverParaCidade(c6);
        
        u1.adicionarAmizade(u2);
        u1.adicionarAmizade(u3);
        u1.adicionarAmizade(u4);
        u2.adicionarAmizade(u4);
        
        controller = new RemoverUtilizadorController(mundo);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of existeUtilizadorComNome method, of class RemoverUtilizadorController.
     */
    @Test
    public void testExisteUtilizadorComNome() {
        System.out.println("existeUtilizadorComNome");
        String nomeUtilizador = "u1";
        RemoverUtilizadorController instance = controller;
        boolean expResult = true;
        boolean result = instance.existeUtilizadorComNome(nomeUtilizador);
        assertEquals(expResult, result);
    }

    /**
     * Test of removerUtilizador method, of class RemoverUtilizadorController.
     */
    @Test
    public void testRemoverUtilizador() {
        System.out.println("removerUtilizador");
        String nomeUtilizador = "u1";
        RemoverUtilizadorController instance = controller;
        boolean expResult = true;
        boolean result = instance.removerUtilizador(nomeUtilizador);
        assertEquals(expResult, result);
        
        assertEquals(u3, c1.getMayor());
        assertEquals(null, c5.getMayor());
        assertEquals(u4, c3.getMayor());
        assertEquals(u2, c6.getMayor());
        assertEquals(c6, u2.getUltimaCidade());
        
        
        assertEquals(false, u2.getRegistoAmizade().isAmigo(u1));
        assertEquals(false, u2.getRegistoAmizade().isAmigo(u1));
        assertEquals(false, u3.getRegistoAmizade().isAmigo(u1));
        assertEquals(false, u4.getRegistoAmizade().isAmigo(u1));
        
        
    }
    
}
