/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class FazerCheckInNovaCidadeControllerTest {
    
    static Mundo mundo;
    static RegistoCidade registoCidade;
    static RegistoUtilizador registoUtilizador;
    static Utilizador utilizador1;
    static Utilizador utilizador2;
    static Cidade cidade1;
    static Cidade cidade2;
    static FazerCheckInNovaCidadeController controller;
    
    
    public FazerCheckInNovaCidadeControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mundo = new Mundo();
        registoUtilizador = mundo.getRegistoUtilizador();
        registoCidade = mundo.getRegistoCidade();
        utilizador1 = new Utilizador("u1", "u1@email.com");
        utilizador2 = new Utilizador("u2", "u2@email.com");
        cidade1 = new Cidade("cidade1", 1, 2f,3f);
        cidade2 = new Cidade("cidade2", 1, 2f,3f);
        registoUtilizador.criarUtilizador(utilizador1.getNome(), utilizador1.getEmail());
        registoUtilizador.criarUtilizador(utilizador2.getNome(), utilizador2.getEmail());
        registoCidade.adicionarCidade(cidade1.getNome(), cidade1.getPontos(), cidade1.getLatitude(),cidade1.getLongitude());
        registoCidade.adicionarCidade(cidade2.getNome(), cidade2.getPontos(), cidade2.getLatitude(),cidade2.getLongitude());
        
        //Os utilizadores criados tem a cidade onde estao a null, vamos movelos para uma cidade.
        registoUtilizador.getUtilizador(utilizador1.getNome()).moverParaCidade(registoCidade.getCidade(cidade1.getNome())); 
        registoUtilizador.getUtilizador(utilizador2.getNome()).moverParaCidade(registoCidade.getCidade(cidade1.getNome())); 
        
        controller = new FazerCheckInNovaCidadeController(mundo);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of existeUtilizadorComNome method, of class FazerCheckInNovaCidadeController.
     */
    @Test
    public void testExisteUtilizadorComNome() {
        System.out.println("existeUtilizadorComNome");
        String nomeUtilizador = utilizador1.getNome();
        FazerCheckInNovaCidadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.existeUtilizadorComNome(nomeUtilizador); //utilizador existente
        assertEquals(expResult, result);
        
        assertEquals(false, instance.existeUtilizadorComNome("lixo")); // veer se existe utilizador inexistente.
    }

    /**
     * Test of existeCidadeComNome method, of class FazerCheckInNovaCidadeController.
     */
    @Test
    public void testExisteCidadeComNome() {
        System.out.println("existeCidadeComNome");
        String nomeCidade = cidade1.getNome();
        FazerCheckInNovaCidadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.existeCidadeComNome(nomeCidade); // verificar cidade existente
        assertEquals(expResult, result);
        
        assertEquals(false, instance.existeCidadeComNome("lixo")); //verificar cidade inexistente
    }

    /**
     * Test of nomeDaCidadeOndeEstou method, of class FazerCheckInNovaCidadeController.
     */
    @Test
    public void testNomeDaCidadeOndeEstou() {
        
        System.out.println("nomeDaCidadeOndeEstou");
        String utilizador = utilizador1.getNome();
        FazerCheckInNovaCidadeController instance = controller;
        String expResult = cidade1.getNome();
        String result = instance.nomeDaCidadeOndeEstou(utilizador);  // true, nome igual.
        assertEquals(expResult, result);
        
    }

    /**
     * Test of moverUtilizadorParaCidade method, of class FazerCheckInNovaCidadeController.
     */
    @Test
    public void testMoverUtilizadorParaCidade() {
        System.out.println("moverUtilizadorParaCidade");
        String nomeUtilizador = utilizador1.getNome();
        String nomeCidade = cidade2.getNome();
        FazerCheckInNovaCidadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.moverUtilizadorParaCidade(nomeUtilizador, nomeCidade); // movemos o utilizador da cidade 1 para a 2
        assertEquals(expResult, result);
        
        assertEquals(false, instance.moverUtilizadorParaCidade(nomeUtilizador, nomeCidade)); // repetimos o movimento, 2 para 2. Deve falhar.
    }
    
}
