/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class AdicionarCidadeControllerTest {
    
    static Mundo mundo;
    static RegistoCidade registoCidade;
    static RegistoUtilizador registoUtilizador;
    static AdicionarCidadeController controller;
    static Cidade c1;
    
    
    public AdicionarCidadeControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mundo = new Mundo();
        registoCidade = mundo.getRegistoCidade();
        c1 = new Cidade("c1", 1, 1f, 1f);
        registoCidade.adicionarCidade(c1.getNome(), c1.getPontos(), c1.getLatitude(), c1.getLongitude());
        controller = new AdicionarCidadeController(mundo);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of existeCidadeComNome method, of class AdicionarCidadeController.
     */
    @Test
    public void testExisteCidadeComNome() {
        System.out.println("existeCidadeComNome");
        String nome = c1.getNome();
        AdicionarCidadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.existeCidadeComNome(nome);
        assertEquals(expResult, result);
        assertEquals(false, instance.existeCidadeComNome("lixo") );
        
        
        
    }

    /**
     * Test of adicionaCidade method, of class AdicionarCidadeController.
     */
    @Test
    public void testAdicionaCidade() {
        System.out.println("adicionaCidade");
        String nome = "NovaYork";
        int pontos = 1;
        float latitude = 1.0F;
        float longitude = 1.0F;
        AdicionarCidadeController instance = controller;
        boolean expResult = true;
        boolean result = instance.adicionaCidade(nome, pontos, latitude, longitude);
        
        assertEquals(expResult, result); //Insere uma cidade.
        assertEquals(false, instance.adicionaCidade(nome, pontos, latitude, longitude)); // insersao duplicada.
        assertEquals(nome, registoCidade.getCidade(nome).getNome()); //Convifrmamos que a cidade foi adicionada.
        
    }
    
}
