/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Cidade;
import AMD_2DL_1140376_1970581.Mundo;
import AMD_2DL_1140376_1970581.RegistoCidade;
import AMD_2DL_1140376_1970581.RegistoUtilizador;
import AMD_2DL_1140376_1970581.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class DevolverPontosECidadesVisitadasDeUtilizadorControllerTest {
    
    static Mundo mundo;
    static RegistoCidade registoCidade;
    static RegistoUtilizador registoUtilizador;
    static Utilizador u1;
    static Utilizador u2;
    static Cidade c1;
    static Cidade c2;
    static Cidade c3;
    static Cidade c4;
    static DevolverPontosECidadesVisitadasDeUtilizadorController controller;
    
    
    public DevolverPontosECidadesVisitadasDeUtilizadorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        mundo = new Mundo();
        registoUtilizador = mundo.getRegistoUtilizador();
        registoCidade = mundo.getRegistoCidade();
        
        
        registoUtilizador.criarUtilizador("u1", "u1@email.com");
        registoUtilizador.criarUtilizador("u2", "u2@email.com");
        
        registoCidade.adicionarCidade("c1" , 1, 1f,1f);
        registoCidade.adicionarCidade("c2" , 10, 2f,2f);
        registoCidade.adicionarCidade("c3" , 3, 3f,3f);
        registoCidade.adicionarCidade("c4" , 5, 4f,4f);
        
        c1 = registoCidade.getCidade("c1");
        c2 = registoCidade.getCidade("c2");
        c3 = registoCidade.getCidade("c3");
        c4 = registoCidade.getCidade("c4");
        
        u1 = registoUtilizador.getUtilizador("u1");
        u2 = registoUtilizador.getUtilizador("u2");
                
        //Os utilizadores criados tem a cidade onde estao a null, vamos movelos para uma cidade.
        u1.moverParaCidade(c1);
        u2.moverParaCidade(c1);
        
        
        controller = new DevolverPontosECidadesVisitadasDeUtilizadorController(mundo);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of obterPontosDeUtilizador method, of class DevolverPontosECidadesVisitadasDeUtilizadorController.
     */
    @Test
    public void testObterPontosDeUtilizador() {
        System.out.println("obterPontosDeUtilizador");
        String nomeUtilizador = u1.getNome();
        DevolverPontosECidadesVisitadasDeUtilizadorController instance = controller;
        Integer expResult = u1.getPontos();
        Integer result = instance.obterPontosDeUtilizador(nomeUtilizador);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of obterStringArrayComNomeCidadesVisitadasPeloUtilizador method, of class DevolverPontosECidadesVisitadasDeUtilizadorController.
     */
    @Test
    public void testObterStringArrayComNomeCidadesVisitadasPeloUtilizador() {
        System.out.println("obterStringArrayComNomeCidadesVisitadasPeloUtilizador");
        String nomeUtilizador = u1.getNome();
        DevolverPontosECidadesVisitadasDeUtilizadorController instance = controller;
        String[] expResult = new String[5];
        expResult[0] = c1.getNome();
        expResult[1] = c2.getNome();
        expResult[2] = c3.getNome();
        expResult[3] = c4.getNome();
        expResult[4] = c1.getNome();
        
        //u1 ja esta na cidade 1 vamos movelo para as outras.
        u1.moverParaCidade(c2);
        u1.moverParaCidade(c3);
        u1.moverParaCidade(c4);
        u1.moverParaCidade(c1);
        
        String[] result = instance.obterStringArrayComNomeCidadesVisitadasPeloUtilizador(nomeUtilizador);
        assertArrayEquals(expResult, result);
        
    }
    
}
