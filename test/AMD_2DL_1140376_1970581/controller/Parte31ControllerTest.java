/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581.controller;

import AMD_2DL_1140376_1970581.Mundo;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author i140376
 */
public class Parte31ControllerTest {
    Mundo mundo;
    Parte31Controller controller;
    public Parte31ControllerTest() {
        mundo = new Mundo();
        LerFicheiroController lerController = new LerFicheiroController(mundo);
        lerController.carregarFicheiroCidades("data/files10/cities10.txt");
        lerController.carregarFicheiroUtilizadores("data/files10/users10.txt");
        controller = new Parte31Controller(mundo); // Tem de ser o ultimo para ja termos as cidades/Utilizadores carregadas.
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of ordenarMayorPontos method, of class Parte31Controller.
     */
    @Test
    public void testOrdenarMayorPontos() {
        ArrayList<String> resultados = new ArrayList<>();
        resultados.add("nick6:146");
        resultados.add("nick8:117");
        resultados.add("nick2:109");
        resultados.add("nick4:87");
        resultados.add("nick7:65");
        resultados.add("nick9:49");
        System.out.println("ordenarMayorPontos");
        Parte31Controller instance = controller;
        ArrayList<String> expResult = resultados;
        ArrayList<String> result = instance.ordenarMayorPontos();
        assertEquals(expResult, result);
       
    }
    
}
