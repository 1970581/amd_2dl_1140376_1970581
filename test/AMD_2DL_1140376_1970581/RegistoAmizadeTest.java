/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author CelsoEusebio
 */
public class RegistoAmizadeTest {
    static RegistoAmizade registoAmizade;
    static Utilizador utilizador;
    static Utilizador amigo1;
    static Utilizador amigo2;
    static Utilizador amigo3;
    static Utilizador amigo4;
    
    public RegistoAmizadeTest(){}
    
    @BeforeClass
    public static void setUpClass() {
        Mundo mundo = new Mundo();
        mundo.getRegistoUtilizador().criarUtilizador("u1", "u1@email.com");
        mundo.getRegistoUtilizador().criarUtilizador("amigo1", "amigo1@email.com");
        mundo.getRegistoUtilizador().criarUtilizador("amigo2", "amigo2@email.com");
        mundo.getRegistoUtilizador().criarUtilizador("amigo3", "amigo3@email.com");
        mundo.getRegistoUtilizador().criarUtilizador("amigo4", "amigo4@email.com");
        utilizador = mundo.getRegistoUtilizador().getUtilizador("u1");
        amigo1 = mundo.getRegistoUtilizador().getUtilizador("amigo1");
        amigo2 = mundo.getRegistoUtilizador().getUtilizador("amigo2");
        amigo3 = mundo.getRegistoUtilizador().getUtilizador("amigo3");
        amigo4 = mundo.getRegistoUtilizador().getUtilizador("amigo4");
        
        registoAmizade = utilizador.getRegistoAmizade();
        registoAmizade.adicionarAmizade(amigo1);
        registoAmizade.adicionarAmizade(amigo2);
        registoAmizade.adicionarAmizade(amigo3);
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of hasAmigo method, of class RegistoAmizade.
     */
    @Test
    public void hasAmigoTest(){
        System.out.println("hasAmigo");
        boolean hasAmigo = registoAmizade.isAmigo(amigo1);
        
        boolean expResult = true;
        boolean result = hasAmigo;
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of adicionarAmizade method in case it is true, of class RegistoAmizade.
     */
    @Test
    public void adicionarAmizadeVerdadeiroTeste(){
        System.out.println("AdicionarAmizade");
        boolean adicionar = registoAmizade.adicionarAmizade(amigo4);
    
        boolean expResult = true;
        boolean result = adicionar;
        assertEquals(expResult, result);
    }
    /**
     * Test of adicionarAmizade method in case it is false, of class RegistoAmizade.
     */
    @Test
    public void adicionarAmizadeFalseTeste(){
        System.out.println("AdicionarAmizade");
        boolean adicionar = registoAmizade.adicionarAmizade(amigo1);
    
        boolean expResult = false;
        boolean result = adicionar;
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAmigo method, of class RegistoAmizade.
     */
    @Test
    public void getAmigoTest(){
        System.out.println("getAmigo");
        Utilizador u = registoAmizade.getAmigo(amigo1.getNome());
        
        Utilizador expResult = amigo1;
        Utilizador result = u;
        
        assertEquals(expResult, result);
        
    }
}
