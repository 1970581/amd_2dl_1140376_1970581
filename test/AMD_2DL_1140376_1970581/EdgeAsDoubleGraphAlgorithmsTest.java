 
package AMD_2DL_1140376_1970581;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author DEI-ESINF
 */
public class EdgeAsDoubleGraphAlgorithmsTest {
    
    AdjacencyMatrixGraph <String, Double> distanceMap = new AdjacencyMatrixGraph<>();

    public EdgeAsDoubleGraphAlgorithmsTest() {
    }
    
    @Before
    public void setUp() throws Exception {
		distanceMap.insertVertex("Porto");
		distanceMap.insertVertex("Braga");
		distanceMap.insertVertex("Vila Real");
		distanceMap.insertVertex("Aveiro");
		distanceMap.insertVertex("Coimbra");
		distanceMap.insertVertex("Leiria");

		distanceMap.insertVertex("Viseu");
		distanceMap.insertVertex("Guarda");
		distanceMap.insertVertex("Castelo Branco");
		distanceMap.insertVertex("Lisboa");
		distanceMap.insertVertex("Faro");
		distanceMap.insertVertex("Évora");
		

		distanceMap.insertEdge("Porto", "Aveiro", 75.0);
		distanceMap.insertEdge("Porto", "Braga", 60.0);
		distanceMap.insertEdge("Porto", "Vila Real", 100.0);
		distanceMap.insertEdge("Viseu", "Guarda", 75.0);
		distanceMap.insertEdge("Guarda",  "Castelo Branco", 100.0);
		distanceMap.insertEdge("Aveiro", "Coimbra", 60.0);
		distanceMap.insertEdge("Coimbra", "Lisboa", 200.0);
		distanceMap.insertEdge("Coimbra",  "Leiria",  80.0);
		distanceMap.insertEdge("Aveiro", "Leiria", 120.0);
		distanceMap.insertEdge("Leiria", "Lisboa", 150.0);

		
		distanceMap.insertEdge("Aveiro", "Viseu", 85.0);
		distanceMap.insertEdge("Leiria", "Castelo Branco", 170.0);
		distanceMap.insertEdge("Lisboa", "Faro", 280.0);

	}

 	
	@Test
	public void testShortestPath() {
		System.out.println("Test of shortest path");
		
		LinkedList<String> path = new LinkedList<String>();
		
		assertTrue("Should be -1 if vertex does not exist", EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap,  "Porto",  "LX",  path) == -1);
		
		assertTrue("Should be -1 if there is no path", EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap, "Porto", "Évora", path)==-1);
		
		assertTrue("Should be 0 if source and vertex are the same", EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap,  "Porto",  "Porto",  path) == 0);
		
		assertTrue("Path should be single vertex if source and vertex are the same", path.size() == 1);
		
		assertTrue("Path between Porto and Lisboa should be 335 Km", EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap,  "Porto",  "Lisboa",  path) == 335);
		
		Iterator<String> it = path.iterator();

		assertTrue("First in path should be Porto", it.next().compareTo("Porto")==0);
		assertTrue("then Aveiro", it.next().compareTo("Aveiro")==0);
		assertTrue("then Coimbra", it.next().compareTo("Coimbra")==0);
		assertTrue("then Lisboa", it.next().compareTo("Lisboa")==0);

		assertTrue("Path between Braga and Leiria should be 255 Km", EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap,  "Braga",  "Leiria",  path) == 255);
		
		it = path.iterator();

		assertTrue("First in path should be Braga", it.next().compareTo("Braga")==0);
		assertTrue("then Porto", it.next().compareTo("Porto")==0);
		assertTrue("then Aveiro", it.next().compareTo("Aveiro")==0);
		assertTrue("then Leiria", it.next().compareTo("Leiria")==0);
		
		assertTrue("Path between Porto and Castelo Branco should be 335 Km", EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap,  "Porto",  "Castelo Branco",  path) == 335);
		assertTrue("Path between Porto and Castelo Branco should be 5 cities", path.size() == 5);
		
		it = path.iterator();

		assertTrue("First in path should be Porto", it.next().compareTo("Porto")==0);
		assertTrue("then Aveiro", it.next().compareTo("Aveiro")==0);
		assertTrue("then Viseu", it.next().compareTo("Viseu")==0);
		assertTrue("then Guarda", it.next().compareTo("Guarda")==0);
		assertTrue("then Castelo Branco", it.next().compareTo("Castelo Branco")==0);
		
		// Changing Viseu to Guarda should change shortest path between Porto and Castelo Branco

		distanceMap.removeEdge("Viseu", "Guarda");
		distanceMap.insertEdge("Viseu", "Guarda", 125.0);
		
		assertTrue("Path between Porto and Castelo Branco should now be 365 Km", EdgeAsDoubleGraphAlgorithms.shortestPath(distanceMap,  "Porto",  "Castelo Branco",  path) == 365);
		assertTrue("Path between Porto and Castelo Branco should be 4 cities", path.size() == 4);
		
		it = path.iterator();
	
		assertTrue("First in path should be Porto", it.next().compareTo("Porto")==0);
		assertTrue("then Aveiro", it.next().compareTo("Aveiro")==0);
		assertTrue("then Leiria", it.next().compareTo("Leiria")==0);
		assertTrue("then Castelo Branco", it.next().compareTo("Castelo Branco")==0);
		
		
	}

	@Test
	public void testMinDistGraph() {
                
            /*
            Double a = 100d;
                Double b = null;
                if (b != null) {System.out.println("ok");}
                
                //if (distanceMap.edgeMatrix[0][0] == null) System.out.println("null");
                if (distanceMap.getEdge(distanceMap.vertices.get(0),distanceMap.vertices.get(5)) == null) System.out.println("null 1");
               //boolean pass = distanceMap.insertEdge(distanceMap.vertices.get(0), distanceMap.vertices.get(5), new Double(12d));
                if (distanceMap.getEdge(distanceMap.vertices.get(0),distanceMap.vertices.get(5)) == null) System.out.println("null 1");
                
                
                AdjacencyMatrixGraph<String,Double>  test = (AdjacencyMatrixGraph<String,Double>) distanceMap.clone();
                boolean pass2 = test.insertEdge(test.vertices.get(0), test.vertices.get(5), new Double(12d));
                
                
                Object test2[][] = new Object[2][2];
                
                if (test2[0][0] == null) System.out.println("null");
                
                Object[][] test3 = test2.clone();
                if (test3[0][0] == null) System.out.println("null");
                
                
                if (test.edgeMatrix[0][0] == null) System.out.println("null");
                
                
                //test.insertEdge( gk1.vertices.get(1) ,gk1.vertices.get(2) ,  new Double(  test.edgeMatrix[1][2] + test.edgeMatrix[2][3]));
            */
		AdjacencyMatrixGraph<String, Double> answer = EdgeAsDoubleGraphAlgorithms.minDistGraph(distanceMap);
                
                 int i = 0;
                 i++;
                Double e = answer.getEdge("Braga","Leiria");
                assertTrue("falhou ligacao Braga Leiria", (e != null) );
                // "Porto",  "Lisboa",  path) == 335
                Double pl = answer.getEdge("Porto","Lisboa");
                assertTrue("distancia Porto Lisboa tem de ser 335",(pl.doubleValue() == 335) );
                
                assertTrue("Nao pode existir caminho para Evora",(answer.getEdge("Porto","Evora")  == null) );
                
                //"Braga",  "Leiria",  path) == 255);
                assertTrue("distancia Braga Leiria tem de ser 255",(answer.getEdge("Braga","Leiria").doubleValue() == 255) );
                
                //"Porto",  "Castelo Branco",  path) == 335);
                assertTrue("distancia Porto Castelo Branco tem de ser 355",(answer.getEdge("Porto","Castelo Branco").doubleValue() == 335) );
                assertFalse("distancia Porto Castelo Branco tem de ser 355",(answer.getEdge("Porto","Castelo Branco").doubleValue() == 3351) );
                
	}
        
        
        @Test
	public void testConstrainedShortestPath() {
            System.out.println("Test of Constrained shortest path (CUSTOM)");
            
            
            Double distancia = 0d;
            
            ArrayList<String> restricao = new ArrayList<>();
            ArrayList<String> caminho = new ArrayList<>();
            
            
            distancia = EdgeAsDoubleGraphAlgorithms.constrainedShortestPath(distanceMap, "Porto", "Lisboa", restricao, caminho);
            
            
            assertTrue("Tem de existir caminho Porto Lisboa", distancia.doubleValue() != 1);
            assertTrue("Distancia do caminho Porto Lisboa tem de ser 335", distancia.doubleValue() == 335);
            
            restricao.add("Guarda");
            restricao.add("Castelo Branco");
            caminho.clear();
            distancia = EdgeAsDoubleGraphAlgorithms.constrainedShortestPath(distanceMap, "Porto", "Lisboa", restricao, caminho);
            assertTrue("Tem de existir caminho Porto Lisboa passando por Braga e Castelo Branco", distancia.doubleValue() != 1);
            assertTrue("Tem de Comecar no Porto", caminho.get(0).equals("Porto"));
            assertTrue("Tem de Terminar em Lisboa", caminho.get(caminho.size()-1).equals("Lisboa"));
            boolean contains = false;
            for(String s : caminho){
                if(s.equals("Guarda")) contains = true;
            }
            assertTrue("Tem de conter em Guarda", contains);
            contains = false;
            for(String s : caminho){
                if(s.equals("Castelo Branco")) contains = true;
            }
            assertTrue("Tem de conter em Castelo Branco", contains);
            
            
            
            
        }

}
