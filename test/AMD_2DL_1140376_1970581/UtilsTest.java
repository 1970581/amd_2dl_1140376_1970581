/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class UtilsTest {
    
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularDistanciaEntreDuasCoordenadas method, of class Utils.
     */
    @Test
    public void testCalcularDistanciaEntreDuasCoordenadas() {
        System.out.println("calcularDistanciaEntreDuasCoordenadas");
        float latitudeA = 41.173486F;
        float longitudeA = -8.621292F;  //Porto
        float latitudeB = 38.718733F;   //Lisboa
        float longitudeB = -9.141540F;
        //float expResult = 275.0F; //Km
        float expResult = 275000.0F; //m
        float result = Utils.calcularDistanciaEntreDuasCoordenadas(latitudeA, longitudeA, latitudeB, longitudeB);
        System.out.println("Distancia do teste = " + result + " km.");
        assertEquals(expResult, result, 5000.0F);
        
    }
    
}
