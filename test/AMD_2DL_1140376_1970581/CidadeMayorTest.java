/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class CidadeMayorTest {
    Utilizador u1;
    Utilizador u2;
    Cidade c1;
    Cidade c2;
    
    public CidadeMayorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        u1 = new Utilizador("u1", "u1@email.com");
        u2 = new Utilizador("u2", "u2@email.com");
        c1 = new Cidade("c1", 1, 1f, 1f);
        c2 = new Cidade("c2", 1, 2f, 1f);
        
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCidade method, of class CidadeMayor.
     */
    @Test
    public void testGetCidade() {
        System.out.println("getCidade");
        CidadeMayor instance = new CidadeMayor(c1, u1);
        Cidade expResult = c1;
        Cidade result = instance.getCidade();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getPontos method, of class CidadeMayor.
     */
    @Test
    public void testGetPontos() {
        System.out.println("getPontos");
        CidadeMayor instance = new CidadeMayor(c1, u1);
        u1.moverParaCidade(c1);
        int expResult = 1;
        int result = instance.getPontos();
        assertEquals(expResult, result);       
        
    }

    /**
     * Test of setCidade method, of class CidadeMayor.
     */
    @Test
    public void testSetCidade() {
        System.out.println("setCidade");
        Cidade cidade = c2;
        CidadeMayor instance = new CidadeMayor(c1, u1);;
        instance.setCidade(cidade);
        
        assertEquals(c2, instance.getCidade());
    }

    /**
     * Test of getMayor method, of class CidadeMayor.
     */
    @Test
    public void testGetMayor() {
        System.out.println("getMayor");
        CidadeMayor instance = new CidadeMayor(c1, u1);
        Utilizador expResult = u1;
        Utilizador result = instance.getMayor();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMayor method, of class CidadeMayor.
     */
    @Test
    public void testSetMayor() {
        System.out.println("setMayor");
        Utilizador mayor = u2;
        CidadeMayor instance = new CidadeMayor(c1, u1);;
        instance.setMayor(mayor);
        assertEquals(u2,instance.getMayor());
    }

    /**
     * Test of equals method, of class CidadeMayor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new CidadeMayor(c1, u1);
        CidadeMayor instance = new CidadeMayor(c1, u1);;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        assertEquals(false,(new CidadeMayor(c1,u1)).equals(new CidadeMayor(c1,u2)));
        
    }

    /**
     * Test of hashCode method, of class CidadeMayor.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        CidadeMayor i1 = new CidadeMayor(c1,u2);
        CidadeMayor i2 = new CidadeMayor(c2,u2);
        CidadeMayor i3 = new CidadeMayor(c1,u2);
        
        assertEquals(true, i1.hashCode() == i3.hashCode());
        assertEquals(false, i1.hashCode() == i2.hashCode());
        assertEquals(false, i2.hashCode() == i3.hashCode());
        
        
    }

    /**
     * Test of compareTo method, of class CidadeMayor.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Object o = new CidadeMayor(c1, u1);
        CidadeMayor instance = new CidadeMayor(c1, u1);
        int expResult = 0;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
        
        u1.moverParaCidade(c1);
        u1.moverParaCidade(c2);
        u1.moverParaCidade(c1);
        u2.moverParaCidade(c1);
        
        assertEquals(-2, instance.compareTo(new CidadeMayor(c2,u2)));
        
        
    }
    
}
