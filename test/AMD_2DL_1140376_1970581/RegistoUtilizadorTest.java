/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class RegistoUtilizadorTest {
    static RegistoUtilizador registoUtilizador;
    static Utilizador utilizador1;
    static Utilizador utilizador2;
    static Utilizador utilizador3;
    
    public RegistoUtilizadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        registoUtilizador = new RegistoUtilizador();
        utilizador1 = new Utilizador("nome1", "email1@isep.ipp.pt");
        utilizador2 = new Utilizador("nome2", "email2@isep.ipp.pt");
        utilizador3 = new Utilizador("nome3", "email3@isep.ipp.pt");
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUtilizador method, of class RegistoUtilizador.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        String nomeUtilizador = utilizador1.getNome();
        RegistoUtilizador instance = new RegistoUtilizador();
        instance.criarUtilizador(utilizador1.getNome(), utilizador1.getEmail());
        Utilizador expResult = utilizador1;
        Utilizador result = instance.getUtilizador(nomeUtilizador);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getUtilizadorByEmail method, of class RegistoUtilizador.
     */
    @Test
    public void testGetUtilizadorByEmail() {
        System.out.println("getUtilizadorByEmail");
        String email = utilizador1.getEmail();
        RegistoUtilizador instance = new RegistoUtilizador();
        instance.criarUtilizador(utilizador1.getNome(), email);
        Utilizador expResult = utilizador1;
        Utilizador result = instance.getUtilizadorByEmail(email);
        assertEquals(expResult, result);        
    }

    /**
     * Test of criarUtilizador method, of class RegistoUtilizador.
     */
    @Test
    public void testCriarUtilizador() {
        System.out.println("criarUtilizador");
        String nickName = utilizador1.getNome();
        String email = utilizador1.getEmail();
        RegistoUtilizador instance = new RegistoUtilizador();
        Utilizador expResult = utilizador1;
        
        Utilizador result = instance.criarUtilizador(nickName, email);
        assertEquals(expResult, result);
        
    }
    
}
