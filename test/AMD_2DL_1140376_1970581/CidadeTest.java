/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AMD_2DL_1140376_1970581;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 
 */
public class CidadeTest {
    static  Cidade cidade1;
    
    public CidadeTest() {
       
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        cidade1 = new Cidade("cidade1",1, 2f,3f);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNome method, of class Cidade.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Cidade instance = cidade1;
        String expResult = "cidade1";
        String result = instance.getNome();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getPontos method, of class Cidade.
     */
    @Test
    public void testGetPontos() {
        System.out.println("getPontos");
        Cidade instance = cidade1;
        int expResult = 1;
        int result = instance.getPontos();
        assertEquals(expResult, result);        
    }

    /**
     * Test of getLatitude method, of class Cidade.
     */
    @Test
    public void testGetLatitude() {
        System.out.println("getLatitude");
        Cidade instance = cidade1;
        float expResult = 2.0f;
        float result = instance.getLatitude();
        assertEquals(expResult, result,0.0);
        
    }

    /**
     * Test of getLongitude method, of class Cidade.
     */
    @Test
    public void testGetLongitude() {
        System.out.println("getLongitude");
        Cidade instance = cidade1;
        float expResult = 3.0F;
        float result = instance.getLongitude();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of utilizadorMoveuParaCidadeActualizarMayor method, of class Cidade.
     */
    @Test
    public void testUtilizadorMoveuParaCidade() {
        System.out.println("utilizadorMoveuParaCidade");
        Mundo mundo = new Mundo();
        mundo.getRegistoUtilizador().criarUtilizador("u1", "u1@email.com");
        mundo.getRegistoCidade().adicionarCidade("c1", 1, 2f,3f);
        mundo.getRegistoCidade().adicionarCidade("c2", 1, 2f,3f);
        Cidade c1 = mundo.getRegistoCidade().getCidade("c1");
        Cidade c2 = mundo.getRegistoCidade().getCidade("c2");
        Utilizador utilizador1 = mundo.getRegistoUtilizador().getUtilizador("u1");
        
        assertEquals(null, c1.getMayor());
        utilizador1.getListaCidadesVisitadas().add(c1);
        
        
        Cidade instance = c1;
        instance.utilizadorMoveuParaCidadeActualizarMayor(utilizador1);
        boolean expResult = true;
        boolean result = instance.getMayor().equals(utilizador1);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of equals method, of class Cidade.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = cidade1;
        Cidade instance = new Cidade(cidade1.getNome(),cidade1.getPontos(),cidade1.getLatitude(),cidade1.getLongitude());
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result && !cidade1.equals(new Cidade("a",1,2f,3f)));
        
    }

    /**
     * Test of hashCode method, of class Cidade.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Cidade instance = new Cidade("cidade1",1,2f,3f);
        int expResult = cidade1.hashCode();
        int result = instance.hashCode();
        assertEquals(expResult, result);
        
    }

 
    
    /**
     * Test of getMayor method, of class Cidade.
     */
    @Test
    public void testGetMayor() {
        Utilizador u1 = new Utilizador("u1", "u1@email.com");
        Utilizador u2 = new Utilizador("u2", "u2@email.com");
        Utilizador u3 = new Utilizador("u3", "u3@email.com");
        Utilizador u4 = new Utilizador("u4", "u4@email.com");
        Utilizador ua = new Utilizador("ua", "ua@email.com");
        Utilizador uz = new Utilizador("uz", "ua@email.com");
        Cidade c1 = new Cidade("c1", 1, 1f, 1f);
        Cidade c5 = new Cidade("c5", 5, 2f,2f);
        Cidade c20 = new Cidade("c20", 20, 3f, 3f);
        Cidade c500 = new Cidade("c500", 500, 3f, 3f);
        
        /*
        u2.moverParaCidade(c1);
        u2.moverParaCidade(c500);
        u2.moverParaCidade(c1); // 510 pontos.
        u2.moverParaCidade(c5); // 515 pontos e fora da cidade
        
        u1.moverParaCidade(c1);
        u1.moverParaCidade(c5);
        u1.moverParaCidade(c1); // 7 pontos
        
        u3.moverParaCidade(c1);
        u3.moverParaCidade(c20);
        u3.moverParaCidade(c1); // 22 pontos
        
        u4.moverParaCidade(c20);
        u4.moverParaCidade(c1);  //21 pontos
        */
        
        ua.moverParaCidade(c1);
        uz.moverParaCidade(c1);
        uz.moverParaCidade(c5);
        ua.moverParaCidade(c1);
        uz.moverParaCidade(c1);
        uz.moverParaCidade(c5);
        ua.moverParaCidade(c1); 
        uz.moverParaCidade(c1); 
        uz.moverParaCidade(c5);
        
        // ua e uz moveram 3 vezes para a cidade c1.
        
        System.out.println("getMayor");
        Cidade instance = c1;
        Utilizador expResult = uz;
        Utilizador result = instance.getMayor();
        assertEquals(expResult, result);
        assertEquals(null, c20.getMayor());
    }
    
}
